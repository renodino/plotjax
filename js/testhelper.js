/*
 *	PlotJax Helper class for testmatrix
 */

TestHelper = function () {
	this.msg = "Hello world!";
	this.content = document.createElement("div");
	this.content.setAttribute("id", "balloon_innards");
	this.content.style.position = "absolute";
	this.content.style.left = 0;
	this.content.style.top = 0;
	this.content.style.width = "300px";
	this.content.style.visibility = "hidden";
	this.content.innerHTML = "this balloon should display a whole bunch of text that gets wrapped when we insert it into the balloon" +
	" then it should wrap again as much as needed until we finally get at least a vertical scrollbar, " +
	"and maybe a horiztonal scrollbar as well. Now is the time for all good men to come to the aid of their country.<br>" +
	"The quick brown fox jumped over the lazy dog. Now is the time for all good men to come to the aid of their country.<br>" +
	"The quick brown fox jumped over the lazy dog. Now is the time for all good men to come to the aid of their country.<br>" +
	"The quick brown fox jumped over the lazy dog. Now is the time for all good men to come to the aid of their country.<br>" +
	"The quick brown fox jumped over the lazy dog. Now is the time for all good men to come to the aid of their country.<br>";
	document.body.appendChild(this.content);
};

TestHelper.prototype.getMsg = function() { return this.msg; };

TestHelper.prototype.getBalloonContent = function(plotjax, chart, elemid) {
//	return "this is some content.";
//	return "id:balloon_innards";
/*
	return "this balloon should display a whole bunch of text that gets wrapped when we insert it into the balloon" +
	" then it should wrap again as much as needed until we finally get at least a vertical scrollbar, " +
	"and maybe a horiztonal scrollbar as well. Now is the time for all good men to come to the aid of their country.<br>" +
	"The quick brown fox jumped over the lazy dog. Now is the time for all good men to come to the aid of their country.<br>" +
	"The quick brown fox jumped over the lazy dog. Now is the time for all good men to come to the aid of their country.<br>" +
	"The quick brown fox jumped over the lazy dog. Now is the time for all good men to come to the aid of their country.<br>" +
	"The quick brown fox jumped over the lazy dog. Now is the time for all good men to come to the aid of their country.<br>";
*/
	return "url:../test/balloontext.html";
};

TestHelper.prototype.onhover = function(plotjax, chart, elemid) {
	return chart.getElement(elemid).join(",");
}
