
registerNS("PlotJax.BarChart");
registerNS("PlotJax.HBarChart");

PlotJax.BarChart = function(chartdesc, plot, limits, shapes) {
	if (arguments.length > 0) {
		this.plot = plot;
		this.chartdesc = chartdesc;
		this.shapes = shapes;
		this.points = [];
		this.visible_points = [];
		this.zooms = [];
		this.undos = [];
		this.redos = [];
		this.balloon = plot.getBalloon();
		this.gesture = plot.getGesture();
		this.xType = plot.getXType();
		this.yType = plot.getYType();
		this.limits = limits;
		this.myid = chartdesc.PlotID;
		this.helper = plot.getHelper();
		this.chartstyles = (chartdesc.Style == null) ? [ ] : 
			(chartdesc.Style instanceof Array) ? chartdesc.Style :
			[ chartdesc.Style ];
		if (chartdesc.Stacked == null) { chartdesc.Stacked = false; }

		this.colors = (chartdesc.Colors != null) ? 
			chartdesc.Color[0] : 
			[ "#000000", 'red', 'blue', 'green', 'orange', 'yellow', 'purple' ];
// alert("in barchart constructor");
		var i;
//
//	validate styling
//
		var chartstyles = this.chartstyles;
		for (i = 0; i < chartstyles.length; i++) {
			var chartstyle = chartstyles[i];
// alert("in barchart get color");
			if (chartstyle.Color == null) {
				chartstyle.Color = "black";
			}
		}		
//
//	collect points, mins, and max's
//
		var chartdata = chartdesc.Data;
		if ((chartdata == null) || (chartdata.length == 0)) {
			if ((limits[1] == null) || (limits[0] == null) ||
				(limits[3] == null) || (limits[2] == null)) {
				alert("No data provided, and incomplete limits specified.");
				return null;
			}
		}
		else {
// alert("in barchart set datapts");
			var j, x, y, rangecnt, maxranges;
			maxranges = 0;
			for (i = 0; i < chartdata.length; i++) {
				var datapt = chartdata[i];
				if (datapt[0] == null) {
					alert("Missing domain value at element " + i);
					return null;
				}
				x = this.xType.normalize(datapt[0]);
				if ( ( (limits[1] != null) && (x > limits[1])) ||
					((limits[0] != null) && (x < limits[0]))) {
					continue;
				}
				maxranges = Math.max(maxranges, datapt.length - 1);
				for (j = 1; j < datapt.length; j++) {
//
//	we tolerate missing range values
//
					if (datapt[j] == null) {
						continue;
					}
					y = this.yType.normalize(datapt[j]);
/*
 *	filter data points outside limits
 */
//  alert("in barchart set x as " + x);
					if (((limits[3] != null) && (y > limits[3])) ||
						((limits[2] != null) && (y < limits[2]))) {
						continue;
					}
//
//	make sure our point cache has enough ranges
//
					while (this.points.length < j) {
						this.points.push([]);
					}
					this.points[j-1].push([x,y]);
				} // end for each range
			}	// end for each domain value
			if (!chartdesc.Stacked) {
				this.bars_per_domain = maxranges;
			}
		}
/*
 *	points are maintained in domain ascending order
 *	(makes rendering of linegraphs easier)
 */
		for (i = 0; i < this.points.length; i++) {
			this.points[i] = this.points[i].sort(this.sortByDomain);
		}
		while (this.visible_points.length < this.points.length) {
			this.visible_points.push([]);
		}
//
//	note: we record only the index of a visible point, not a copy of the data
//
		for (i = 0; i < this.points.length; i++) {
			for (j = 0; j < this.points[i].length; j++) {
				this.visible_points[i].push(j);
			}
		}
//
//	setup as a gesture observer
//
// alert("in barchart set gesture");
		this.gesture.observe(this);
	}
}

PlotJax.BarChart.prototype.getChartID = function() { return this.myid; }

PlotJax.BarChart.prototype.getBarsPerDomain = function() { return this.bars_per_domain; }

PlotJax.BarChart.prototype.setBarOffsets = function(offset, pxlsperbar) { 
//
//	compute space required: check for icons
//
	this.pxlsperbar = pxlsperbar;
	this.bar_offset = offset;
	for (var i = 0; i < this.chartstyles.length; i++) {
		if (this.chartstyles[i].Icon != null) {
			var icon = this.shapes.getIcon(this.chartstyles[i].Icon);
			if (((this instanceof PlotJax.HBarChart) && (icon.offsetHeight > pxlsperbar)) ||
				(!(this instanceof PlotJax.HBarChart) && (icon.offsetWidth > pxlsperbar))) 
			{
				alert("Icon " + this.chartstyles[i].Icon + " dimensions exceeds available chart size.");
				return null;
			}
		}
	}
	return offset - (pxlsperbar * this.bars_per_domain); 
}

/*
 *	!!! DEPRECATE !!! We can't use this and maintain sorted order...
 */
PlotJax.BarChart.prototype.addPoint = function(range, x,y) {
	while (this.points.length <= range) {
		this.points.push([]);
	}
	this.points[range].push([x,y]);
	return true;
};

PlotJax.BarChart.prototype.getElement = function(range, i) { 
	return (range < this.points.length) ? 
		[ this.xType.display(this.points[range][i][0]),
		this.yType.display(this.points[range][i][1]) ] :
		null; 
};

PlotJax.BarChart.prototype.getElements = function(range) { 
	return (range != null) ?
		((range >= this.points.length) ?  null : this.points[range]) : this.points; 
};

PlotJax.BarChart.prototype.layout = function(sticky, limits) {
// alert("in BarChart layout");

	if ((this.helper != null) && (this.helper.onlayout != null)) {
		this.helper.onlayout(this.plot, this);
	}
/*
 *	determine bounds of each dimension;
 *	size/intensity aren't computed until redraw time
 */
	var min_x = Number.MAX_VALUE;
	var max_x = Number.MIN_VALUE;
	var min_y = Number.MAX_VALUE;
	var max_y = Number.MIN_VALUE;

// alert("in BarChart layout: find bounds");
	var i,j, count = 0;
	for (i = 0; i < this.visible_points.length; i++) {
		for (j = 0; j < this.visible_points[i].length; j++) {
			var dimensions = this.points[i][this.visible_points[i][j]];
			max_x = Math.max(max_x, dimensions[0]);
			min_x = Math.min(min_x, dimensions[0]);
			max_y = Math.max(max_y, dimensions[1]);
			min_y = Math.min(min_y, dimensions[1]);
		}
		count += j;
	}
/*
 *	merge our limits with the global limits
 */
// alert("in BarChart layout: merge limits");
	limits[0] = (limits[0] == null) ? min_x : Math.min(min_x, limits[0]);
	limits[1] = (limits[1] == null) ? max_x : Math.max(max_x, limits[1]);
	limits[2] = (limits[2] == null) ? min_y : Math.min(min_y, limits[2]);
	limits[3] = (limits[3] == null) ? max_y : Math.max(max_y, limits[3]);
// alert("in BarChart layout: returning " + limits.join(","));
	return count;
}


PlotJax.BarChart.prototype.sortVisibles = function() {
	var count;
	for (var i = 0; (i < this.visible_points.length) ; i++) { 
		if (this.visible_points[i].length != 0) {
//
//	this works cuz we enforce the sort order on the master points array
//
			this.visible_points[i] = this.visible_points[i].sort(this.sortByIndex);
			count++;
		}
	}
	return count;
}

/*
 * draw points
 */
PlotJax.BarChart.prototype.redraw = function(sticky) {
 alert("in BarChart redraw");
	if ((this.helper != null) && (this.helper.ondraw != null)) {
		this.helper.ondraw(this.plot, this);
	}
	if (this.sortVisibles() == 0) { return; }	// nothing to do

	var i, j, k, x, y;
	this.visible_coords = [];
	axisCoords = this.plot.getAxisCoords();

	var lm = this.plot.getLeftMargin();
	var ve = this.plot.getVertEdge();
	var color, shape, icon;
	k = 0;
	for (i = 0; i < this.visible_points.length; i++) {
		this.visible_coords.push([]);
		var style = this.chartstyles[k++];
		if (k == this.chartstyles.length) { k = 0; }
		for (j = 0; j < this.visible_points[i].length; j++) {
			x = this.xType.pt2pxl(this.points[i][this.visible_points[i][j]][0], lm);
			y = ve - this.yType.pt2pxl(this.points[i][this.visible_points[i][j]][1], 0);

			this.visible_coords[i].push(this.shapes.draw(x, y, style.Color, style.Shape, style.Icon));
		}
	}
}

PlotJax.BarChart.prototype.drawBar = function(x, y, color, icon) {
}

PlotJax.BarChart.prototype.sortByDomain = function(a,b) {
	return ((a == null) || (b == null)) ? 0 : a[0] - b[0];
};

PlotJax.BarChart.prototype.sortByIndex = function(a,b) { return a - b; }

PlotJax.BarChart.prototype.redo = function() {
	var redo = this.redos.pop();
	var temp = [];
	var i,j,k;
	for (i = 0; i < this.visible_points.length; i++) {
		for (j = this.visible_points[i].length - 1; (i >= 0) && (redo.length > 0); i--) {
			var id = this.visible_points[i][j];
/*
 *	locate the point in the redo list; if found, remove it and push
 *	into undo list
 */
			for (k = 0; (k < redo.length) && ((redo[k][0] != i) || (redo[k][1] != id)); k++) {}
			if (k < redo.length) {
				this.visible_points[i].splice(j, 1);
				temp.push([i, id]);
				redo.splice(k, 1);
			}
		}
	}
	this.undos.push(temp);
};

PlotJax.BarChart.prototype.undo = function() {
	var redo = this.undos.pop();
	for (var i = 0; i < redo.length; i++) {
		this.visible_points[redo[i][0]].push(redo[i][1]);
	}
	this.redos.push(redo);
};
/*
 *	 discard a point (no undo)
 */
PlotJax.BarChart.prototype.discardElement = function(range, id) {
	if ((range < this.points.length) && (this.points[range].length < id)) {
		this.points[range][id] = null;
	}
	return this;
};

/*
 *	return the range index for a given range name
 */
PlotJax.BarChart.prototype.getRangeByName = function(name) {
	for (var i = 0; (i < this.chartstyle.length); i++) {
		if ((this.chartstyle[i].Name != null) && (this.chartstyle[i].Name == name)) {
			return i;
		}
	}
	return null;
}

/*
 *	return the range name for a given range index
 */
PlotJax.BarChart.prototype.getNameByRange = function(range) {
	return (range < this.chartstyle.length) ? this.chartstyle[range].Name : null;
}

/*
 *	recoverably hide points in the ids list
 *	input is a list of range, id pairs, an optimization to avoid
 *	multiple call overhead and add'l undo list mgmt
 */
PlotJax.BarChart.prototype.hideElements = function(ids) {
	var id;
	var undos = [];
	var i,j,k;
/*
 *	traverse in reverse order so we don't mess up the indexing when we splice
 */
	for (i = 0; (ids.length > 0) && (i < this.visible_points.length); i++) {
		for (j = this.visible_points[i].length - 1; (ids.length > 0) && (j >= 0) ; j--) {
			id = this.visible_points[i][j];
			for (k = 0; (k < ids.length) && ((i != ids[k]) || (id != ids[k+1])); k+=2) {}
			if (k < ids.length) {
				this.visible_points[i].splice(j, 1);
				undos.push(i,id);
				ids.splice(k, 2);	// don't need to look at this id again
			}
		}
	}
	if (undos.length > 0) {
		this.undos.push(undos);
	}
	return this;
};

/*
 *	recoverably hide points that are NOT in the specified list
 *	(ids is a list of range, id pairs)
 */
PlotJax.BarChart.prototype.keepElements = function(ids) {
	var id;
	var undos = [];
	var i,j,k;
	for (i = 0; (ids.length > 0) && (i < this.visible_points.length); i++) {
		for (j = this.visible_points[i].length - 1; (ids.length > 0) && (j >= 0) && (ids.length > 0); j--) {
			id = this.visible_points[i][j];
			for (k = 0; (k < ids.length) && ((i != ids[k]) || (id != ids[k+1])); k+=2) {}
			if (k == ids.length) {	// not in keep list
				this.visible_points[i].splice(j, 1);
				undos.push(i,id);
			}
			else {
				ids.splice(k, 2); 	// don't need to look at it again
			}
		}
		while (j >= 0) {
/*
 *	all remaining visible were not in keeplist, so remove them
 */
			this.undos.push(this.visible_points[i][j]);
			this.visible_points[i].splice(j, 1);
			j--;
		}
	}
	if (undos.length > 0) {
		this.undos.push(undos);
	}
	return this;
};

/*
 *	called by Gesture on an event; it will scan the visibles
 *	to locate any effected elements; note that we return a copy
 *	returned list is (range, id) pairs
 */
PlotJax.BarChart.prototype.getVisibleElements = function() {
	var elems = [];
	for (var i = 0; i < this.visible_points.length; i++) {
		for (var j = 0; j < this.visible_points[i].length; j++) {
			elems.push(i,j);
		}
	}
	return elems;
};

/*
 *	called by a click in the chart; check if any elements
 *	contain the specified coordinate; if so, return the element ID
 *	and hover text; return maxwidth/height as 0,0
 *	overlaps are not expected/supported
 */
PlotJax.BarChart.prototype.onclick = function(chartx, charty) {
	var clicked_pts = [0,0];
	var i,j, xdist, ydist;
	for (i = 0; i < this.visible_points.length; i++) {
		var coords = this.visible_coords[i];
		for (j = 0; j < coords.length; j++) {
			if ((chartx >= coords[j][0]) && (chartx <= coords[j][2]) &&
				(charty >= coords[j][1]) && (charty <= coords[j][3])) {
				clicked_pts.push(this.visible_points[i][j],
					this.xType.display(this.points[this.visible_points[i][j]][0]) + "," +
					this.yType.display(this.points[this.visible_points[i][j]][1]));
				return clicked_pts;
			}
		}
	}

	return null;
}

PlotJax.BarChart.prototype.onhover = function(x, y) {
	var clicked_pts = [];
	var i,j, xdist, ydist;
	for (i = 0; i < this.visible_points.length; i++) {
		var coords = this.visible_coords[i];
		for (j = 0; j < coords.length; j++) {
			if ((x >= coords[j][0]) && (x <= coords[j][2]) &&
				(y >= coords[j][1]) && (y <= coords[j][3])) {
				return this.helper.onhover(this.plot, this, this.visible_points[i][j]);
			}
		}
	}
	return null;
}

PlotJax.BarChart.prototype.clear = function() {
/*
 *	delete any members that might cause memory leaks
 */
	this.points = null;
	this.visible_points = null;
	this.visible_coords = null;
	this.plot = null;
	this.helper = null;
	this.balloon = null;
	this.gesture = null;
	this.zooms = null;
	this.redos = null;
	this.undos = null;
}

/*
 *	observe a zoom event
 *		input is array of zoom objects that implement contains()
 *		return a list of element IDs that are contained 
 *		in the zoom. contains() takes the x,y of the
 *		center of an element
 *	HOW TO SELECT A PT TO contain ? or add an zoom.instersects() ?
 */
PlotJax.BarChart.prototype.observe = function(zooms) {
	var contained_bars = [];
	return contained_bars;
};

/*
 *	Horizontal barchart is just a barchart w/ different layout
 */
PlotJax.HBarChart = function(chartdesc, plot, limits, shapes) {
	PlotJax.HBarChart.baseConstructor.call(this, chartdesc, plot, limits, shapes);
}

JSPPTrustee.extend(PlotJax.HBarChart, PlotJax.BarChart);

/*
 * draw horizontal bars
 */
PlotJax.HBarChart.prototype.redraw = function(sticky) {
	if ((this.helper != null) && (this.helper.ondraw != null)) {
		this.helper.ondraw(this.plot, this);
	}
	if (this.visible_points.length == 0) {
		return;
	}
	this.visible_points = this.visible_points.sort(this.sortByIndex);
	this.visible_coords = [];

	var i;
	var lm = this.plot.getLeftMargin();
	var ve = this.plot.getVertEdge();
	var coords = [];
/*
 *	draw lines first, then the shapes
 *	NOTE: need to include any preceding or succeding points outside
 *	current chart area!!
 *	NOTE2: we need to support 3D lines via gradient effect
 */
 	if (this.visible_points.length > 0) {
		for (i = 0; i < this.visible_points.length; i++) {
			coords.push(
				this.xType.pt2pxl(this.points[this.visible_points[i]][0], lm),
				ve - yType.pt2pxl(this.points[this.visible_points[i]][1], 0)
			);
		}
		
		this.plot.drawLine(coords, this.color, this.lineWidth, this.pattern);
		for (i = 0; i < coords.length; i += 2) {
			this.visible_coords.push(this.drawBar(coords[i], coords[i+1], this.color, this.icon));
		}
	}
}

PlotJax.HBarChart.prototype.drawBar = function(x, y, color, icon) {
}

