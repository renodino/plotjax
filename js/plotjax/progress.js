registerNS("PlotJax");
registerNS("PlotJax.ProgressBar");
/*
 *	given a div, embed a canvas to fill it,
 *	then set the individual shapes as "open" shapes
 *	oriented as "circle", "fwdbar" (to progress from left to right, then
 *	restart at left), or "cyclebar" (to progress from left to right,
 *	then right to left, etc.), with a 
 *	radius (for circle) or length (for bar) of radlen
 */
PlotJax.ProgressBar = function(progdivid, background, shape, color, innercolor, path, radlen, dotsize) {

	this.progdivid = progdivid;
	this.progdiv = document.getElementById(progdivid);
	this.shape = shape;
	this.color = color;
	this.innercolor = innercolor;
	this.dotsize = (dotsize != null) ? dotsize : PlotJax.ProgressBar.SIZE;
	this.dotradius = this.dotsize >> 1;
	this.valid_shapes = {
		"square":true,
		"circle":true,
		"diamond":true,
		"triangle":true
	};

	if (!this.valid_shapes[shape]) {
		alert("Invalid progress bar shape " + shape);
		return null;
	}
	var w = this.progdiv.offsetWidth;
	var h = this.progdiv.offsetHeight;
	if (radlen == null) { radlen = (path == "circle") ? 20 : 50; }
	if (w < radlen) {
		alert("ProgressBar div too narrow.");
		return null;
	}
	this.items = [];
	var i;
	if (path == "circle") {
		if (h < radlen) {
			alert("ProgressBar div too short.");
			return null;
		}
		this.center = [ w >> 1, h >> 1];
		for (i = 0; i < 360; i += 30) {
			var radians = (Math.PI/180) * i;
			this.items.push(Math.round(radlen + ((radlen - this.dotradius - 1) * Math.cos(radians))), 
				Math.round(radlen + ((radlen - this.dotradius - 1) * Math.sin(radians))));
		}
		w = radlen * 2;
		h = radlen * 2;
	}
	else {
		if (h < this.dotsize) {
			alert("ProgressBar div too short.");
			return null;
		}
		this.center = [ (w >> 1), h >> 1];
		var j = radlen - this.dotsize - 1;
		for (i = -(j >> 1); i < (j >> 1); i += this.dotsize + 4) {
			this.items.push((radlen >> 1) + i, this.dotradius + 2);
		}
		if (path == "cyclebar") {
			for (i = this.items.length - 4; i > 0; i -= 2) {
				this.items.push(this.items[i], this.items[i+1]);
			}
		}
		w = radlen + 4;
		h = this.dotsize + 10;
	}
	this.progdiv.style.background = background;
	this.progdiv.style.zIndex = -1;
	var canvas = document.createElement("canvas");
    this.progdiv.appendChild(canvas);

	canvas.setAttribute("width", w);
	canvas.setAttribute("height", h);
    canvas.setAttribute("id", this.progdivid + "_canvas");
	canvas.style.position = "absolute";
	canvas.style.left = (path == "circle") ? this.center[0] - radlen : (this.center[0] - (radlen >> 1));
	canvas.style.top = (path == "circle") ? this.center[1] - radlen : this.center[1] - 5;
	canvas.style.zIndex = 5;
	
	this.canvas = document.getElementById(this.progdivid + "_canvas");
	this.ctx = canvas.getContext('2d');
	this.curpos = 0;
	this.progdiv.progbar = this;
};

PlotJax.ProgressBar.SIZE = 8;
PlotJax.ProgressBar.RADIUS = 4;

/*
 *	pop the div to the top at the specified location and start the timer
 *	fill the first
 */
PlotJax.ProgressBar.prototype = {
	start : function(x, y, interval) {
		this.progdiv.style.left = x;
		this.progdiv.style.top = y;
		this.interval = interval;
		this.ctx.clearRect(0,0, this.canvas.offsetWidth, this.canvas.offsetHeight);
		for (var i = 0; i < this.items.length; i += 2) {
			this.draw(this.items[i], this.items[i+1], (i != this.curpos));
		}
		this.curpos += 2;
		if (this.curpos >= this.items.length) { this.curpos = 0; }
		this.progdiv.style.zIndex = 2;
		this.progdiv.style.visibility = "visible";
		this.timer = setTimeout("document.getElementById('" + this.progdivid + "').progbar.advance()",interval);
	},

	advance : function() {
		this.ctx.clearRect(0,0, this.canvas.offsetWidth, this.canvas.offsetHeight);
		for (var i = 0; i < this.items.length; i += 2) {
			this.draw(this.items[i], this.items[i+1], (i != this.curpos));
		}
		this.curpos += 2;
		if (this.curpos >= this.items.length) { this.curpos = 0; }
		this.timer = setTimeout("document.getElementById('" + this.progdivid + "').progbar.advance()",this.interval);
	},

	stop : function() {
		clearTimeout(this.timer);
		this.curpos = 0;
		this.progdiv.style.zIndex = -1;
		this.progdiv.style.visibility = "hidden";
	},

	clear : function() {
/*
 *	remove any references that might cause memory leaks
 */
		this.progdiv = null;
		this.ctx = null;
		this.canvas = null;
	},

	draw : function(x, y, isopen) {
		if (!isopen) {	// fill inner 1st
			this.ctx.fillStyle = this.innercolor;
			switch (this.shape) {
				case "square":
					this.ctx.fillRect(
						x - this.dotradius, 
						y - this.dotradius, 
						this.dotsize, 
						this.dotsize);
					break;

				case "circle":
					this.ctx.beginPath();
					this.ctx.arc(x, y, this.dotradius+1, 0, 2 * Math.PI, true);
					this.ctx.closePath();
					this.ctx.fill();
					break;

				case "diamond":
					this.ctx.beginPath();
					this.ctx.moveTo(x, y - this.dotradius);
					this.ctx.lineTo(x - this.dotradius, y);
					this.ctx.lineTo(x, y + this.dotradius);
					this.ctx.lineTo(x + this.dotradius, y);
					this.ctx.closePath();
					this.ctx.fill();
					break;

				case "triangle":
					this.ctx.beginPath();
					this.ctx.moveTo(x, y - this.dotradius);
					this.ctx.lineTo(x - this.dotradius, y + this.dotradius);
					this.ctx.lineTo(x + this.dotradius, y + this.dotradius);
					this.ctx.closePath();
					this.ctx.fill();
					break;

				default:
					return null;
			}
		}
		this.ctx.lineWidth = 2;
		this.ctx.strokeStyle = this.color;
		switch (this.shape) {
			case "square":
				this.ctx.strokeRect(
					x - this.dotradius, 
					y - this.dotradius, 
					this.dotsize, 
					this.dotsize);
				break;
			
			case "circle":
				this.ctx.beginPath();
				this.ctx.arc(x, y, this.dotradius, 0, 2 * Math.PI, true);
				this.ctx.closePath();
				this.ctx.stroke();
				break;

			case "diamond":
				this.ctx.beginPath();
				this.ctx.moveTo(x, y - this.dotradius);
				this.ctx.lineTo(x - this.dotradius, y);
				this.ctx.lineTo(x, y + this.dotradius);
				this.ctx.lineTo(x + this.dotradius, y);
				this.ctx.closePath();
				this.ctx.stroke();
				break;

			case "triangle":
				this.ctx.beginPath();
				this.ctx.moveTo(x, y - this.dotradius);
				this.ctx.lineTo(x - this.dotradius, y + this.dotradius);
				this.ctx.lineTo(x + this.dotradius, y + this.dotradius);
				this.ctx.closePath();
				this.ctx.stroke();
				break;
		}
	}
};
