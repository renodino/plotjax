if (window.PlotJax.PointGraph == null) {

registerNS("PlotJax.PointGraph");
registerNS("PlotJax.LineGraph");
registerNS("PlotJax.AreaGraph");

PlotJax.PointGraph = function(chartdesc, plot, limits, shapes) {
	if (arguments.length > 0) {
		this.plot = plot;
		this.chartdesc = chartdesc;
		this.shapes = shapes;
		this.points = [];
		this.visible_points = [];
		this.zooms = [];
		this.undos = [];
		this.redos = [];
		this.balloon = plot.getBalloon();
		this.gesture = plot.getGesture();
		this.xType = plot.getXType();
		this.yType = plot.getYType();
		this.limits = limits;
		this.myid = chartdesc.PlotID;
		this.helper = plot.getHelper();
		this.chartstyles = (chartdesc.Style == null) ? [ ] : 
			(chartdesc.Style instanceof Array) ? chartdesc.Style :
			[ chartdesc.Style ];
		var i;
//
//	validate styling
//
		var chartstyles = this.chartstyles;
		for (i = 0; i < chartstyles.length; i++) {
			var chartstyle = chartstyles[i];
			if (chartstyle.Shape == null) {
				chartstyle.Shape = 'circle';
			}
			else if (!shapes.isValidShape(chartstyle.Shape, chartstyle.Icon)) {
				if (chartstyle.Shape == "icon") {
					alert("Unknown icon " + chartstyle.Icon);
				}
				else {
					alert("Unknown shape " + chartstyle.Shape);
				}
				return null;
			}

// alert("in ptgraph get color");
			if (chartstyle.Color == null) {
				chartstyle.Color = "black";
			}
		}		
//
//	collect points, mins, and max's
//
		var chartdata = chartdesc.Data;
		var j, x, y;
		if ((chartdata == null) || (chartdata.length == 0)) {
			if ((limits[1] == null) || (limits[0] == null) ||
				(limits[3] == null) || (limits[2] == null)) {
				alert("No data provided, and incomplete limits specified.");
				return null;
			}
		}
		else {
// alert("in ptgraph set datapts");
			for (i = 0; i < chartdata.length; i++) {
				var datapt = chartdata[i];
				if (datapt[0] == null) {
					alert("Missing domain value at element " + i);
					return null;
				}
				x = this.xType.normalize(datapt[0]);
				if ( ( (limits[1] != null) && (x > limits[1])) ||
					((limits[0] != null) && (x < limits[0]))) {
					continue;
				}
				for (j = 1; j < datapt.length; j++) {
//
//	we tolerate missing range values
//
					if (datapt[j] == null) {
						continue;
					}
					y = this.yType.normalize(datapt[j]);
/*
 *	filter data points outside limits
 */
//  alert("in ptgraph set x as " + x);
					if (((limits[3] != null) && (y > limits[3])) ||
						((limits[2] != null) && (y < limits[2]))) {
						continue;
					}
//
//	make sure our point cache has enough ranges
//
					while (this.points.length < j) {
						this.points.push([]);
					}
					this.points[j-1].push([x,y]);
				} // end for each range
			}	// end for each domain value
		}
/*
 *	points are maintained in domain ascending order
 *	(makes rendering of linegraphs easier)
 */
		for (i = 0; i < this.points.length; i++) {
			this.points[i] = this.points[i].sort(this.sortByDomain);
		}
		while (this.visible_points.length < this.points.length) {
			this.visible_points.push([]);
		}
//
//	note: we record only the index of a visible point, not a copy of the data
//
		for (i = 0; i < this.points.length; i++) {
			for (j = 0; j < this.points[i].length; j++) {
				this.visible_points[i].push(j);
			}
		}
//
//	setup as a gesture observer
//
		if (this.gesture != null) {
			this.gesture.observe(this);
		}
	}
};

PlotJax.PointGraph.prototype = {
	getChartID : function() { return this.myid; },

	getElement : function(elemid) { 
		var range = elemid[0];
		var i = elemid[1];
		return (range < this.points.length) ? 
			[ this.xType.display(this.points[range][i][0]), 
			this.yType.display(this.points[range][i][1]) ] :
			null; 
	},

	getElements : function(range) { 
		return (range != null) ?
			((range >= this.points.length) ?  null : this.points[range]) : this.points; 
	},
/*
 *	must return the number of visible datapts
 */
	layout : function(sticky, limits) {
		if ((this.helper != null) && (this.helper.onlayout != null)) {
			this.helper.onlayout(this.plot, this);
		}
/*
 *	determine bounds of each dimension;
 *	size/intensity aren't computed until redraw time
 */
		var min_x = Number.MAX_VALUE;
		var max_x = Number.MIN_VALUE;
		var min_y = Number.MAX_VALUE;
		var max_y = Number.MIN_VALUE;
		var i,j, count = 0;
		for (i = 0; i < this.visible_points.length; i++) {
			for (j = 0; j < this.visible_points[i].length; j++) {
				var dimensions = this.points[i][this.visible_points[i][j]];
				max_x = Math.max(max_x, dimensions[0]);
				min_x = Math.min(min_x, dimensions[0]);
				max_y = Math.max(max_y, dimensions[1]);
				min_y = Math.min(min_y, dimensions[1]);
			}
			count += j;
		}
/*
 *	merge our limits with the global limits
 */
		limits[0] = (limits[0] == null) ? min_x : Math.min(min_x, limits[0]);
		limits[1] = (limits[1] == null) ? max_x : Math.max(max_x, limits[1]);
		limits[2] = (limits[2] == null) ? min_y : Math.min(min_y, limits[2]);
		limits[3] = (limits[3] == null) ? max_y : Math.max(max_y, limits[3]);
		return count;
	},

	sortVisibles : function() {
		var count;
		for (var i = 0; (i < this.visible_points.length) ; i++) { 
			if (this.visible_points[i].length != 0) {
//
//	this works cuz we enforce the sort order on the master points array
//
				this.visible_points[i] = this.visible_points[i].sort(this.sortByIndex);
				count++;
			}
		}
		return count;
	},
/*
 * draw points
 */
	redraw : function(sticky) {
		if ((this.helper != null) && (this.helper.ondraw != null)) {
			this.helper.ondraw(this.plot, this);
		}
		if (this.sortVisibles() == 0) { return; }	// nothing to do

		var i, j, k, x, y;
		this.visible_coords = [];
		var lm = this.plot.getLeftMargin();
		var ve = this.plot.getVertEdge();
		var color, shape, icon;
		k = 0;
		for (i = 0; i < this.visible_points.length; i++) {
			this.visible_coords.push([]);
			var style = this.chartstyles[k++];
			if (k == this.chartstyles.length) { k = 0; }
			for (j = 0; j < this.visible_points[i].length; j++) {
				x = this.xType.pt2pxl(this.points[i][this.visible_points[i][j]][0], lm);
				y = ve - this.yType.pt2pxl(this.points[i][this.visible_points[i][j]][1], 0);
				this.visible_coords[i].push(this.shapes.draw(x, y, style.Color, style.Shape, style.Icon));
			}
		}
	},

	sortByDomain : function(a,b) {
		return ((a == null) || (b == null)) ? 0 : a[0] - b[0];
	},
	sortByIndex : function(a,b) { return a - b; },

	redo : function() {
		var redo = this.redos.pop();
		var temp = [];
		var i,j,k;
		for (i = 0; i < this.visible_points.length; i++) {
			for (j = this.visible_points[i].length - 1; (i >= 0) && (redo.length > 0); i--) {
				var id = this.visible_points[i][j];
/*
 *	locate the point in the redo list; if found, remove it and push
 *	into undo list
 */
				for (k = 0; (k < redo.length) && ((redo[k][0] != i) || (redo[k][1] != id)); k++) {}
				if (k < redo.length) {
					this.visible_points[i].splice(j, 1);
					temp.push([i, id]);
					redo.splice(k, 1);
				}
			}
		}
		this.undos.push(temp);
	},

	undo : function() {
		var redo = this.undos.pop();
		for (var i = 0; i < redo.length; i++) {
			this.visible_points[redo[i][0]].push(redo[i][1]);
		}
		this.redos.push(redo);
	},
/*
 *	 discard a point (no undo)
 */
	discardElement : function(range, id) {
		if ((range < this.points.length) && (this.points[range].length < id)) {
			this.points[range][id] = null;
		}
		return this;
	},
/*
 *	return the range index for a given range name
 */
	getRangeByName : function(name) {
		for (var i = 0; (i < this.chartstyle.length); i++) {
			if ((this.chartstyle[i].Name != null) && (this.chartstyle[i].Name == name)) {
				return i;
			}
		}
		return null;
	},
/*
 *	return the range name for a given range index
 */
	getNameByRange : function(range) {
		return (range < this.chartstyle.length) ? this.chartstyle[range].Name : null;
	},
/*
 *	recoverably hide points in the ids list
 *	input is a list of range, id pairs, an optimization to avoid
 *	multiple call overhead and add'l undo list mgmt
 */
	hideElements : function(ids) {
		var id;
		var undos = [];
		var i,j,k;
/*
 *	traverse in reverse order so we don't mess up the indexing when we splice
 */
		for (i = 0; (ids.length > 0) && (i < this.visible_points.length); i++) {
			for (j = this.visible_points[i].length - 1; (ids.length > 0) && (j >= 0) ; j--) {
				id = this.visible_points[i][j];
				for (k = 0; (k < ids.length) && ((i != ids[k]) || (id != ids[k+1])); k+=2) {}
				if (k < ids.length) {
					this.visible_points[i].splice(j, 1);
					undos.push(i,id);
					ids.splice(k, 2);	// don't need to look at this id again
				}
			}
		}
		if (undos.length > 0) { this.undos.push(undos); }
		return this;
	},
/*
 *	recoverably hide points that are NOT in the specified list
 *	(ids is a list of range, id pairs)
 */
	keepElements : function(ids) {
		var id;
		var undos = [];
		var i,j,k;
		for (i = 0; (ids.length > 0) && (i < this.visible_points.length); i++) {
			for (j = this.visible_points[i].length - 1; (ids.length > 0) && (j >= 0) && (ids.length > 0); j--) {
				id = this.visible_points[i][j];
				for (k = 0; (k < ids.length) && ((i != ids[k]) || (id != ids[k+1])); k+=2) {}
				if (k == ids.length) {	// not in keep list
					this.visible_points[i].splice(j, 1);
					undos.push(i,id);
				}
				else {
					ids.splice(k, 2); 	// don't need to look at it again
				}
			}
			while (j >= 0) {
/*
 *	all remaining visible were not in keeplist, so remove them
 */
				this.undos.push(this.visible_points[i][j]);
				this.visible_points[i].splice(j, 1);
				j--;
			}
		}
		if (undos.length > 0) { this.undos.push(undos); }
		return this;
	},
/*
 *	called by Gesture on an event; it will scan the visibles
 *	to locate any effected elements; note that we return a copy
 *	returned list is (range, id) pairs
 */
	getVisibleElements : function() {
		var elems = [];
		for (var i = 0; i < this.visible_points.length; i++) {
			for (var j = 0; j < this.visible_points[i].length; j++) {
				elems.push(i,j);
			}
		}
		return elems;
	},
/*
 *	called by a click in the chart; check if any elements
 *	contain the specified coordinate; if so, add the element
 *	[ chart, id ] pair to a returned list, along with the
 *	associated hover text
 */
	onclick : function(chartx, charty) {
		var clicked_pts = [ ];
		var i,j, xdist, ydist;
		for (i = 0; i < this.visible_points.length; i++) {
			var chartstyle = chartstyles[i];
			var coords = this.visible_coords[i];
			for (j = 0; j < this.visible_points[i].length; j++) {
				xdist = (coords[j][0] - chartx);
				ydist = (coords[j][1] - charty);
				if (Math.sqrt((xdist * xdist) + (ydist * ydist)) <= coords[j][2]) {
					clicked_pts.push( [i, this.visible_points[i][j]] );
				}
			}
		}

		return (clicked_pts.length == 0) ? null : clicked_pts;
	},
	
	drawAt : function (canvas, x, y, peepdiam) {
		for (var i = 0; i < elements.length; i += 3) {
			var style = this.chartStyles[elements[i][0]];
			this.shapes.draw(elements[i+1], elements[i+2], 
				style.Color, style.Shape, style.Icon, canvas, peepdiam);
		}
	},
/*
 *	called by hovering in the chart; check if any elements
 *	contain the specified coordinate; if so, fetch hover content from helper
 */
	onhover : function(x, y) {
		var clicked_bubbles = [];
		var i,j, xdist, ydist;
		for (i = 0; i < this.visible_points.length; i++) {
			var coords = this.visible_coords[i];
			for (j = 0; j < this.visible_points[i].length; j++) {
				xdist = (coords[j][0] - x);
				ydist = (coords[j][1] - y);
				if (Math.sqrt((xdist * xdist) + (ydist * ydist)) <= coords[j][2]) {
					return this.helper.onhover(this.plot, this, [i,j]);
				}
			}
		}
		return null;
	},

	clear : function() {
/*
 *	delete any members that might cause memory leaks
 */
		this.points = null;
		this.visible_points = null;
		this.visible_coords = null;
		this.plot = null;
		this.helper = null;
		this.balloon = null;
		this.gesture = null;
		this.zooms = null;
		this.redos = null;
		this.undos = null;
	},
/*
 *	observe a zoom event
 *		input is array of zoom objects that implement contains()
 *		return a list of element IDs that are contained 
 *		in the zoom. contains() takes the x,y of the
 *		center of an element
 */
	observeZoom : function(zooms) {
		var contained_points = [];
		var i,j, k;
		for (i = 0; i < this.visible_points.length; i++) {
			var coords = this.visible_coords[i];
			for (j = 0; j < this.visible_points[i].length; j++) {
				for (k = 0; k < zooms.length; k++) {
					if (zooms[k].contains(coords[j][0], coords[j][1])) {
						contained_points.push([i,j]);
					}
				}
			}
		}
		return contained_points;
	},

	observeFlick : function(flick) {
		var flicked_points = [];
		var i,j;
		for (i = 0; i < this.visible_points.length; i++) {
			var coords = this.visible_coords[i];
			for (j = 0; j < this.visible_points[i].length; j++) {
				if (flick.intersects(coords[j][0], coords[j][1], 3)) {
					flicked_points.push([i,j]);
				}
			}
		}
		return flicked_points;
	}
};
/*
 *	Linegraph is just a pointgraph with connecting lines;
 *	on zoom, we have to draw to edges if points outside
 *	new chart area
 */
PlotJax.LineGraph = function(chartdesc, plot, limits, shapes) {
	if (arguments.length > 0) {
		var i;
		if (chartdesc.Style == null) {
			chartdesc.Style = [ { "Shape" : "dot", "LineWidth" : 1, "Pattern" : "solid" }];
		}
		else {
			if (!(chartdesc.Style instanceof Array)) {
				chartdesc.Style = [ chartdesc.Style ];
			}
			for (i = 0; i < chartdesc.Style.length; i ++) {
				if (chartdesc.Style[i].Shape == null) {
					chartdesc.Style[i].Shape = "dot";
				}
				if (chartdesc.Style[i].LineWidth == null) {
					chartdesc.Style[i].LineWidth = 1;
				}
				if (chartdesc.Style[i].Pattern == null) {
					chartdesc.Style[i].Pattern = "solid";
				}
			}
		}
		
		PlotJax.LineGraph.baseConstructor.call(this, chartdesc, plot, limits, shapes);
		this.canvas = plot.getCanvas();
	}
};

JSPPTrustee.extend(PlotJax.LineGraph, PlotJax.PointGraph);

/*
 * draw lines
 */
PlotJax.LineGraph.prototype.redraw = function(sticky) {
	if ((this.helper != null) && (this.helper.ondraw != null)) {
		this.helper.ondraw(this.plot, this);
	}
	if (this.sortVisibles() == 0) { return; }

	this.visible_coords = [];

	var i,j,k;
	var lm = this.plot.getLeftMargin();
	var ve = this.plot.getVertEdge();
/*
 *	draw lines first, then the shapes
 *	NOTE: need to include any preceding or succeding points outside
 *	current chart area!!
 *	NOTE2: we need to support 3D lines via gradient effect
 */
 	k = 0;
 	for (i = 0; i < this.visible_points.length; i++) {
		this.visible_coords.push([]);
	 	var style = this.chartstyles[k++];
	 	if (k == this.chartstyles.length) { k = 0; }
		var coords = [];
		for (j = 0; j < this.visible_points[i].length; j++) {
			var pt = this.points[i][this.visible_points[i][j]];
			coords.push(
				this.xType.pt2pxl(pt[0], lm),
				ve - this.yType.pt2pxl(pt[1], 0)
			);
		}
		this.plot.drawLine(coords, style.Color, style.LineWidth, style.Pattern, false);
		for (j = 0; j < coords.length; j += 2) {
			this.visible_coords[i].push(this.shapes.draw(coords[j], coords[j+1], 
				style.Color, style.Shape, style.Icon));
		}
	}
};

/*
 *	Areagraph is just a linegraph with fill between domain acxis and line;
 *	on zoom, we have to draw to edges if points outside
 *	new chart area
 */
PlotJax.AreaGraph = function(chartdesc, plot, limits, shapes) {
	PlotJax.AreaGraph.baseConstructor.call(this, chartdesc, plot, limits, shapes);
};

JSPPTrustee.extend(PlotJax.AreaGraph, PlotJax.LineGraph);

/*
 * draw area
 */
PlotJax.AreaGraph.prototype.redraw = function(sticky) {
	if ((this.helper != null) && (this.helper.ondraw != null)) {
		this.helper.ondraw(this.plot, this);
	}
	if (this.sortVisibles() == 0) { return; }
	this.visible_coords = [];

	var i,j,k;
	var lm = this.plot.getLeftMargin();
	var ve = this.plot.getVertEdge();
/*
 *	draw lines first, then the shapes
 *	NOTE: need to include any preceding or succeding points outside
 *	current chart area!!
 *	NOTE2: we need to support 3D lines via gradient effect
 */
 	k = 0;
	for (i = 0; i < this.visible_points.length; i++) {
		this.visible_coords.push([]);
	 	var style = this.chartstyles[k++];
	 	if (k == this.chartstyles.length) { k = 0; }
		var coords = [];
		for (j = 0; j < this.visible_points[i].length; j++) {
			var pt = this.points[i][this.visible_points[i][j]];
			coords.push(
				this.xType.pt2pxl(pt[0], lm),
				ve - this.yType.pt2pxl(pt[1], 0)
			);
		}
/*
 *	need drawPoly() here; also need to break this up if it cross axis, or
 *	drops outside of the clipping region
 */
		this.plot.drawPolygon(coords, style.Color, style.LineWidth, style.Pattern);
		for (j = 0; j < coords.length; j += 2) {
			this.visible_coords[i].push(this.shapes.draw(coords[j], coords[j+1], 
				style.Color, style.Shape, style.Icon));
		}
	}
};

PlotJax.LineFit = function(chartdesc, plot, limits, shapes) {
	if (arguments.length > 0) {
		var i;
		if (chartdesc.Style == null) {
			chartdesc.Style = [ { "Shape" : "dot", "LineWidth" : 1, "Pattern" : "solid" }];
		}
		else {
			if (!(chartdesc.Style instanceof Array)) {
				chartdesc.Style = [ chartdesc.Style ];
			}
			for (i = 0; i < chartdesc.Style.length; i ++) {
				if (chartdesc.Style[i].Shape == null) {
					chartdesc.Style[i].Shape = "dot";
				}
				if (chartdesc.Style[i].LineWidth == null) {
					chartdesc.Style[i].LineWidth = 1;
				}
				if (chartdesc.Style[i].Pattern == null) {
					chartdesc.Style[i].Pattern = "solid";
				}
			}
		}
		
		PlotJax.LineFit.baseConstructor.call(this, chartdesc, plot, limits, shapes);
		this.canvas = plot.getCanvas();
	}
};

JSPPTrustee.extend(PlotJax.LineFit, PlotJax.LineGraph);

PlotJax.LineFit.prototype.calc = function() {
	var xs=0, ys=0, x2s=0, y2s=0, xys=0;

	var datapts = this.chartdesc.Data;
	for (var i = 0; i < datapts.length; i++) {
		xs += datapts[i][0];
		ys += datapts[i][1];
		x2s += datapts[i][0] * datapts[i][0];
		y2s += datapts[i][1] * datapts[i][1];
		xys += datapts[i][0] * datapts[i][1];
	}	
	
	var count = datapts.length;
	this.intercept = (ys*x2s - xys*xs)/(count*x2s - xs*xs);
	this.slope = (count*xys - xs*ys)/(count*x2s - xs*xs);
	this.correlation = (count*xys - xs*ys)/Math.sqrt((count*x2s-xs*xs)*(count*y2s-ys*ys));
	this.correlation = this.correlation * this.correlation;
};

}