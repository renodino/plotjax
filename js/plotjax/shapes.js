
if (window.PlotJax.Shapes == null) {

registerNS("PlotJax.Shapes");

PlotJax.Shapes = function(plot, chartctx) {
	this.plot = plot;
	this.ctx = chartctx;
	this.icons = [];
	this.valid_shapes = {
		"square":true,
		"fillsquare":true,
		"opensquare":true,
		"circle":true,
		"fillcircle":true,
		"opencircle":true,
		"diamond":true,
		"filldiamond":true,
		"opendiamond":true,
		"triangle":true,
		"filltriangle":true,
		"opentriangle":true,
		"horizcross":true,
		"diagcross":true,
		"dot":true,
		"icon":true
	};
};

PlotJax.Shapes.SIZE = 8;
PlotJax.Shapes.RADIUS = 4;

PlotJax.Shapes.prototype = {
	addIcon : function(iconname, img) { this.icons[iconname] = img; },
	getIcon : function(iconname) { return this.icons[iconname]; },

	isValidShape : function(shape, iconname) { 
		return (shape == "icon") ? (this.icons[iconname] != null) : (this.valid_shapes[shape] != null);
	},

	getDimensions : function(shape, iconname) {
		return (shape == "icon") ? 
			[ this.icons[iconname].width, this.icons[iconname].height ] :
			[ PlotJax.Shapes.SIZE, PlotJax.Shapes.SIZE ];
	},
	
	clear : function() {
/*
 *	remove any references that might cause memory leaks
 */
		this.plot = null;
		this.ctx = null;
		this.icons = null;
	},

	draw : function(x, y, color, shape, iconname, canvas, maxdiam) {
		if (canvas == null) {
			canvas = this.ctx;
		}
		switch (shape) {
			case "icon":
				if (this.icons[iconname] == null) { return null; }
				var w = this.icons[iconname].width;
				var h = this.icons[iconname].height;
				if ((maxdiam != null) && ((maxdiam > w) || (maxdiam > h))) {
					var scale = maxdiam/Math.max(w, h);
					w = Math.floor(w * scale);
					h = Math.floor(h * scale);
				}
				canvas.drawImage(this.icons[iconname], x - (w>>1), y - (h>>1));
				return [ x, y, Math.max(w, h) ];

			case "dot":
				canvas.fillStyle = color;
				canvas.fillRect(x, y, 1, 1);
				return [ x, y, 1 ];

			case "star":
			case "openstar":
				return null;

			case "square":
			case "fillsquare":
				canvas.fillStyle = color;
				canvas.fillRect(
					x - PlotJax.Shapes.RADIUS, 
					y - PlotJax.Shapes.RADIUS, 
					PlotJax.Shapes.SIZE, 
					PlotJax.Shapes.SIZE);
				break;

			case "opensquare":
				canvas.strokeStyle = color;
				canvas.strokeRect(
					x - PlotJax.Shapes.RADIUS, 
					y - PlotJax.Shapes.RADIUS, 
					PlotJax.Shapes.SIZE, 
					PlotJax.Shapes.SIZE);
				break;
			
			case "circle":
			case "fillcircle":
				canvas.fillStyle = color;
				canvas.beginPath();
				canvas.arc(x, y, PlotJax.Shapes.RADIUS, 0, 2 * Math.PI, false);
				canvas.closePath();
				canvas.fill();
				break;

			case "opencircle":
				canvas.strokeStyle = color;
				canvas.beginPath();
				canvas.arc(x, y, PlotJax.Shapes.RADIUS, 0, 2 * Math.PI, false);
				canvas.closePath();
				canvas.stroke();
				break;

			case "diamond":
			case "filldiamond":
				canvas.fillStyle = color;
				canvas.beginPath();
				canvas.moveTo(x, y - PlotJax.Shapes.RADIUS);
				canvas.lineTo(x - PlotJax.Shapes.RADIUS, y);
				canvas.lineTo(x, y + PlotJax.Shapes.RADIUS);
				canvas.lineTo(x + PlotJax.Shapes.RADIUS, y);
				canvas.closePath();
				canvas.fill();
				break;

			case "opendiamond":
				canvas.strokeStyle = color;
				canvas.beginPath();
				canvas.moveTo(x, y - PlotJax.Shapes.RADIUS);
				canvas.lineTo(x - PlotJax.Shapes.RADIUS, y);
				canvas.lineTo(x, y + PlotJax.Shapes.RADIUS);
				canvas.lineTo(x + PlotJax.Shapes.RADIUS, y);
				canvas.closePath();
				canvas.stroke();
				break;

			case "triangle":
			case "filltriangle":
				canvas.fillStyle = color;
				canvas.beginPath();
				canvas.moveTo(x, y - PlotJax.Shapes.RADIUS);
				canvas.lineTo(x - PlotJax.Shapes.RADIUS, y + PlotJax.Shapes.RADIUS);
				canvas.lineTo(x + PlotJax.Shapes.RADIUS, y + PlotJax.Shapes.RADIUS);
				canvas.closePath();
				canvas.fill();
				break;

			case "opentriangle":
				canvas.strokeStyle = color;
				canvas.beginPath();
				canvas.moveTo(x, y - PlotJax.Shapes.RADIUS);
				canvas.lineTo(x - PlotJax.Shapes.RADIUS, y + PlotJax.Shapes.RADIUS);
				canvas.lineTo(x + PlotJax.Shapes.RADIUS, y + PlotJax.Shapes.RADIUS);
				canvas.closePath();
				canvas.stroke();
				break;

			case "horizcross":
				canvas.strokeStyle = color;
				canvas.beginPath();
				canvas.moveTo(x, y - PlotJax.Shapes.RADIUS);
				canvas.lineTo(x, y + PlotJax.Shapes.RADIUS);
				canvas.closePath();
				canvas.stroke();
				canvas.beginPath();
				canvas.moveTo(x - PlotJax.Shapes.RADIUS, y);
				canvas.lineTo(x + PlotJax.Shapes.RADIUS, y);
				canvas.closePath();
				canvas.stroke();
				break;

			case "diagcross":
				canvas.strokeStyle = color;
				canvas.beginPath();
				canvas.moveTo(x - PlotJax.Shapes.RADIUS, y - PlotJax.Shapes.RADIUS);
				canvas.lineTo(x + PlotJax.Shapes.RADIUS, y + PlotJax.Shapes.RADIUS);
				canvas.closePath();
				canvas.stroke();
				canvas.beginPath();
				canvas.moveTo(x + PlotJax.Shapes.RADIUS, y - PlotJax.Shapes.RADIUS);
				canvas.lineTo(x - PlotJax.Shapes.RADIUS, y + PlotJax.Shapes.RADIUS);
				canvas.closePath();
				canvas.stroke();
				break;

			default:
				return null;
		}
		return [ x, y, PlotJax.Shapes.RADIUS ];
	}
};

}