
registerNS("PlotJax.BoxChart");

PlotJax.BoxChart = function(chartdesc, plot, limits, shapes) {
	if (arguments.length > 0) {
		this.plot = plot;
		this.chartdesc = chartdesc;
		this.shapes = shapes;
		this.points = [];
		this.visible_points = [];
		this.zooms = [];
		this.undos = [];
		this.redos = [];
		this.balloon = plot.getBalloon();
		this.gesture = plot.getGesture();
		this.xType = plot.getXType();
		this.yType = plot.getYType();
		this.limits = limits;
		this.myid = chartdesc.PlotID;
		this.helper = plot.getHelper();

		if (chartdesc.Shape == null) {
			this.shape = 'circle';
		}
		else if (!shapes.isValidShape(chartdesc.Shape, chartdesc.Icon)) {
			alert("Unknown shape " + chartdesc.Shape);
			return null;
		}
		else {
			this.shape = chartdesc.Shape;
			this.icon = chartdesc.Icon;
		}

		this.color = (chartdesc.Colors != null) ? chartdesc.Colors[0] : "#000000";

		var chartdata = chartdesc.Data;
		if ((chartdata == null) || (chartdata.length == 0)) {
			if ((limits[1] == null) || (limits[0] == null) ||
				(limits[3] == null) || (limits[2] == null)) {
				alert("No data provided, and incomplete limits specified.");
				return null;
			}
		}
		else {
			var i = 0;
			var chartdata = chartdesc.Data;
			for (i = 0; i < chartdata.length; i++) {
				var datapt = chartdata[i].slice(0);
				if (datapt[0] == null) {
					alert("Missing domain value at element " + i);
					return null;
				}
				if (datapt[1] == null) {
					alert("Missing range value at element " + i);
					return null;
				}
				var x = this.xType.normalize(datapt[0]);
				var y = this.yType.normalize(datapt[1]);
/*
 *	filter data points outside limits
 */
				if (((limits[1] != null) && (x > limits[1])) ||
					((limits[0] != null) && (x < limits[0])) ||
					((limits[3] != null) && (y > limits[3])) ||
					((limits[2] != null) && (y < limits[2]))) {
					continue;
				}

				this.points.push([x,y]);
			}
/*
 *	points are maintained in domain ascending order
 *	(makes rendering of linegraphs easier)
 */
			this.points = this.points.sort(this.sortByDomain);
			for (i = 0; i < this.points.length; i++) {
				this.visible_points.push(i);
			}
		}
//
//	setup as a gesture observer
//
		this.gesture.observe(this);
	}
}

PlotJax.BoxChart.prototype.getChartID = function() { return this.myid; }

PlotJax.BoxChart.prototype.addPoint = function(x,y) {
	this.points.push([x,y]);
	return true;
};

PlotJax.BoxChart.prototype.getElement = function(i) { return this.points[i]; };

PlotJax.BoxChart.prototype.getElements = function() { return this.points; };

PlotJax.BoxChart.prototype.layout = function(sticky, limits) {
	if ((this.helper != null) && (this.helper.onlayout != null)) {
		this.helper.onlayout(this.plot, this);
	}
/*
 *	determine bounds of each dimension;
 *	size/intensity aren't computed until redraw time
 */
	var min_x = Number.MAX_VALUE;
	var max_x = Number.MIN_VALUE;
	var min_y = Number.MAX_VALUE;
	var max_y = Number.MIN_VALUE;

	var i;
	for (i = 0; i < this.visible_points.length; i++) {
		var dimensions = this.visible_points[i];
		max_x = Math.max(max_x, dimensions[0]);
		min_x = Math.min(min_x, dimensions[0]);
		max_y = Math.max(max_y, dimensions[1]);
		min_y = Math.min(min_y, dimensions[1]);
	}
/*
 *	merge our limits with the global limits
 */
	limits[0] = Math.min(min_x, limits[0]);
	limits[1] = Math.max(max_x, limits[1]);
	limits[2] = Math.min(min_y, limits[2]);
	limits[3] = Math.max(max_y, limits[3]);
}

/*
 * draw points
 */
PlotJax.BoxChart.prototype.redraw = function(sticky) {
	if ((this.helper != null) && (this.helper.ondraw != null)) {
		this.helper.ondraw(this.plot, this);
	}
	if (this.visible_points.length == 0) {
		return;
	}
	this.visible_points = this.visible_points.sort(this.sortByIndex);
	this.visible_coords = [];

	var i;
	var lm = this.plot.getLeftMargin();
	var ve = this.plot.getVertEdge();
	var x, y;
	for (i = 0; i < this.visible_points.length; i++) {
		x = this.xType.pt2pxl(this.points[this.visible_points[i]][0], lm);
		y = ve - yType.pt2pxl(this.points[this.visible_points[i]][1], 0);

		this.visible_coords.push(this.shapes.draw(x, y, this.color, this.shape, this.icon));
	}
}

PlotJax.BoxChart.prototype.sortByDomain = function(a,b) {
	return ((a == null) || (b == null)) ? 0 : a[0] - b[0];
};

PlotJax.BoxChart.prototype.sortByIndex = function(a,b) { return a - b; }

PlotJax.BoxChart.prototype.redo = function() {
	var redo = this.redos.pop();
	var temp = [];
	for (var i = this.visible_points.length - 1; (i >= 0) && (redo.length > 0); i--) {
		var id = this.visible_points[i];
		/*
		 *	locate the point in the redo list; if found, remove it and push
		 *	into undo list
		 */
		for (var j = 0; (j < redo.length) && (redo[j] != id); j++) {}
		if (j < redo.length) {
			this.visible_points.splice(i, 1);
			temp.push(id);
			redo.splice(j, 1);
		}
	}
	this.undos.push(temp);
};

PlotJax.BoxChart.prototype.undo = function() {
	var redo = this.undos.pop();
	for (var i = 0; i < redo.length; i++) {
		this.visible_points.push(redo[i]);
	}
	this.redos.push(redo);
};
/*
 *	 discard a point (no undo)
 */
PlotJax.BoxChart.prototype.discardElement = function(id) {
	if (this.points.length < id) {
		this.points[id] = null;
	}
	return this;
};

/*
 *	recoverably hide points in the ids list
 *	input is a list of ids, an optimization to avoid
 *	multiple call overhead and add'l undo list mgmt
 */
PlotJax.BoxChart.prototype.hideElements = function(ids) {
	var id;
	var undos = [];
	var i,j;
/*
 *	traverse in reverse order so we don't mess up the indexing when we splice
 */
	for (i = this.visible_points.length - 1; i >= 0 ; i--) {
		id = this.visible_points[i];
		for (j = 0; (j < ids.length) && (id != ids[j]); j++) {}
		if (j < ids.length) {
			this.visible_points.splice(i, 1);
			undos.push(id);
			ids.splice(j, 1);	// don't need to look at this id again
		}
	}
	if (undos.length > 0) {
		this.undos.push(undos);
	}
	return this;
};

/*
 *	recoverably hide points that are NOT in the specified list
 */
PlotJax.BoxChart.prototype.keepElements = function(ids) {
	var id;
	var undos = [];
	var i,j;
	for (i = this.visible_points.length - 1; (i >= 0 ) && (ids.length > 0); i--) {
		id = this.visible_points[i];
		for (j = 0; (j < ids.length) && (id != ids[j]); j++) {}
		if (j == ids.length) {	// not in keep list
			this.visible_points.splice(i, 1);
			undos.push(id);
		}
		else {
			ids.splice(j, 1); 	// don't need to look at it again
		}
	}
	while (i >= 0) {
/*
 *	all remaining visible were not in keeplist, so remove them
 */
		this.undos.push(this.visible_points[i]);
		this.visible_points.splice(i, 1);
		i--;
	}
	if (undos.length > 0) {
		this.undos.push(undos);
	}
	return this;
};

/*
 *	called by Gesture on an event; it will scan the visibles
 *	to locate any effected elements; note that we return a copy
 */
PlotJax.BoxChart.prototype.getVisibleElements = function() {
	return this.visible_points.slice(0);
};

/*
 *	called by a click in the chart; check if any elements
 *	contain the specified coordinate; if so, open a balloon
 *	and return true
 *	Note we may eventually need to collect overlapping pts
 */
PlotJax.BoxChart.prototype.onclick = function(chartx, charty, windowx, windowy) {
	var clicked_pts = [];
	var i, xdist, ydist;
	for (i = 0; i < this.visible_points.length; i++) {
		xdist = (this.visible_coords[i][0] - chartx);
		ydist = (this.visible_coords[i][1] - charty);
		
		if (Math.sqrt((xdist * xdist) + (ydist * ydist)) <= this.visible_coords[i][2]) {
			clicked_pts.push(this.visible_points[i]);
		}
	}

	if (clicked_pts.length > 0) {
		var tooltip = this.helper.getBalloonContent(this.plot, this, clicked_pts[0]);
		if (tooltip != null) {
			this.balloon.openBalloon(windowx, windowy, tooltip);
		}
	}
	return (clicked_pts.length > 0);
}

PlotJax.BoxChart.prototype.clear = function() {
/*
 *	delete any members that might cause memory leaks
 */
	this.points = null;
	this.visible_points = null;
	this.visible_coords = null;
	this.plot = null;
	this.helper = null;
	this.balloon = null;
	this.gesture = null;
	this.zooms = null;
	this.redos = null;
	this.undos = null;
}
