
if (window.PlotJax.Gesture == null) {

registerNS("PlotJax.Zoom");
registerNS("PlotJax.Gesture");

/**
 *	Elliptical zoom area.
 */
PlotJax.Zoom = function(theta, centerpt, minor_r, major_r, zoomctx, leftOffset, topOffset) {
	this.theta = theta;
	this.major_r = major_r;
	this.minor_r = minor_r;
	this.centerpt = centerpt;
	this.zoomctx = zoomctx;
	this.eccentricity = minor_r/major_r;
	this.minx = centerpt[0] - (major_r * Math.sin(theta));
	this.maxx = centerpt[0] + (major_r * Math.sin(theta));
	this.miny = centerpt[1] - (major_r * Math.cos(theta));
	this.maxy = centerpt[1] + (major_r * Math.cos(theta));
	this.major_sq = major_r * major_r;
//
//	need the offset of our zoom canvas to the chart canvas
//	(zoom canvas is fitted over just the chart portion, so
//	we need to add the left margin and vert edge offsets)
//
	this.relcenter = [ centerpt[0] + leftOffset, centerpt[1] + topOffset ];
	this.leftoff = leftOffset;
	this.topoff = topOffset;
//
//	optimization for contains:
//
	this.cos_theta = Math.cos(theta);
	this.sin_theta = Math.sin(theta);
};

PlotJax.Zoom.COLOR = "rgba(200,200,200,0.5)";
PlotJax.Zoom.CLEAR = "rgba(254,254,254,0.0)";

PlotJax.Zoom.prototype = {

	shade : function(color) {
//
//	save context
//	translate axis to origin
//	rotate by -theta (or -theta - PI)
//	scale vertically to minor_axis
//	draw ellipse
//	restore context
//
		this.zoomctx.save();
		this.zoomctx.translate(this.centerpt[0], this.centerpt[1]);
		this.zoomctx.rotate(this.theta);
		this.zoomctx.scale(1, this.minor_r/this.major_r);

		this.zoomctx.beginPath();
		this.zoomctx.fillStyle = color;
		this.zoomctx.strokeStyle = "black";
		this.zoomctx.lineWidth = 2;
		this.zoomctx.arc(0,0, this.major_r, 0, 2 * Math.PI, false);
		this.zoomctx.closePath();
		this.zoomctx.fill();
		this.zoomctx.stroke();
		this.zoomctx.restore();
	},

	redraw : function() { this.shade(PlotJax.Zoom.COLOR); },
/*
 *	does the specified coordinate lie inside this zoom ?
 *	need some fancy trig here to deal with slanted ellipses
 */
	contains : function(x,y,descr) {
		x -= this.leftoff;
		y -= this.topoff;
		var dx = x - this.centerpt[0];
//		var dy = y - this.centerpt[1];
		var dy = this.centerpt[1] - y;
		if (descr != null) {
		this.zoomctx.strokeStyle = "black";
		this.zoomctx.lineWidth = 2;
		this.zoomctx.beginPath();
		this.zoomctx.moveTo(this.centerpt[0], this.centerpt[1]);
		this.zoomctx.lineTo(x, y);
		this.zoomctx.closePath();
		this.zoomctx.stroke();
		}
	//	1. compute angle of line from center to x,y
		var phi = (dx == 0) ? ((dy < 0) ? 1.5 * Math.PI : Math.PI/2) : Math.atan(dy/dx);
		if (dx < 0) {
			phi += Math.PI;
		}
		else if (dy < 0) {
			phi += (2 * Math.PI);
		}
//		alert("dx " + dx + " dy " + dy + " phi " + Math.round(phi * 180/Math.PI));

		phi += this.theta;
		if (phi > 2 * Math.PI) {
			phi -= (2 * Math.PI);
		}
		else if (phi > Math.PI) {
			phi = (2 * Math.PI) - phi;
		}
/*
		var sintheta = Math.sin(this.theta);
		var costheta = Math.cos(this.theta);
	//	2. compute parameteric px,py of point on ellipse at that angle
		var px = (this.major_r * Math.cos(phi) * costheta) - (this.minor_r * Math.sin(phi) * sintheta);
		var py = (this.minor_r * Math.sin(phi) * costheta) + (this.major_r * Math.cos(phi) * sintheta);
*/
		var px = this.major_r * Math.cos(phi);
		var py = this.minor_r * Math.sin(phi);
	
	//	3. compute distance D from center to x,y
		var D = Math.sqrt((dx * dx) + (dy * dy));

	//	4. compute distance P from center to px,py
		var P = Math.sqrt((px * px) + (py * py));
/*
if (descr != null) {
	alert("for element " + descr + "\ncenter at " + this.centerpt.join(",") + 
		"\nelem at angle " + Math.round(phi * 180/Math.PI) + " coords " + x + "," + y + " radius " + D +
		"\nzoom angle " + Math.round(this.theta * 180/ Math.PI) + " radius at " + px + "," + py + " length " + P);
}
*/
	//	5. if D < P, it contains
		return (D < P);
	},

/*
 *	does the specified line intersect this zoom ?
 *	Note: this only applies to flicks intersecting bubbles
 *	or points. Barcharts, pies, etc. require line segment
 *	intersection...and flicking a barchart/pie wedge may not
 *	really make any sense
 */
	intersects : function(cx, cy, r) {
//
//	fudge the radius a bit for big bubbles
//
		if (r > 4) { r *= 0.8; }

//	1. compute distance D of input point from zoom centerpt
		var dx = cx - this.relcenter[0];
		var dy = cy - this.relcenter[1];
		var r_sq = r * r;
		var d_sq = (dx * dx) + (dy * dy);

//	2. if D < radius R, it intersects
		if (d_sq <= r_sq) { return true; }

//	3. if D > Dmajor (distance from center to point R offset from
//		end of major axis), it does not intersect
		var dmajor = this.major_sq + r_sq;
		if (dmajor < d_sq) { return false; }

//	4. compute angle of line segment from centerpt to input pt
		var phi = (dx == 0) ? ((dy < 0) ? -Math.PI/2 : Math.PI/2) : Math.atan(dy/dx);
	
//	5. rotate coord system by zoom's theta (so major axis is horizontal)
//	6. compute Y offset of input pt from center pt; if Y < R, it intersects
//	(but only if the X offset is < major_r)
		d_sq = Math.sqrt(d_sq);
		return ((r >= Math.abs(Math.sin(phi - this.theta) * d_sq)) &&
			(this.major_r >= Math.abs(Math.cos(phi - this.theta) * d_sq)));
	},

	isFlick : function() { return ((this.eccentricity < 0.20) || (this.minor_r < 5)); },
	getCenterPoint : function() { return this.relcenter; },
	getBBox : function() { return [this.minx, this.miny, this.maxx, this.maxy]; }
};

PlotJax.Gesture = function(x, y, w, h, zoom, plot, debugid, chartid) {
	this.left = x;
	this.top = y;
	this.width = this.origwidth = w;
	this.height = this.origheight = h;
	this.plot = plot;
	this.zoomctx = zoom;
	this.observers = [];
	this.zooms = [];
	this.undos = [];
	this.redos = [];
	/*
	 *	gesture direction histograms
	 */
	this.slash = 0;
	this.backslash = 0;
	this.l2r = 0;
	this.r2l = 0;
	this.up = 0;
	this.down = 0;
	/*
	 *	gesture bounding box coords
	 */
	this.max_zoomx = 0;
	this.min_zoomx = 0;
	this.max_zoomy = 0;
	this.min_zoomy = 0;
	/*
	 *	zoom bounding box
	 */
	this.zoom_left = 0;
	this.zoom_rt = 0;
	this.zoom_top = 0;
	this.zoom_bot = 0;
	this.zoom_lastx = 0;
	this.zoom_lasty = 0;
	this.zoom_draw = false;
	/*
	 *	for debug display
	 */
	this.debugdiv = null;
	this.mousex = null;
	this.mousey = null;
	this.mousebtn = null;
	this.minx_out = null;
	this.maxx_out = null;
	this.miny_out = null;
	this.maxy_out = null;
	this.up_out = null;
	this.down_out = null;
	this.left_out = null;
	this.right_out = null;

	this.clickcount = 0;

	if (debugid != null) {
		this.debugdiv = document.getElementById(debugid);
		this.debugdiv.innerHTML =
"<p>\n" +
"<b>For chart " + chartid + "\n" +
"<b>Mouse X: </b><span id='" + debugid + "_mousex' style='font-weight: bold;'></span><br/>\n" +
"<b>Mouse Y: </b><span id='" + debugid + "_mousey' style='font-weight: bold;'></span><br/>\n" +
"<b>Click count: </b><span id='" + debugid + "_clicks' style='font-weight: bold;'></span><br/>\n" +
"<b>Mouse Button: </b><span id='" + debugid + "_mousebtn' style='font-weight: bold;'></span><br/>\n" +
"<b>Min X: </b><span id='" + debugid + "_minx' style='font-weight: bold;'></span><b>; Max X: </b><span id='" + debugid + "_maxx' style='font-weight: bold;'></span><br/>\n" +
"<b>Min Y: </b><span id='" + debugid + "_miny' style='font-weight: bold;'></span><b>; Max Y: </b><span id='" + debugid + "_maxy' style='font-weight: bold;'></span><br/>\n" +
"<b>Up: </b><span id='" + debugid + "_up' style='font-weight: bold;'></span> " +
"<b>Down: </b><span id='" + debugid + "_down' style='font-weight: bold;'></span> " +
"<b>Left: </b><span id='" + debugid + "_left' style='font-weight: bold;'></span> " +
"<b>Right: </b><span id='" + debugid + "_right' style='font-weight: bold;'></span><br/</p>";
		this.mousex = document.getElementById(debugid + "_mousex");
		this.mousey = document.getElementById(debugid + "_mousey");
		this.clicks = document.getElementById(debugid + "_clicks");
		this.mousebtn = document.getElementById(debugid + "_mousebtn");
		this.minx_out = document.getElementById(debugid + "_minx");
		this.maxx_out = document.getElementById(debugid + "_maxx");
		this.miny_out = document.getElementById(debugid + "_miny");
		this.maxy_out = document.getElementById(debugid + "_maxy");
		this.up_out = document.getElementById(debugid + "_up");
		this.down_out = document.getElementById(debugid + "_down");
		this.left_out = document.getElementById(debugid + "_left");
		this.right_out = document.getElementById(debugid + "_right");
		
	}
	else {
		this.debugdiv = null;
	}
};

PlotJax.Gesture.prototype = {

	endDraw : function() {
		if (!this.zoom_draw) { 
//		alert("not enddraw"); 
		this.noclick = false;	// click debouncer
		return false;
		}
		this.zoom_draw = false;
		this.zoom_lastx = null;
		this.zoom_lasty = null;
		this.clickcount++;
		if (this.debugdiv != null) {
			this.mousebtn.innerHTML = "UP";
			this.clicks.innerHTML = "" + this.clickcount;
		}
/*
 *	if no movement, its a mouseclick
 */
		if ((this.up == 0) && (this.down == 0) && (this.l2r == 0) && (this.r2l == 0)) { return; }
		this.noclick = true;	// click debouncer
/*
 *	check if its a single stroke
 */
 		var updown = Math.abs(this.up - this.down);
 		var sideway = Math.abs(this.l2r - this.r2l);
 		var flicked = 
 			((this.up/(this.up + this.down) < 0.01) || (this.up/(this.up + this.down) > 0.9)) ||
 			((this.l2r/(this.l2r + this.r2l) < 0.01) || (this.l2r/(this.l2r + this.r2l) > 0.9));
/*
 * compute zoom shape
 *	create the zoom
 *	clear the zoom outline and redraw all zooms
 */
		var newzoom = this.makeZoom();

		var i = 0;
		if (flicked || newzoom.isFlick()) {
/* 
 * must be a flick:
 * apply gesture to any existing zooms first,
 * if no zooms flicked, try individual elements;
 *	flick is considered in zoom if its center pt
 *	lies inside the zoom
 */
	 		var zoomflicks = false;
	 		var centerpt = newzoom.getCenterPoint();
			for (i = this.zooms.length - 1; i >= 0 ; i--) {
//
//	work backwards, as we'll be splicing zooms out of the list
//
				if ((this.zooms[i].contains(centerpt[0], centerpt[1])) && (this.flickZoom(i) > 0)) {
			 		zoomflicks = true;
		 		}
			}

			if (zoomflicks || (this.flick(newzoom) > 0)) { this.plot.redraw(); }
		}
		else {
//
//	must be a zoom
//
			this.zooms.push(newzoom);
		}
//
//	clear zoom canvas and redraw
//	(to get rid of gesture trace)
//
		this.zoomctx.clearRect(0,0,this.origwidth,this.origheight);
		for (i = 0; i < this.zooms.length; i++) {
			this.zooms[i].redraw();
		}
		return true;
	},

	makeZoom : function() {
/*
 *	compute length/angle of major axis before rendering ellipse
 *	To determine major axis:
 *		compute centerpt of bbox
 *		compute angle and distance of each sample from center pt
 *		set major axis and angle to longest distance
 *		set minor axis to longest orthogonal to major
 */
	 	var centerpt = [ 
	 		(this.max_zoomx + this.min_zoomx) >> 1,
	 		(this.max_zoomy + this.min_zoomy) >> 1
	 	];
	 	var majoridx = 0;
		var major_r = 0;
		var angles = [];
		var euclidian = [];
		var i, xdist, ydist, majortheta;
		for (i = 0; i < this.samples.length; i += 2) {
			xdist = centerpt[0] - this.samples[i];
			ydist = centerpt[1] - this.samples[i+1];
			euclidian.push(Math.sqrt((xdist * xdist) + (ydist * ydist)));
			angles.push((Math.abs(xdist) < 2) ? Math.PI/2 : Math.atan(ydist/xdist));
			if (major_r < euclidian[euclidian.length - 1]) {
				major_r = euclidian[euclidian.length - 1];
				majortheta = angles[angles.length - 1];
				majoridx = angles.length - 1;
			}
		}
/*
 *	then find longest orthogonal to major:
 *	rotate each pt by major theta, then just compute delta from cy
 */
	 	var minor_r = 0;
		for (i = 0; i < angles.length; i++) {
			if (i != majoridx) {
				minor_r = Math.max(minor_r, Math.abs(Math.sin(angles[i] - majortheta) * euclidian[i]));
			}
		}
/*
 *	now generate the ellipse
 *	but first, adjust theta to canvas orientation
 */
		minor_r = Math.round(minor_r);
		major_r = Math.round(major_r);
		return new PlotJax.Zoom(majortheta, centerpt, minor_r, major_r, this.zoomctx, this.left, this.top);
	},

	hasZooms : function() { return (this.zooms.length > 0); },
	clearZooms : function() { 
		this.zooms = [];
		this.zoomctx.clearRect(0,0,this.origwidth,this.origheight);
	},

	reset : function() {
		this.zoom_lastx = null;
		this.zoom_lasty = null;
	},

	clear : function() {
		this.zooms = [];
		this.observers = [];
		this.redos = [];
		this.undos = [];
		this.reset();
		this.zoomctx.clearRect(0,0,this.origwidth,this.origheight);
	},

	startDraw : function() {
		this.zoom_draw = true;
		if (this.debugdiv != null) { this.mousebtn.innerHTML = "DOWN"; }
		this.slash = 0;
		this.backslash = 0;
		this.l2r = 0;
		this.r2l = 0;
		this.up = 0;
		this.down = 0;
		this.min_zoomx = Number.MAX_VALUE;
		this.max_zoomx = Number.MIN_VALUE;
		this.min_zoomy = Number.MAX_VALUE;
		this.max_zoomy = Number.MIN_VALUE;
		this.samples = [];
	},

	updateMouseXY : function(evt) {
/*
 *	add a ugesture (but only if drawing)
 */
	 	if (!this.zoom_draw) { this.startDraw(); }

		var cnvscoords = this.plot.getCanvasCoords();
		var newx,newy;
		if (evt.clientX != null) {
			newx = evt.clientX;
			newy = evt.clientY;
		}
		else {
			newx = evt.pageX;
			newx = evt.pageY;
		}
		var scrollx, scrolly;
		scrollX = window.pageXOffset;
		scrollY = window.pageYOffset;
//
//	adapt to any scrolling
//
		newx += scrollX;
		newy += scrollY;
	
		newx -= (cnvscoords[0] + this.left);
		newy -= (cnvscoords[1] + this.top);
		this.samples.push(newx, newy);
//
//	if over the limit, trim from the front
//
		if (this.samples.length > 400) { this.samples.splice(0,2); }
	 	if (this.zoom_lastx != null) {
	 		this.zoomctx.lineWidth = 2;
	 		this.zoomctx.beginPath();
	 		this.zoomctx.moveTo(this.zoom_lastx, this.zoom_lasty);
	 		this.zoomctx.lineTo(newx, newy);
	 		this.zoomctx.closePath();
	 		this.zoomctx.stroke();
	 	}
/*
 * update histograms
 */
 		if (this.zoom_lastx < newx) { this.l2r += Math.abs(this.zoom_lastx - newx); }
 		else if (this.zoom_lastx > newx) { this.r2l += Math.abs(this.zoom_lastx - newx); }
		if (this.zoom_lasty < newy) { this.down += Math.abs(this.zoom_lasty - newy); }
		else if (this.zoom_lasty > newy) { this.up += Math.abs(this.zoom_lasty - newy); }

		this.min_zoomx = Math.min(newx, this.min_zoomx);
		this.max_zoomx = Math.max(newx, this.max_zoomx);
		this.min_zoomy = Math.min(newy, this.min_zoomy);
		this.max_zoomy = Math.max(newy, this.max_zoomy);
		this.zoom_lastx = newx;
	 	this.zoom_lasty = newy;
		if (this.debugdiv != null) {
			this.up_out.innerHTML = this.up;
			this.down_out.innerHTML = this.down;
			this.left_out.innerHTML = this.left;
			this.right_out.innerHTML = this.right;
			this.minx_out.innerHTML = this.min_zoomx;
			this.maxx_out.innerHTML = this.max_zoomx;
			this.miny_out.innerHTML = this.min_zoomy;
			this.maxy_out.innerHTML = this.max_zoomy;
		}
	},

	observe : function(observer) { this.observers.push(observer); },

	redo : function() {
		if (this.redos.length == 0) { return 0; }
		var redo = this.redos.pop();
		var temp = [];
		for (var i = 0; i < redo.length; i++) {
			redo[i].redo();
		}
		this.undos.push(redo);
		return redo.length;
	},

	undo : function() {
		if (this.undos.length == 0) { return 0; }
		var undo = this.undos.pop();
		for (var i = 0; i < undo.length; i++) {
			undo[i].undo();
		}
		this.redos.push(undo);
		return undo.length;
	},

	zoom : function() {
/*
 *	hide all elements outside of existing zooms
 *	then discard all the zooms
 *	Note that this may cause some observers to have
 *	no visible elements
 *	Returns number of charts impacted; if none, then PlotJax
 *	does not redraw
 *	!!!NOTE: need a helper hook here
 *	also clear all zooms
 */
 		if (this.zooms.length == 0) { return 0; }
 		var undo = [];
 		var keepers = [];
		var i,j, keepcount = 0;
		for (i = 0; i < this.observers.length; i++) {
			var keep = this.observers[i].observeZoom(this.zooms);
/*
 *	always apply keep, even if none are kept, since that
 *	means observer must remove all elements
 */
			undo.push(this.observers[i]);
			keepers.push(keep.slice(0));
			keepcount += keep.length;
		}
		if (keepcount == 0) {
//
//	if not zooming anything, don't do anything
//
			this.zooms = [];
			return 0;
		}
		for (i = 0; i < keepers.length; i++) {
			if (keepers[i].length > 0) {
				this.observers[i].keepElements(keepers[i]);
			}
		}
		this.undos.push(undo);
		this.clearZooms();
		return undo.length;
	},

	flickZoom : function(zoomidx) {
		var i,j;
		var undo = [];
		for (i = 0; i < this.observers.length; i++) {
			var hide = this.observers[i].observeZoom([this.zooms[zoomidx]]);
			if (hide.length > 0) {
				this.observers[i].hideElements(hide);
				undo.push(this.observers[i]);
			}
		}
		if (undo.length > 0) { this.undos.push(undo); }
/*
 *	discard the zoom when done
 *	should we keep them on the undo stack ??? for now, no
 */
		this.zooms.splice(zoomidx,1);
		return undo.length;
	},

	flick : function(flick) {
		var undo = [];
		for (var i = 0; i < this.observers.length; i++) {
			var flicked = this.observers[i].observeFlick(flick);
			if (flicked.length > 0) {
/*
 *	if more than one, we should post balloon with select list
 *	to permit selective hide
 */
				this.observers[i].hideElements(flicked);
				undo.push(this.observers[i]);
			}
		}
		if (undo.length > 0) { this.undos.push(undo); }
		return undo.length;
	},
/*
 *	called from streambox to delete elements
 */
	hide : function(chart, elemid) {
		chart.hideElements([ elemid ]);
		this.undos.push([chart]);
		return 1;
	},
/*
 *	set bounding box for gestures; these are coordinates relative to the
 *	canvas object's bbox. Called by PlotJax to fit the gesture area
 *	over the chart area
 */
	setBBox : function(left, top, right, bottom) {
		this.left = left;
		this.top = top;
		this.right = right;
		this.bottom = bottom;
		this.width = right - left + 1;
		this.height = bottom - top + 1;
	},

	hasUndos : function() { return this.undos.length; },
	hasRedos : function() { return this.redos.length; },
/*
 *	on click, check for zooms; if none, return false
 *	else if click is in a zoom, zoom
 */
	mouseClick : function(chartx, charty) {
		if (this.zoom_draw) { return 0; }
		if (this.noclick) {
			this.noclick = false;
			return 0;
		}
		if (this.zooms.length == 0) {
			return -1;
		}
		for (var i = 0; i < this.zooms.length; i++) {
			if (this.zooms[i].contains(chartx, charty)) {
				this.zoom();
				return 1;
			}
		}
		return -1;
	}
};

}