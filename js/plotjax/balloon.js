if (window.PlotJax.Balloon == null) {

registerNS("PlotJax.StreamBox");
registerNS("PlotJax.Balloon");

PlotJax.StreamBox = function(plot, containerid, top, peepdiam, width, container, helper, zIndex) {
	this.top = top;
	this.peepdiam = peepdiam;
	this.width = width;
	this.curframe = -1;
	this.containerid = containerid;
	this.container = container;
	this.helper = helper;
	this.plot = plot;
	this.zIndex = zIndex || 1;
//
//	compute size of left/right overlays
	var leftw = (width - peepdiam) >> 1;
	var rightw = (width - peepdiam) >> 1;
	this.peepmargin = leftw;
//
//	adjust for scrollbuttons
	leftw -= 18;
	rightw -= 18;
//
//	compute positions of everything
	var leftbtnpos = 0;
	var overlaypos = 18;
	var leftcoverpos = 18;
	var peeppos = leftcoverpos + leftw;
	var rtcoverpos = peeppos + peepdiam + 4;
	var rtbtnpos = rtcoverpos + rightw;
	var deletetop = peepdiam + 4;
	var deleteleft = 0;
	var undotop = peepdiam + 4;
	var undoleft = 18;
	var captiontop = deletetop;
	var captionleft = 36;
	var captionwidth = width - 44;
/*
	var infobox = [ "width", width, "peepdiam", peepdiam, "leftw", leftw, "rightw", rightw, "leftcoverpos", leftcoverpos ];
	alert(infobox.join(", "));
*/
//
//	a container div for everything
  	this.streambox = document.createElement('div');
	this.streambox.setAttribute('id', containerid + "_streambox");
	this.streambox.style.visibility = 'hidden';
	this.streambox.style.position = 'absolute';
	this.streambox.style.left = '0';
	this.streambox.style.top = top;
	this.streambox.style.width = width + "px";
	this.streambox.style.height = (peepdiam + 24) + "px";
	this.streambox.style.overflow = "hidden";
	this.streambox.style.zIndex = zIndex;
  	document.getElementById(containerid).appendChild(this.streambox);
//
//	left scrollbutton
  	var btntop = ((peepdiam + 4) >> 1) - 8;
  	this.leftbtn = document.createElement('div');
	this.leftbtn.setAttribute('id', containerid + "_stream_prev");
	this.leftbtn.style.position = 'absolute';
	this.leftbtn.style.left = leftbtnpos;
	this.leftbtn.style.top = 0;
	this.leftbtn.style.width = "18px";
	this.leftbtn.style.height = peepdiam + "px";
	this.leftbtn.style.background = "white";
	this.leftbtn.style.zIndex = zIndex + 2;
  	this.streambox.appendChild(this.leftbtn);
  	var btnimg = plot.adjustImagePath("imgs/leftarrow16.gif");
	this.leftbtn.innerHTML = "<img id='" + containerid + "_prevbtn' src='" + btnimg + "' style='position: absolute; left: 0; top: " + btntop + ";'>";
 //
 //	canvas
	this.canvas = document.createElement("canvas");
	this.canvas.setAttribute("width", leftw + rightw + peepdiam + 4);
	this.canvas.setAttribute("height", peepdiam);
    this.canvas.setAttribute("id", this.balloonid + "_streambox_canvas");
	this.canvas.style.position = "absolute";
	this.canvas.style.left = 0;
	this.canvas.style.top = 0;
	this.canvas.style.zIndex = zIndex + 1;
	this.canvas.style.marginTop = "0em";
	this.canvas.style.marginLeft = (18 + leftw) + "px";
    this.streambox.appendChild(this.canvas);

	this.canvasctx = this.canvas.getContext("2d");
//
//	left cover
  	this.leftcover = document.createElement('div');
	this.leftcover.setAttribute('id', containerid + "_stream_left");
	this.leftcover.style.position = 'absolute';
	this.leftcover.style.left = leftcoverpos;
	this.leftcover.style.top = 0;
	this.leftcover.style.width = leftw + "px";
	this.leftcover.style.height = (2 + peepdiam) + "px";
	this.leftcover.style.zIndex = zIndex + 10;
	this.leftcover.style.borderTop = "solid black 1px";
	this.leftcover.style.borderBottom = "solid black 1px";
	this.leftcover.style.borderLeft = "solid black 1px";
	this.leftcover.style.background = "white";
	this.leftcover.style.opacity = 0.7;
	this.leftcover.style.filter = "alpha(opacity = 70)";
  	this.streambox.appendChild(this.leftcover);
//
//	peephole
  	this.peephole = document.createElement('div');
	this.peephole.setAttribute('id', containerid + "_stream_peep");
	this.peephole.style.position = 'absolute';
	this.peephole.style.left = peeppos + "px";
	this.peephole.style.top = 0;
	this.peephole.style.width = peepdiam + "px";
	this.peephole.style.height = peepdiam + "px";
	this.peephole.style.zIndex = zIndex + 10;
	this.peephole.style.border = "solid black 2px";
  	this.streambox.appendChild(this.peephole);
//
//	right cover
  	this.rtcover = document.createElement('div');
	this.rtcover.setAttribute('id', containerid + "_stream_right");
	this.rtcover.style.position = 'absolute';
	this.rtcover.style.left = rtcoverpos;
	this.rtcover.style.top = 0;
	this.rtcover.style.width = rightw + "px";
	this.rtcover.style.height = (2 + peepdiam) + "px";
	this.rtcover.style.zIndex = zIndex + 10;
	this.rtcover.style.borderTop = "solid black 1px";
	this.rtcover.style.borderBottom = "solid black 1px";
	this.rtcover.style.borderRight = "solid black 1px";
	this.rtcover.style.background = "white";
	this.rtcover.style.opacity = 0.7;
	this.rtcover.style.filter = "alpha(opacity = 70)";
  	this.streambox.appendChild(this.rtcover);
//
//	right scrollbutton
  	this.rightbtn = document.createElement('div');
	this.rightbtn.setAttribute('id', containerid + "_stream_next");
	this.rightbtn.style.position = 'absolute';
	this.rightbtn.style.left = rtbtnpos;
	this.rightbtn.style.top = 0;
	this.rightbtn.style.width = "18px";
	this.rightbtn.style.height = peepdiam + "px";
	this.rightbtn.style.background = "white";
	this.rightbtn.style.zIndex = zIndex + 3;
  	this.streambox.appendChild(this.rightbtn);
  	btnimg = plot.adjustImagePath("imgs/rightarrow16.gif");
	this.rightbtn.innerHTML = "<img id='" + containerid + "_nextbtn' src='" + btnimg + "' style='position: absolute; left: 2; top: " + btntop + ";'>";
//
//	delete button
  	this.deletebtn = document.createElement('div');
	this.deletebtn.setAttribute('id', containerid + "_stream_delete");
	this.deletebtn.style.position = 'absolute';
	this.deletebtn.style.left = deleteleft;
	this.deletebtn.style.top = deletetop;
	this.deletebtn.style.width = "16px";
	this.deletebtn.style.height = "auto";
	this.deletebtn.style.background = "#FFFFC8";
	this.deletebtn.style.overflow = "hidden";
	this.deletebtn.style.textAlign = "center";
	this.deletebtn.style.border = "solid black 1px";
  	this.streambox.appendChild(this.deletebtn);
  	btnimg = plot.adjustImagePath("imgs/actcross16.gif");
	this.deletebtn.innerHTML = "<img id='" + containerid + "_delbtn' src='" + btnimg + "' alt='Delete'>";
//
//	undo button
  	this.undobtn = document.createElement('div');
	this.undobtn.setAttribute('id', containerid + "_stream_undo");
	this.undobtn.style.position = 'absolute';
	this.undobtn.style.left = undoleft;
	this.undobtn.style.top = undotop;
	this.undobtn.style.width = "16px";
	this.undobtn.style.height = "auto";
	this.undobtn.style.background = "#FFFFC8";
	this.undobtn.style.overflow = "hidden";
	this.undobtn.style.textAlign = "center";
	this.undobtn.style.border = "solid black 1px";
  	this.streambox.appendChild(this.undobtn);
  	btnimg = plot.adjustImagePath("imgs/actundo16.gif");
	this.undobtn.innerHTML = "<img id='" + containerid + "_undobtn' src='" + btnimg + "' alt='Undo'>";
//
//	caption
  	this.caption = document.createElement('div');
	this.caption.setAttribute('id', containerid + "_stream_delete");
	this.caption.style.position = 'absolute';
	this.caption.style.left = captionleft;
	this.caption.style.top = captiontop;
	this.caption.style.width = captionwidth + "px";
	this.caption.style.height = "auto";
	this.caption.style.background = "#FFFFC8";
	this.caption.style.overflow = "hidden";
	this.caption.style.textAlign = "center";
	this.caption.style.border = "solid black 1px";
	this.caption.style.fontSize = "13px";
	this.caption.style.fontFamily = "verdana, arial";
	this.caption.style.fontWeight = "bold";
  	this.streambox.appendChild(this.caption);
//
//	yes, very kludgy, but its GOWI
//
  	document.getElementById(containerid).streambox = this;

  	this.prevbtn = document.getElementById(containerid + "_prevbtn");
  	this.prevbtn.streambox = this;
  	this.prevbtn.onclick = function() { this.streambox.prevFrame(); };

  	this.nextbtn = document.getElementById(containerid + "_nextbtn");
  	this.nextbtn.streambox = this;
  	this.nextbtn.onclick = function() { this.streambox.nextFrame(); };

  	this.delbtn = document.getElementById(containerid + "_delbtn");
  	this.delbtn.streambox = this;
  	this.delbtn.onclick = function() { this.streambox.deleteFrame(); };

  	this.undobtn = document.getElementById(containerid + "_undobtn");
  	this.undobtn.streambox = this;
  	this.undobtn.onclick = function() { this.streambox.restoreFrame(); };
};

PlotJax.StreamBox.prototype = {
	populate : function(elemlist) {
//
//	create initial list of elements as all visible
//	each entry includes the chart, the element, and the caption
//	on delete, entries are removed to the undo list
//
		this.undos = [];
		this.visibles = [];
		for (var i = 0; i < elemlist.length; i += 2) {
			for (var j = 0; j < elemlist[i+1].length; j++) {
//				alert("adding " + elemlist[i] + ":" + elemlist[i+1][j]);
				this.visibles.push([ elemlist[i], elemlist[i+1][j], elemlist[i].getElement(elemlist[i+1][j]).join(",") ]);
			}
		}
		this.streambox.visibility = "visible";
		this.streambox.zIndex = this.zIndex;
		return this.redraw(0);
	},
	
	redraw : function(curframe) {
		var width = (this.visibles.length > 0) ? 
			(this.peepdiam + 4) * this.visibles.length :
			this.peepdiam + 4;
		this.canvas.setAttribute("width", width);
		this.canvasctx.clearRect(0, 0, width, this.peepdiam);
//
//	repopulate the canvas
//
		var x = (this.peepdiam >> 1) + 2;
		var y = (this.peepdiam >> 1) + 2;
		for (i = 0; i < this.visibles.length; i++) {
			this.visibles[i][0].drawAt(this.canvasctx, x, y, this.peepdiam - 4, this.visibles[i][1]);
			x += this.peepdiam + 4;
		}
		this.curframe = curframe;
		return this.changeFrame();
	},

	changeFrame : function() {
		if (this.curframe == -1) {
		//
		//	no visibles left, clear everything
		//
			this.canvas.style.marginLeft = this.peepmargin + "px";
			this.caption.innerHTML = "<i>No elements remaining.</i>";
			this.container.update("<center><b>No element selected.</b></center>");
			return false;
		}
		var margin = this.peepmargin - (this.curframe * (this.peepdiam + 4));
		this.canvas.style.marginLeft = margin + "px";
		this.caption.innerHTML = this.visibles[this.curframe][2];
		this.container.update("<center><i><b>Loading...</b></i></center>");
		var tofunc = "document.getElementById('" + this.containerid + "').streambox.updateContent()";
		this.loadto = setTimeout(tofunc, 300);
		return false;
	},

	updateContent : function() {
//
//	change this to invoke the helper
//
		this.loadto = null;
		var content = (this.helper != null) ?
			this.helper.getBalloonContent(this.plot, this.visibles[this.curframe][0], this.visibles[this.curframe][1]) :
			null;

		if (content == null) {
			content = "The current frame is " + this.curframe + " which should have a caption of\n<pre>\n" +
				this.visibles[this.curframe][2] + "\n</pre></n>";
		}
		this.container.update(content);
		this.container.openFixed(false);
	},

	nextFrame : function() {
		if (this.curframe == 0) { return false; }
		if (this.loadto) { clearTimeout(this.loadto); this.loadto = null; }
		this.curframe--;
		return this.changeFrame();
	},

	prevFrame : function() {
		if (this.curframe == this.visibles.length - 1) { return false; }
		if (this.loadto) { clearTimeout(this.loadto); this.loadto = null; }
		this.curframe++;
		return this.changeFrame();
	},
	
	move : function(left, top) {
		this.streambox.style.left = left;
		this.streambox.style.top = top;
		this.streambox.style.visibility = "visible";
		this.streambox.style.zIndex = this.zIndex;
		return this.streambox.offsetHeight;
	},
	
	deleteFrame : function() {
//
//	remove current frame and reposition the canvas
//
		if (this.visibles.length > 0) {
			var newframe = (this.curframe == this.visibles.length - 1) ? this.curframe - 1 : this.curframe;
			var removed = this.visibles.splice(this.curframe, 1);
			this.undos.push([ this.curframe, removed[0] ]);
			this.redraw(newframe);
		}
		return false;
	},
	
	restoreFrame : function() {
		if (this.undos.length == 0) { return false; }
		var restore = this.undos.pop();
		this.visibles.splice(restore[0], 0, restore[1]);
		if (this.curframe < 0) { this.curframe = 0; }
		return this.redraw(this.curframe);
	},
	
	getHeight : function() { 
		return this.streambox.offsetHeight;
	},
	
	close : function() { 
		if (this.loadto != null) { 
			clearTimeout(this.loadto); 
			this.loadto = null; 
		}
		this.streambox.style.visibility = "hidden";
		this.streambox.style.zIndex = -1;
		this.visibles = null;
		for (var i = 0; i < this.undos.length; i++) {
			var restore = this.undos[i][1];
			this.plot.hide(restore[0], restore[1]);
		}
		this.undos = null;
		return i;
	}

};

PlotJax.Balloon = function(plot, chartid, maxw, maxh, maxZ, helper, peepdiam) {
	this.balloonid = chartid + "_balloon";
	this.centered = false;
	this.plot = plot;
	this.helper = helper;
	this.is_open = false;
/*
 *	add a hidden div to the body
 *	Note that it is not constrained to the chart canvas area
 */
 	if ((maxw == null) || (maxw < 100)) { maxw = 100; }
 	if ((maxh == null) || (maxh < 100)) { maxh = 100; }
 	if (peepdiam != null) {
 		maxh += peepdiam + 20;
 	}
 	this.maxw = maxw;
 	this.maxh = maxh;
  	this.balloondiv = document.createElement('div');
	this.balloondiv.setAttribute('id',this.balloonid);
	this.balloondiv.style.visibility = 'hidden';
	this.balloondiv.style.position = 'absolute';
	this.balloondiv.style.left = '0';
	this.balloondiv.style.top = '0';
	this.balloondiv.style.width = maxw + "px";
	this.balloondiv.style.height = maxh + "px";
  	document.body.appendChild(this.balloondiv);
	this.balloondiv = document.getElementById(this.balloonid);

	var canvas = document.createElement("canvas");
	canvas.setAttribute("width", maxw);
	canvas.setAttribute("height", maxh);
    canvas.setAttribute("id", this.balloonid + "_canvas");
	canvas.style.position = "absolute";
	canvas.style.left = "0";
	canvas.style.top = "0";
	canvas.style.zIndex = 1;
    this.balloondiv.appendChild(canvas);

  	var closediv = document.createElement('div');
	closediv.setAttribute('id',this.balloonid + "_button");
//    this.balloondiv.appendChild(closediv);
	closediv.style.position = 'absolute';
	closediv.style.right = '16';
	closediv.style.top = '0';
	closediv.style.width = "16px";
	closediv.style.height = "16px";
	closediv.style.zIndex = 2;
	closediv.balloonid = this.balloonid;
	closediv.balloon = this;
	closediv.onclick = function() { this.balloon.close(); };
    this.balloondiv.appendChild(closediv);
//
//	add a streambox if a peep diameter was specfied
//
	var contenttop = 16;
	if (peepdiam != null) {
		this.streambox = new PlotJax.StreamBox(plot, this.balloonid, 16, peepdiam, (maxw - 30 - 75 - 10), this, this.helper, 4);
		contenttop = this.streambox.getHeight();
	}

  	var contentdiv = document.createElement('div');
	contentdiv.setAttribute('id', this.balloonid + "_content");
	contentdiv.style.position = 'absolute';
	contentdiv.style.left = '10';
	contentdiv.style.top = contenttop;
	contentdiv.style.width = (maxw - 30 - 75) + "px";
	contentdiv.style.maxWidth = (maxw - 20 - 75) + "px";
	contentdiv.style.maxHeight = (maxh - (44 + contenttop)) + "px";
	contentdiv.style.zIndex = 2;
	contentdiv.style.height = "auto";
	contentdiv.style.overflow = "auto";

    this.balloondiv.appendChild(contentdiv);

	this.contenttop = contenttop;
	this.canvas = document.getElementById(this.balloonid + '_canvas');
	this.ctx = this.canvas.getContext('2d');
	this.closebtn = document.getElementById(this.balloonid + '_button');
	this.contentdiv = document.getElementById(this.balloonid + '_content');
	this.maxZ = maxZ || 20;
  	this.balloondiv.balloon = this;

	this.balloondiv.close = function() {
		this.balloon.close();
	};
};

PlotJax.Balloon.SHADOW_COLOR = "rgba(100,100,100,0.3)";
PlotJax.Balloon.SHADOW_SIZE = 8;
PlotJax.Balloon.FILL_COLOR = "white"; // "rgb(255,255,255)"; // "rgba(255,255,255,0.9)";
PlotJax.Balloon.CENTER_FILL_COLOR = "rgba(255,255,255,0.8)";
PlotJax.Balloon.BORDER_SIZE = 2;

PlotJax.Balloon.prototype = {
	centerBalloon : function() {
		this.centered = true;
		this.contentdiv.style.width = (this.maxw - 30) + "px";
		this.contentdiv.style.maxWidth = (this.maxw - 20) + "px";
		return this;
	},

	offsetBalloon : function() {
		this.centered = false;
		this.contentdiv.style.width = (this.maxw - 30 - 75) + "px";
		this.contentdiv.style.maxWidth = (this.maxw - 20 - 75) + "px";
		return this;
	},

	drawCenteredBalloon : function(w, h, offset) {
		var ctx = this.ctx;
		var left = 2;
		var top = 5;
		var right = w + 16;
		var bottom = top + h + offset;

// Draw shadow
		ctx.fillStyle = PlotJax.Balloon.SHADOW_COLOR;
		this.centeredBalloon(
			left + PlotJax.Balloon.SHADOW_SIZE, 
			top + PlotJax.Balloon.SHADOW_SIZE, 
			right + PlotJax.Balloon.SHADOW_SIZE, 
			bottom + PlotJax.Balloon.SHADOW_SIZE);
		ctx.fill();

// Draw balloon
		ctx.fillStyle = PlotJax.Balloon.FILL_COLOR;
		this.centeredBalloon(left, top, right, bottom);
		ctx.fill();

		ctx.strokeStyle = 'black';
		ctx.lineWidth = PlotJax.Balloon.BORDER_SIZE;
		this.centeredBalloon(left, top, right, bottom);
		ctx.stroke();
		return this;
	},

	drawLowLeftBalloon : function(w, h, offset) {
		var ctx = this.ctx;
		var left = 2;
		var top = 5;
		var right = w + 16;
		var bottom = top + h + offset;

// Draw shadow
		ctx.fillStyle = PlotJax.Balloon.SHADOW_COLOR;
		this.lowerLeftBalloon(
			left + PlotJax.Balloon.SHADOW_SIZE, 
			top + PlotJax.Balloon.SHADOW_SIZE, 
			right + PlotJax.Balloon.SHADOW_SIZE, 
			bottom + PlotJax.Balloon.SHADOW_SIZE);
		ctx.fill();

// Draw balloon
		ctx.fillStyle = PlotJax.Balloon.FILL_COLOR;
		this.lowerLeftBalloon(left, top, right, bottom);
		ctx.fill();

		ctx.strokeStyle = 'black';
		ctx.lineWidth = PlotJax.Balloon.BORDER_SIZE;
		this.lowerLeftBalloon(left, top, right, bottom);
		ctx.stroke();
		return this;
	},

	drawLowRightBalloon : function(w, h, offset) {
		var ctx = this.ctx;
		var left = 75;
		var top = 5;
		var right = left + w + 16;
		var bottom = top + h + offset;

// Draw shadow
		ctx.fillStyle = PlotJax.Balloon.SHADOW_COLOR;
		this.lowerRightBalloon(
			left + PlotJax.Balloon.SHADOW_SIZE, 
			top + PlotJax.Balloon.SHADOW_SIZE, 
			right + PlotJax.Balloon.SHADOW_SIZE, 
			bottom + PlotJax.Balloon.SHADOW_SIZE);
		ctx.fill();

// Draw balloon
		ctx.fillStyle = PlotJax.Balloon.FILL_COLOR;
		this.lowerRightBalloon(left, top, right, bottom);
		ctx.fill();

		ctx.strokeStyle = 'black';
		ctx.lineWidth = PlotJax.Balloon.BORDER_SIZE;
		this.lowerRightBalloon(left, top, right, bottom);
		ctx.stroke();
		return this;
	},

	drawMidLeftBalloon : function(w, h, offset) {
		var ctx = this.ctx;
		var left = 2;
		var top = 5;
		var right = w + 16;
		var bottom = top + h + offset;

// Draw shadow
		ctx.fillStyle = PlotJax.Balloon.SHADOW_COLOR;
		this.midLeftBalloon(
			left + PlotJax.Balloon.SHADOW_SIZE, 
			top + PlotJax.Balloon.SHADOW_SIZE, 
			right + PlotJax.Balloon.SHADOW_SIZE, 
			bottom + PlotJax.Balloon.SHADOW_SIZE);
		ctx.fill();

// Draw balloon
		ctx.fillStyle = PlotJax.Balloon.FILL_COLOR;
		this.midLeftBalloon(left, top, right, bottom);
		ctx.fill();

		ctx.strokeStyle = 'black';
		ctx.lineWidth = PlotJax.Balloon.BORDER_SIZE;
		this.midLeftBalloon(left, top, right, bottom);
		ctx.stroke();
		return this;
	},

	drawMidRightBalloon : function(w, h, offset) {
		var ctx = this.ctx;
		var left = 75;
		var top = 5;
		var right = left + w + 16;
		var bottom = top + h + offset;

// Draw shadow
		ctx.fillStyle = PlotJax.Balloon.SHADOW_COLOR;
		this.midRightBalloon(
			left + PlotJax.Balloon.SHADOW_SIZE, 
			top + PlotJax.Balloon.SHADOW_SIZE, 
			right + PlotJax.Balloon.SHADOW_SIZE, 
			bottom + PlotJax.Balloon.SHADOW_SIZE);
		ctx.fill();

// Draw balloon
		ctx.fillStyle = PlotJax.Balloon.FILL_COLOR;
		this.midRightBalloon(left, top, right, bottom);
		ctx.fill();

		ctx.strokeStyle = 'black';
		ctx.lineWidth = PlotJax.Balloon.BORDER_SIZE;
		this.midRightBalloon(left, top, right, bottom);
		ctx.stroke();
		return this;
	},

	drawUpLeftBalloon : function(w, h, offset) {
		var ctx = this.ctx;
		var left = 2;
		var top = 5;
		var right = w + 16;
		var bottom = top + h + offset;

// Draw shadow
		ctx.fillStyle = PlotJax.Balloon.SHADOW_COLOR;
		this.upperLeftBalloon(
			left + PlotJax.Balloon.SHADOW_SIZE, 
			top + PlotJax.Balloon.SHADOW_SIZE, 
			right + PlotJax.Balloon.SHADOW_SIZE, 
			bottom + PlotJax.Balloon.SHADOW_SIZE);
		ctx.fill();

// Draw balloon
		ctx.fillStyle = PlotJax.Balloon.FILL_COLOR;
		this.upperLeftBalloon(left, top, right, bottom);
		ctx.fill();

		ctx.strokeStyle = 'black';
		ctx.lineWidth = PlotJax.Balloon.BORDER_SIZE;
		this.upperLeftBalloon(left, top, right, bottom);
		ctx.stroke();
		return this;
	},

	drawUpRightBalloon : function(w, h, offset) {
		var ctx = this.ctx;
		var left = 75;
		var top = 5;
		var right = left + w + 16;
		var bottom = top + h + offset;

// Draw shadow
		ctx.fillStyle = PlotJax.Balloon.SHADOW_COLOR;
		this.upperRightBalloon(
			left + PlotJax.Balloon.SHADOW_SIZE, 
			top + PlotJax.Balloon.SHADOW_SIZE, 
			right + PlotJax.Balloon.SHADOW_SIZE, 
			bottom + PlotJax.Balloon.SHADOW_SIZE);
		ctx.fill();

// Draw balloon
		ctx.fillStyle = PlotJax.Balloon.FILL_COLOR;
		this.upperRightBalloon(left, top, right, bottom);
		ctx.fill();
		ctx.closePath();

		ctx.strokeStyle = 'black';
		ctx.lineWidth = PlotJax.Balloon.BORDER_SIZE;
		this.upperRightBalloon(left, top, right, bottom);
		ctx.stroke();
		return this;
	},

	centeredBalloon : function(left, top, right, bottom) {
		var ctx = this.ctx;
		ctx.beginPath();
		ctx.moveTo(left + 10, top);
		ctx.quadraticCurveTo(left, top, left, top + 10);	// upper left corner
		ctx.lineTo(left, bottom - 10);
		ctx.quadraticCurveTo(left, bottom, left + 10, bottom); // lower left corner
		ctx.lineTo(right - 10, bottom);
		ctx.quadraticCurveTo(right, bottom, right, bottom - 10);	// lower right corner
		ctx.lineTo(right, top + 10);
		ctx.quadraticCurveTo(right, top, right - 10, top);	// upper right corner
		ctx.lineTo(left + 10, top);
	},

	lowerLeftBalloon : function(left, top, right, bottom) {
		var ctx = this.ctx;
		ctx.beginPath();
		ctx.moveTo(left + 10, top);
		ctx.quadraticCurveTo(left, top, left, top + 10);	// upper left corner
		ctx.lineTo(left, bottom - 10);
		ctx.quadraticCurveTo(left, bottom, left + 10, bottom); // lower left corner
		ctx.lineTo(right - 10, bottom);
		ctx.quadraticCurveTo(right, bottom, right, bottom - 10);	// lower right corner
		ctx.lineTo(right, top + 60);
		ctx.lineTo(right + 75, top + 5);
		ctx.lineTo(right, top + 27);
		ctx.lineTo(right, top + 10);
		ctx.quadraticCurveTo(right, top, right - 10, top);	// upper right corner
		ctx.lineTo(left + 10, top);
	},

	lowerRightBalloon : function(left, top, right, bottom) {
		var ctx = this.ctx;
		ctx.beginPath();
		ctx.moveTo(left + 10, top);
		ctx.quadraticCurveTo(left, top, left, top + 10);	// upper left corner
		ctx.lineTo(left, top + 27);
		ctx.lineTo(left - 75, top + 5);
		ctx.lineTo(left, top + 60);
		ctx.lineTo(left, bottom - 10);
		ctx.quadraticCurveTo(left, bottom, left + 10, bottom); // lower left corner
		ctx.lineTo(right - 10, bottom);
		ctx.quadraticCurveTo(right, bottom, right, bottom - 10);	// lower right corner
		ctx.lineTo(right, top + 10);
		ctx.quadraticCurveTo(right, top, right - 10, top);	// upper right corner
		ctx.lineTo(left + 10, top);
	},

	midLeftBalloon : function(left, top, right, bottom) {
		var ctx = this.ctx;
		var mid = (bottom - top) >> 1;
		ctx.beginPath();
		ctx.moveTo(left + 10, top);
		ctx.quadraticCurveTo(left, top, left, top + 10);	// upper left corner
		ctx.lineTo(left, bottom - 10);
		ctx.quadraticCurveTo(left, bottom, left + 10, bottom); // lower left corner
		ctx.lineTo(right - 10, bottom);
		ctx.quadraticCurveTo(right, bottom, right, bottom - 10);	// lower right corner
		ctx.lineTo(right, bottom - mid + 20);
		ctx.lineTo(right + 75, bottom - mid);
		ctx.lineTo(right, bottom - mid - 20);
		ctx.lineTo(right, top + 10);
		ctx.quadraticCurveTo(right, top, right - 10, top);	// upper right corner
		ctx.lineTo(left + 10, top);
	},

	midRightBalloon : function(left, top, right, bottom) {
		var ctx = this.ctx;
		var mid = (bottom - top) >> 1;
		ctx.beginPath();
		ctx.moveTo(left + 10, top);
		ctx.quadraticCurveTo(left, top, left, top + 10);	// upper left corner
		ctx.lineTo(left, bottom - mid - 20);
		ctx.lineTo(left - 75, bottom - mid);
		ctx.lineTo(left, bottom - mid + 20);
		ctx.lineTo(left, bottom - 10);
		ctx.quadraticCurveTo(left, bottom, left + 10, bottom); // lower left corner
		ctx.lineTo(right - 10, bottom);
		ctx.quadraticCurveTo(right, bottom, right, bottom - 10);	// lower right corner
		ctx.lineTo(right, top + 10);
		ctx.quadraticCurveTo(right, top, right - 10, top);	// upper right corner
		ctx.lineTo(left + 10, top);
	},

	close : function() {
		if (this.is_open) {
			this.is_open = false;
			var deletes = 0;
	 		if (this.streambox != null) { deletes = this.streambox.close(); }
	 		this.balloondiv.style.visibility = 'hidden';
	 		this.balloondiv.style.zIndex = -1;
	  		this.ctx.clearRect(0,0,this.canvas.width, this.canvas.height);
			if (this.iframe != null) {
				this.balloondiv.removeChild(this.iframe);
				this.iframe = null;
			}
			this.contentdiv.style.visibility = "hidden";
	  		this.contentdiv.innerHTML = '';
			this.contentdiv.style.width = 
				this.centered ? 
					(this.balloondiv.offsetWidth - 30) + "px" :
					(this.balloondiv.offsetWidth - 30 - 75) + "px";
			this.contentdiv.style.height = "auto";
			if (deletes > 0) { this.plot.redraw(); }
		}
	},

	open : function(x, y, content) {
		if (this.is_open) { this.close(); } // if there's one open
		this.stemx = x;
		this.stemy = y;
/*
 *	if for URL or a stream, create/position a fixed size balloon
 */
		this.stream_open = false;
		this.contenttop = 16;
		this.is_open = true;
		if (content instanceof Array) {
			this.stream_open = true;
			this.contenttop = this.streambox.getHeight();
			this.streambox.populate(content);
			this.openFixed(true);
			return this;
		}
		else if (content.substr(0,4) == "url:") {
			this.update(content);
			return this.openFixed(true);
		}
		this.update(content);
		return this.openFlex();
	},

	update : function(content) {
/*
 *	load content and determine the dimensions of resulting div
 *	then adjust as needed based on the [x,y] location relative
 *	to the document.body dimensions
 */
 		if (content.substr(0, 4) == 'url:') {
		/* load an iframe */
			this.contentdiv.style.visibility = "hidden";
			if (this.iframe == null) {
				var ifrm = document.createElement("IFRAME");
				this.iframe = ifrm;
				ifrm.setAttribute("id", "balloon_iframe");
				ifrm.style.visibility = "hidden";
				var stream_off = this.stream_open ? this.streambox.getHeight() : 0;
				var w = this.centered ? 
					this.balloondiv.offsetWidth - 30 : 
					this.balloondiv.offsetWidth - 30 - 75;
				ifrm.setAttribute("width", w);
				ifrm.setAttribute("height", this.balloondiv.offsetHeight - 70 - stream_off);
//				ifrm.setAttribute("frameborder", 0);
				ifrm.setAttribute("frameBorder", 0);	// for IE
				ifrm.setAttribute("scrolling", "auto");
				ifrm.style.zIndex = 4;
				ifrm.style.position = 'absolute';
				this.balloondiv.appendChild(ifrm);
			}
			this.iframe.src = content.substr(4);
			return this;
		}
		else if (content.substr(0, 3) == 'id:') { /* its a div id */
			if (this.iframe != null) {
				this.iframe.style.visibility = "hidden";
			}
			this.contentdiv.innerHTML = document.getElementById(content.substr(3)).innerHTML;
			this.contentdiv.style.visibility = "visible";
		}
		else { /* its regular text */
			if (this.iframe != null) {
				this.iframe.style.visibility = "hidden";
			}
			this.contentdiv.innerHTML = content;
			this.contentdiv.style.visibility = "visible";
		}
		return this;
	},

	openFixed : function(drawit) {
		var x = this.stemx;
		var y = this.stemy;
		var balht = this.balloondiv.offsetHeight;
		var stream_off = this.stream_open ? this.streambox.getHeight() : 0;
		var contentw = this.balloondiv.offsetWidth - (this.centered ? 30 : 30 + 75);
		var contenth = balht - 70 - stream_off;
		var maxw = contentw;
		var maxh = contenth
		var to_left = x;
		var to_rt = document.body.clientWidth - x;
		if ((!this.centered) && (to_left < this.balloondiv.offsetWidth)) {
// force to the right, let user scroll over if the area is too small
			to_rt = Math.max(to_rt, this.balloondiv.offsetWidth + 10);
		}
		var above = y;
		var below = document.body.clientHeight - y;
		var hdir = this.centered ? 'c' : (to_rt > to_left) ? 'r' : 'l';
		var vdir = this.centered ? 'c' : 
			(Math.abs(above - below)/(above + below) < 0.1) ? 'm' :
			((above < balht) && (below < balht)) ? 'm' :
			(above > below) ? 'u' : 'd';

		if (this.iframe != null) {
			this.iframe.setAttribute("width", maxw);
			this.iframe.setAttribute("height", maxh);
			return this.finishBalloon(x, y, hdir, vdir, contentw, contenth, this.iframe, drawit);
		}
		this.contentdiv.style.width = (maxw - 10) + "px";
		this.contentdiv.style.height = maxh + "px";
		return this.finishBalloon(x, y, hdir, vdir, contentw, contenth, this.contentdiv, drawit);
	},

	openFlex : function() {
		var x = this.stemx;
		var y = this.stemy;
		var w = this.centered ? 30 : 30 + 75;
		var contentw = this.contentdiv.offsetWidth;
		var contenth = this.contentdiv.offsetHeight;
		var maxw = this.balloondiv.offsetWidth - w;
		var maxh = this.balloondiv.offsetHeight - 30;
		var to_left = x;
		var to_rt = document.body.clientWidth - x;
		if ((!this.centered) && (to_left < this.balloondiv.offsetWidth)) {
// force to the right, let user scroll over if the area is too small
			to_rt = Math.max(to_rt, this.balloondiv.offsetWidth + 10);
		}
		var above = y;
		var below = document.body.clientHeight - y;
		var hdir = this.centered ? 'c' : (to_rt > to_left) ? 'r' : 'l';
		var availw = (hdir == 'c') ?
			document.body.clientWidth : 
			(hdir == 'r') ? to_rt : to_left;
		var vdir = this.centered ? 'c' : 
			(Math.abs(above - below)/(above + below) < 0.1) ? 'm' :
			((above < maxh) && (below < maxh)) ? 'm' :
			(above > below) ? 'u' : 'd';
		var availh = (hdir == 'c') ? 
			document.body.clientHeight :
			(vdir == 'm') ? above + below :
			(vdir == 'u') ? above : below;

		if (!this.centered) { availw -= 75;	} // stem space
		if (contentw < 100) { contentw = 100; }
		else if (contentw > maxw) { contentw = maxw; }
/*
 *	if content doesn't fit in available space, squeeze it
 */
		if (availw < contentw) {
			contentw = availw - 10;
			contenth = this.contentdiv.offsetHeight;	// may need timeout here
		}
/*
 *	hopefully, any prior width adjustment has effected height
 *	NOTE: we may need to break this up and throw in a timeout for a redraw event
 */
		if (contenth < 75) { contenth = 75; }
		else if (contenth > maxh) { contenth = maxh; }
/*
 *	if content doesn't fit in available space, squeeze it
 */
		if (availh < contenth) {
			contenth = availh - 10;
			this.contentdiv.style.height = (availh - this.closebtn.offsetHeight - 25) + "px";
		}
		return this.finishBalloon(x, y, hdir, vdir, contentw, contenth, this.contentdiv, true);
	},

	finishBalloon : function(x, y, hdir, vdir, contentw, contenth, shown, drawit) {
		var balloonw = this.balloondiv.offsetWidth;
		var balloonh = this.balloondiv.offsetHeight;
		var contentOffset = 10 + this.closebtn.offsetHeight;
		var stream_off = this.stream_open ? this.streambox.getHeight() : 0;
		var dir = hdir + vdir;
		if (drawit) {
			var bbox =
				(dir == 'cc') ? this.drawCenteredBalloon(contentw, contenth, contentOffset + stream_off) :
				(dir == 'lm') ? this.drawMidLeftBalloon(contentw, contenth, contentOffset + stream_off) :
				(dir == 'lu') ? this.drawUpLeftBalloon(contentw, contenth, contentOffset + stream_off) :
				(dir == 'ld') ? this.drawLowLeftBalloon(contentw, contenth, contentOffset + stream_off) :
				(dir == 'rm') ? this.drawMidRightBalloon(contentw, contenth, contentOffset + stream_off) :
				(dir == 'ru') ? this.drawUpRightBalloon(contentw, contenth, contentOffset + stream_off) :
				/* (dir == 'rd') ? */ this.drawLowRightBalloon(contentw, contenth, contentOffset + stream_off);
/*
 *	draw button and content inside bbox
 */
			this.closebtn.style.left = contentw + ((hdir == 'r') ? 67 : -8);
			this.closebtn.style.top = 10;
			this.closebtn.style.zIndex = this.maxZ + 1;
			this.drawCloseButton(contentw + ((hdir == 'r') ? 67 : -8), 10);
/*
 *	position streambox (if any)
 */
			if (this.stream_open) { 
				contentOffset += this.streambox.move(((hdir == 'r') ? 87 : 9), contentOffset);
			}
		}
/*
 *	finally, position the content
 */
		shown.style.left = (hdir == 'r') ? 87 : 9;
		shown.style.top = contentOffset + stream_off;
		shown.style.visibility = "visible";

		if (drawit) {
/*
 *	properly position balloon div
 */
		if (this.centered) {
			x -= (balloonw >> 1);
			y -= (balloonh >> 1);
			if (x < 3) {
//	anchor at left, let content scroll if needed
				x = 3;
			}
			else if ((x + balloonw) > document.body.clientWidth) {
//	slide to the left
				x = document.body.clientWidth - balloonw;
	 		}
			if (y < 3) {
//	anchor at top, let content scroll if needed
				y = 3;
			}
			else if ((y + balloonh) > document.body.clientHeight) {
// slide up
				y = document.body.clientHeight - balloonh;
	 		}

			this.balloondiv.style.left = x;
			this.balloondiv.style.top = y;
		}
		else {
			this.balloondiv.style.left = (hdir == 'r') ? x : x - 85 - contentw;
			this.balloondiv.style.top = (vdir == 'd') ? y - 10 : 
				(vdir == 'u') ? y - (contenth + contentOffset - 15) :
				/* (vdir == 'm') ? */ y - ((contenth + contentOffset) >> 1);
		}
		this.balloondiv.style.visibility = "visible";
		this.balloondiv.style.zIndex = this.maxZ;
		}
	},

	upperLeftBalloon : function(left, top, right, bottom) {
		var ctx = this.ctx;
		ctx.beginPath();
		ctx.moveTo(left + 10, top);
		ctx.quadraticCurveTo(left, top, left, top + 10);	// upper left corner
		ctx.lineTo(left, bottom - 10);
		ctx.quadraticCurveTo(left, bottom, left + 10, bottom); // lower left corner
		ctx.lineTo(right - 10, bottom);
		ctx.quadraticCurveTo(right, bottom, right, bottom - 10);	// lower right corner
		ctx.lineTo(right, bottom - 27);
		ctx.lineTo(right + 75, bottom - 15);
		ctx.lineTo(right, bottom - 60);
		ctx.lineTo(right, top + 10);
		ctx.quadraticCurveTo(right, top, right - 10, top);	// upper right corner
		ctx.lineTo(left + 10, top);
	},

	upperRightBalloon : function(left, top, right, bottom) {
		var ctx = this.ctx;
		ctx.beginPath();
		ctx.moveTo(left + 10, top);
		ctx.quadraticCurveTo(left, top, left, top + 10);	// upper left corner
		ctx.lineTo(left, bottom - 60);
		ctx.lineTo(left - 75, bottom - 15);
		ctx.lineTo(left, bottom - 27);
		ctx.lineTo(left, bottom - 10);
		ctx.quadraticCurveTo(left, bottom, left + 10, bottom); // lower left corner
		ctx.lineTo(right - 10, bottom);
		ctx.quadraticCurveTo(right, bottom, right, bottom - 10);	// lower right corner
		ctx.lineTo(right, top + 10);
		ctx.quadraticCurveTo(right, top, right - 10, top);	// upper right corner
		ctx.lineTo(left + 10, top);
	},

	drawCloseButton : function(left, top) {
//
//	make a filled rect, then add highlighted border, then add the x
//
		var ctx = this.ctx;
		ctx.lineWidth = 1;
		ctx.fillStyle = "rgb(160,160,160)";
		ctx.fillRect(left, top, 14, 14);

		ctx.strokeStyle = "rgb(200,200,200)";
		ctx.beginPath();
		ctx.moveTo(left + 13, top);
		ctx.lineTo(left, top);
		ctx.lineTo(left, top + 13);
		ctx.closePath();
		ctx.stroke();

		ctx.strokeStyle = "rgb(40,40,40)";
		ctx.beginPath();
		ctx.moveTo(left + 13, top);
		ctx.lineTo(left + 13, top + 13);
		ctx.lineTo(left, top + 13);
		ctx.closePath();
		ctx.stroke();

		ctx.strokeStyle = "rgb(0,0,0)";
		ctx.beginPath();
		ctx.moveTo(left + 11, top + 2);
		ctx.lineTo(left + 2, top + 11);
		ctx.closePath();
		ctx.stroke();

		ctx.beginPath();
		ctx.moveTo(left + 2, top + 2);
		ctx.lineTo(left + 11, top + 11);
		ctx.closePath();
		ctx.stroke();
	},
	
	isOpen : function() { return this.is_open; }
};

}