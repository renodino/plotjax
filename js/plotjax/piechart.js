if (window.PlotJax.PieChart == null) {

registerNS("PlotJax.PieChart");

PlotJax.PieChart = function(chartdesc, plot, limits, shapes) {
	if (arguments.length > 0) {
		this.plot = plot;
		this.chartdesc = chartdesc;
		this.points = [];
		this.balloon = plot.getBalloon();
		this.gesture = plot.getGesture();
		this.xType = plot.getXType();
		this.yType = plot.getYType();
		this.myid = chartdesc.PlotID;
		this.helper = plot.getHelper();
		this.ctx = plot.getCanvas();

		if (!(this.xType instanceof PlotJax.SymbolType)) {
			alert("Invalid datatype for piechart domain (must be symbol type).");
			return null;
		}
//
//	need a color interpolator here...
//
		this.colors = (chartdesc.Style[0].Color != null) ? chartdesc.Style[0].Color.slice(0) : 
			[ 'red', 'blue', 'orange', 'green', 'purple', 'yellow'];
		
		for (var c = 0; c < this.colors.length; c++) { 
			this.colors[c] = pljxGetColor(this.colors[c]); 
		}
		var chartdata = chartdesc.Data;
		if ((chartdata != null) && (chartdata.length > 0)) {
			var i = 0;
			chartdata = chartdesc.Data;
			for (i = 0; i < chartdata.length; i++) {
				var datapt = chartdata[i];
				if (datapt[0] == null) {
					alert("Missing domain value at element " + i);
					return null;
				}
				if (datapt[1] != null) {
					this.points.push([this.xType.normalize(datapt[0]), this.yType.normalize(datapt[1])]);
				}
			}
		}
//
//	setup as a gesture observer
//
		if (this.gesture != null) { this.gesture.observe(this); }
	}
};

PlotJax.PieChart.LABEL_OFFSET = 8;
PlotJax.PieChart.SEGMENT_OFFSET = 8;

PlotJax.PieChart.prototype = {

	getChartID : function() { return this.myid; },

	addPoint : function(x,y) {
		this.points.push([x,y]);
		return true;
	},

	getElement : function(i) { 
		return [ this.xType.display(this.points[i][0]), this.yType.display(this.points[i][1]) ];
	},

	getElements : function() { return this.points; },

	layout : function(sticky, limits) {
		if ((this.helper != null) && (this.helper.onlayout != null)) {
			this.helper.onlayout(this.plot, this);
		}
		return this.points.length;
	},

/*
 * draw wedges:
 *		start wedges at 0 degress (3 o'clock, canvas time)
 *		compute which half each wedges label belongs in, and
 *		the label's bbox
 *		attempt to locate the label at X pixels from diameter,
 *		at a pt Y on a ray of length (RADIUS + X) dissecting the wedge
 *		pie diameter = min((chartw - 2*X - leftbbox - rtbbox), (charth - 2*X))
 */
	redraw : function(sticky) {
		if ((this.helper != null) && (this.helper.ondraw != null)) {
			this.helper.ondraw(this.plot, this);
		}
		var chartw = this.plot.getChartWidth();
		var charth = this.plot.getChartHeight();
		var topmargin = this.plot.getTopMargin();
		var botmargin = this.plot.getBottomMargin();
		var lmargin = this.plot.getLeftMargin();
		var rmargin = this.plot.getRightMargin();
		var i;
		var tickfont = this.plot.getTickFont();
		var maxx = Number.MIN_VALUE;
		var total = 0;
		for (i = 0; i < this.points.length; i++) {
			var bbox = tickfont.getBounds(this.xType.display(this.points[i][0]), 0, 0, 0, 0);
			maxx = Math.max(maxx, bbox[2]);
			total += this.points[i][1];
		}

		this.visible_coords = [];
		var minaxis = Math.min(chartw, charth);
		var diam =  minaxis - ((maxx + PlotJax.PieChart.LABEL_OFFSET) * 2);
		if (this.chartdesc.PlotKind == "segpie") { diam -= (PlotJax.PieChart.LABEL_OFFSET * 2); }
		var cx = (rmargin + lmargin + chartw) >> 1;
		var cy = (topmargin + charth) >> 1;
		this.cx = cx;
		this.cy = cy;
		var j = 0;
		this.radius = diam/2;
		var subtotal = 0;
/*
 *	compute delta angle for segmented gaps
 */
 		var seggap = PlotJax.PieChart.SEGMENT_OFFSET /diam;
 	
		for (i = 0; i < this.points.length; i++) {
			this.ctx.fillStyle = this.colors[j++];
			if (j >= this.colors.length) { j = 0; }
			var startangle = (subtotal / total) * Math.PI * 2;
			var endangle = ((subtotal + this.points[i][1]) / total) * Math.PI * 2;
			var midangle = startangle + ((endangle - startangle)/2);
//
//	if segmented, adjust cx/cy for offset displacement
//
			var tcx = cx;
			var tcy = cy;
			if ((this.chartdesc.PlotKind == "segpie") && (this.points.length > 1)) {
				startangle += seggap;
				midangle += (seggap / 2);
				tcx = cx + (PlotJax.PieChart.LABEL_OFFSET * Math.cos(midangle));
				tcy = cy + (PlotJax.PieChart.LABEL_OFFSET * Math.sin(midangle));
			}
			this.ctx.beginPath();
			this.ctx.moveTo(tcx, tcy);
			this.ctx.arc(tcx, tcy, this.radius, startangle, endangle, false);
			this.ctx.closePath();
			this.ctx.fill();
//
//	draw label ray
//
			this.ctx.beginPath();
			this.ctx.lineWidth = 1;
			this.ctx.strokeStyle = "black";
			this.ctx.moveTo(tcx + ((2 * this.radius/3) * Math.cos(midangle)),
				tcy + ((2 * this.radius/3) * Math.sin(midangle)));
			var labely = tcy + ((this.radius + PlotJax.PieChart.LABEL_OFFSET) * Math.sin(midangle));
			this.ctx.lineTo(tcx + ((this.radius + PlotJax.PieChart.LABEL_OFFSET) * Math.cos(midangle)), labely);
			var labelx = ((midangle < Math.PI/2) || (midangle >= 3 * Math.PI/2)) ?
				tcx + this.radius + PlotJax.PieChart.LABEL_OFFSET :
				tcx - this.radius - PlotJax.PieChart.LABEL_OFFSET;
			this.ctx.lineTo(labelx, labely);
			this.ctx.stroke();

			if ((midangle < Math.PI/2) || (midangle >= 3 * Math.PI/2)) {
				tickfont.drawRightOf(this.xType.display(this.points[i][0]), labelx, labely, 4);
			}
			else {
				tickfont.drawLeftOf(this.xType.display(this.points[i][0]), labelx, labely, 4);
			}
			subtotal += this.points[i][1];
			this.visible_coords.push([startangle, endangle ]);
		}
	},
/*
 *	called by Gesture on an event; it will scan the visibles
 *	to locate any effected elements; note that we return a copy
 */
	getVisibleElements : function() { return this.points.slice(0); },
/*
 *	called by a click in the chart; check if any wedges
 *	contain the specified coordinate; if so, return
 *	the wedge id and the hover text (use 0,0 for max width/height)
 */
	onclick : function(chartx, charty) {
		var clicked_pts = [];
		var i;
//
//	compute distance to make sure its even on the pie
//
		var xdist = this.cx - chartx;
		var ydist = this.cy - charty;
		var dist = Math.sqrt((xdist * xdist) + (ydist * ydist));
		if ((dist > this.radius) ||
			((this.chartdesc.PlotKind == "segpie") && (dist < PlotJax.PieChart.SEGMENT_OFFSET))) {
			return null;
		}
//
//	compute the angle
//
		var angle = Math.atan(Math.abs(ydist)/Math.abs(xdist));
		if (charty < this.cy) {
			angle = (chartx < this.cx) ? (angle + Math.PI) : ((Math.PI * 2) - angle);
		}
		else if (chartx < this.cx) {
			angle = Math.PI - angle;
		}
		for (i = 0; i < this.visible_coords.length; i++) {
			if ((this.visible_coords[i][0] <= angle) &&
				(this.visible_coords[i][1] > angle)) { break; }
		}

		return (i == this.visible_coords.length) ? null :
			[ 0,0, i, this.xType.display(this.points[i][0]) + "," + this.yType.display(this.points[i][1]) ];
	},

	onhover : function(x, y) {
		var i;
//
//	compute distance to make sure its even on the pie
//
		var xdist = this.cx - x;
		var ydist = this.cy - y;
		var dist = Math.sqrt((xdist * xdist) + (ydist * ydist));
		if ((dist > this.radius) ||
			((this.chartdesc.PlotKind == "segpie") && (dist < PlotJax.PieChart.SEGMENT_OFFSET))) {
			return null;
		}
//
//	compute the angle
//
		var angle = Math.atan(Math.abs(ydist)/Math.abs(xdist));
		if (y < this.cy) {
			angle = (x < this.cx) ? (angle + Math.PI) : ((Math.PI * 2) - angle);
		}
		else if (x < this.cx) {
			angle = Math.PI - angle;
		}
		for (i = 0; i < this.visible_coords.length; i++) {
			if ((this.visible_coords[i][0] <= angle) &&
				(this.visible_coords[i][1] > angle)) {
				return this.helper.onhover(this.plot, this, i);
			}
		}
		return null;
	},

	clear : function() {
/*
 *	delete any members that might cause memory leaks
 */
		this.points = null;
		this.visible_coords = null;
		this.plot = null;
		this.helper = null;
		this.balloon = null;
		this.gesture = null;
	},
/*
 *	observe a zoom event
 *		input is array of zoom objects that implement contains()
 *		return a list of element IDs that are contained 
 *		in the zoom. contains() takes the x,y of the
 *		center of an element
 *	(But what can a zoom do with a pie ???)
 *	TBD
 */
	observe : function(zooms) {
		var contained_wedges = [];
		return contained_wedges;
	}
};

}