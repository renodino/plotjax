
function diagnose(msg) { alert(msg); }

if (window.PlotJax.BubbleChart == null) {

registerNS("PlotJax.BubbleChart");
registerNS("PlotJax.Bubble");
registerNS("PlotJax.RoundBubble");
registerNS("PlotJax.SquareBubble");

PlotJax.Bubble = function (plot, parent, bubbleid, bubble, chartctx, balloon, helper) {
	if (arguments.length > 0) {
		this.plot = plot;
		this.parent = parent;
		this.myid = bubbleid;
		this.x_value = bubble[0];
		this.y_value = bubble[1];
		this.size_value = bubble[2];
		this.intensity_value = bubble[3];
		this.x_normal = bubble[4];
		this.y_normal = bubble[5];
		this.size_normal = bubble[6];
		this.intensity_normal = bubble[7];
		this.color = "";
		this.centerx = 0;
		this.centery = 0;
		this.ctx = chartctx;
		this.balloon = balloon;
		this.helper = helper;
	}
};

PlotJax.Bubble.MAX_FACTOR = 0.035;
PlotJax.Bubble.MIN_DIAM = 10;
PlotJax.Bubble.SHADOW = 4;

PlotJax.Bubble.prototype.getBubbleID = function() { return this.myid; };
PlotJax.Bubble.prototype.getCenterX = function() { return this.centerx; };
PlotJax.Bubble.prototype.getCenterY = function() { return this.centery; };
PlotJax.Bubble.prototype.getRadius = function() { return this.radius; };

PlotJax.Bubble.prototype.getNormalized = function() {
	return [ this.x_normal,	this.y_normal, this.size_normal, this.intensity_normal ];
};

PlotJax.Bubble.prototype.getSize = function() { return this.size_normal; };
PlotJax.Bubble.prototype.getIntensity = function() { return this.intensity_normal; };

PlotJax.Bubble.prototype.getValues = function() {
	return [ this.x_value,	this.y_value, this.size_value, this.intensity_value ];
};

PlotJax.Bubble.prototype.dump = function() {
	return [this.x_value, this.y_value, this.size_value, this.intensity_value, 
		this.x_normal, this.y_normal, this.size_normal, this.intensity_normal].join(',');
};

PlotJax.Bubble.prototype.clear = function() {
/*
 *	remove any references that might cause memory leaks
 */
	this.plot = null;
	this.parent = null;
	this.balloon = null;
	this.helper = null;
	this.ctx = null;
};

PlotJax.SquareBubble = function (plot, parent, bubbleid, bubble, chartctx, balloon, helper) {
	PlotJax.SquareBubble.baseConstructor.call(this, plot, parent, bubbleid, bubble, chartctx, balloon, helper);
	this.left = 0;
	this.top = 0;
	this.right = 0;
	this.bottom = 0;
	this.height = 0;
	this.width = 0;
	this.radius = 0;
};

JSPPTrustee.extend(PlotJax.SquareBubble, PlotJax.Bubble);

PlotJax.SquareBubble.prototype.draw = function(xType, yType, sizeSlope, sizeIntercept, color, shadow, leftOffset, vertEdge) {
/*
 *	assumes all relevant parameters have been (re)instantiated
 *	we must persist these values to support gestures
 */
	this.centerx = xType.pt2pxl(this.x_normal, leftOffset);
	this.centery = vertEdge - yType.pt2pxl(this.y_normal, 0);
	this.height = this.width = 
		Math.round(Math.sqrt((this.size_normal * sizeSlope) + sizeIntercept));
	this.color = color;
	this.left = this.centerx - (this.width >> 1);
	this.top = this.centery - (this.height >> 1);
	this.right = this.centerx + (this.width >> 1);
	this.bottom = this.centery + (this.height >> 1);
	this.radius = (this.width >> 1);
	if (shadow) {
		this.ctx.fillStyle = PlotJax.SHADOW;
		this.ctx.beginPath();
		this.ctx.fillRect(this.left + PlotJax.Bubble.SHADOW, this.top + PlotJax.Bubble.SHADOW,
			this.width, this.width);
		this.ctx.closePath();
	}

	this.ctx.fillStyle = this.color;
	this.ctx.fillRect(this.left, this.top, this.width, this.width);
/*
 *	add outline for visibility
 */
	this.ctx.strokeStyle = "black";
	this.ctx.linewidth = 1;
	this.ctx.beginPath();
	this.ctx.strokeRect(this.left, this.top, this.width, this.width);
	this.ctx.closePath();
};

PlotJax.SquareBubble.prototype.drawAt = function(canvas, x, y, scale) {
/*
 *	assumes all relevant parameters have been (re)instantiated
 *	we must persist these values to support gestures
 */
	var scaledw = Math.max(this.radius * scale, 4);
 	x -= scaledw;
 	y -= scaledw;
 	scaledw *= 2;
	canvas.fillStyle = this.color;
	canvas.fillRect(x, y, scaledw, scaledw);
/*
 *	add outline for visibility
 */
	canvas.strokeStyle = "black";
	canvas.linewidth = 1;
	canvas.beginPath();
	canvas.strokeRect(x, y, scaledw, scaledw);
	canvas.closePath();
};

PlotJax.SquareBubble.prototype.contains = function(x, y) {
	return ((this.left < x) && (this.right > x) && (this.top < y) && (this.bottom > y));
};

PlotJax.SquareBubble.prototype.getArea = function() { return (this.height * this.width); };

PlotJax.RoundBubble = function (plot, parent, bubbleid, bubble, chartctx, balloon, helper) {
	PlotJax.RoundBubble.baseConstructor.call(this, plot, parent, bubbleid, bubble, chartctx, balloon, helper);
	this.radius = 0;
};

JSPPTrustee.extend(PlotJax.RoundBubble, PlotJax.Bubble);

PlotJax.RoundBubble.prototype.draw = function(xType, yType, sizeSlope, sizeIntercept, color, shadow, leftOffset, vertEdge) {
	this.centerx = xType.pt2pxl(this.x_normal, leftOffset);
	this.centery = vertEdge - yType.pt2pxl(this.y_normal, 0);
	this.radius = Math.round(Math.sqrt(((this.size_normal * sizeSlope) + sizeIntercept)/Math.PI));
	this.color = color;

	if (shadow) {
		this.ctx.fillStyle = PlotJax.SHADOW;
		this.ctx.beginPath();
		this.ctx.arc(this.centerx + PlotJax.Bubble.SHADOW, this.centery + PlotJax.Bubble.SHADOW, 
			this.radius, 0, 2 * Math.PI, true);
		this.ctx.closePath();
		this.ctx.fill();
	}

	this.ctx.fillStyle = this.color;
	this.ctx.beginPath();
	this.ctx.arc(this.centerx, this.centery, this.radius, 0, 2 * Math.PI, true);
	this.ctx.closePath();
	this.ctx.fill();
/*
 *	add outline for visibility
 */
	this.ctx.strokeStyle = "black";
	this.ctx.linewidth = 1;
	this.ctx.beginPath();
	this.ctx.arc(this.centerx, this.centery, this.radius, 0, 2 * Math.PI, true);
	this.ctx.closePath();
	this.ctx.stroke();
};

PlotJax.RoundBubble.prototype.drawAt = function(canvas, x, y, scale) {
	var scaledr = Math.max(this.radius * scale, 4);
	canvas.fillStyle = this.color;
	canvas.beginPath();
	canvas.arc(x, y, scaledr, 0, 2 * Math.PI, true);
	canvas.closePath();
	canvas.fill();
/*
 *	add outline for visibility
 */
	canvas.strokeStyle = "black";
	canvas.linewidth = 1;
	canvas.beginPath();
	canvas.arc(x, y, scaledr, 0, 2 * Math.PI, true);
	canvas.closePath();
	canvas.stroke();
};

PlotJax.RoundBubble.prototype.contains = function(x, y) {
	var dx = Math.abs(x - this.centerx);
	var dy = Math.abs(y - this.centery);
	var r = Math.sqrt((dx * dx) + (dy * dy));
	return (r < this.radius);
};

PlotJax.RoundBubble.prototype.getArea = function() { return Math.PI * this.radius * this.radius; };

PlotJax.BubbleChart = function (chartdesc, plot, limits, shapes) {
	this.plot = plot;
	this.chartdesc = chartdesc;
	this.shapes = shapes;
	this.bubbles = [];
	this.visible_bubbles = [];
	this.zooms = [];
	this.undos = [];
	this.redos = [];
	this.sizeSlope = this.sizeIntercept = null;
	this.balloon = plot.getBalloon();
	this.gesture = plot.getGesture();
	this.xType = plot.getXType();
	this.yType = plot.getYType();
	this.limits = limits;
	this.myid = chartdesc.PlotID;
	this.bubbleid = 0;
	this.helper = plot.getHelper();
	this.chartstyle = chartdesc.Style[0];

	if (this.chartstyle.Shape == null) {
		this.chartstyle.Shape = 'circle';
	}
	else if ((this.chartstyle.Shape != 'square') &&
		(this.chartstyle.Shape != 'circle') &&
		(this.chartstyle.Shape != 'round')) {
		alert("Unknown shape " + this.chartstyle.Shape);
		return null;
	}

	if (chartdesc.Size != null) {
		this.sizeType = plot.getType(chartdesc.Size);
		if (this.sizeType == null) {
			return;
		}
		if ((chartdesc.Size.Mapping == null) ||
			((chartdesc.Size.Mapping != 'normal') &&
				(chartdesc.Size.Mapping != 'reverse'))) {
			chartdesc.Size.Mapping = 'normal';
		}
	}

	if (chartdesc.Intensity != null) {
		this.intensityType = plot.getType(chartdesc.Intensity);
		if (this.intensityType == null) {
			return;
		}
		if ((chartdesc.Intensity.Mapping == null) ||
			((chartdesc.Intensity.Mapping != 'normal') &&
				(chartdesc.Intensity.Mapping != 'reverse'))) {
			chartdesc.Intensity.Mapping = 'normal';
		}
	}

	if (!(this.chartstyle.Color instanceof Array)) {
		this.chartstyle.Color = [ this.chartstyle.Color ];
	}
	this.colors = this.chartstyle.Color.slice(0);
	if (this.colors == null) {
		this.colors = [ "#D2D2D2", "#000000" ];
	}
	else if (this.colors.length == 1) {
		this.colors[1] = this.colors[0];
	}
	var i;
	for (i = 0; i < this.colors.length; i++) {
		this.colors[i] = pljxParseColor(this.colors[i]);
		if (this.colors[i] == null) {
			alert("Invalid color specification.");
			return null;
		}
	}
	if (chartdesc.Intensity.Mapping == "reverse") { this.colors = this.colors.reverse(); }
	var chartdata = chartdesc.Data;
	if ((chartdata == null) || (chartdata.length == 0)) {
		if ((limits[1] == null) || (limits[0] == null) ||
			(limits[3] == null) || (limits[2] == null)) {
			alert("No data provided, and incomplete limits specified.");
			return null;
		}
	}
	else {
		chartdata = chartdesc.Data;
		var canvas = plot.getCanvas();
		for (i = 0; i < chartdata.length; i++) {
			var datapt = chartdata[i].slice(0);
			var x = this.xType.normalize(datapt[0]);
			var y = this.yType.normalize(datapt[1]);
			if (x == null) {
				alert("Missing domain value at element " + i);
				return null;
			}
			if (y == null) {
				alert("Missing range value at element " + i);
				return null;
			}
			/*
			 *	filter data points outside limits
			 */
			if (((limits[1] != null) && (x > limits[1])) ||
				((limits[0] != null) && (x < limits[0])) ||
				((limits[3] != null) && (y > limits[3])) ||
				((limits[2] != null) && (y < limits[2]))) {
				continue;
			}

			if (datapt.length < 3) {
				if (this.sizeType != null) {
					alert("Missing size value at element " + i);
					return null;
				}
				datapt.push(1);
			}
			var size = (this.sizeType != null) ? 
				this.sizeType.normalize(datapt[2]) : 1;
			if (size == null) {
				alert("Missing size value at element " + i);
				return null;
			}

			if (datapt.length < 4) {
				if (this.intensityType != null) {
					alert("Missing intensity value at element " + i);
					return null;
				}
				datapt.push(1);
			}
			var intensity = (this.intensityType != null) ? 
				this.intensityType.normalize(datapt[3]) : 1;
			if (intensity == null) {
				alert("Missing intensity value at element " + i);
				return null;
			}
			datapt.push(x, y, size, intensity);
			var bubble = (this.chartstyle.Shape == 'square') ?
				new PlotJax.SquareBubble(this.plot, this, this.bubbleid++, datapt, canvas, this.balloon, this.helper) :
				new PlotJax.RoundBubble(this.plot, this, this.bubbleid++, datapt, canvas, this.balloon, this.helper);
			if (bubble == null) {
				return null;
			}
			this.bubbles.push(bubble);
		}
		this.visible_bubbles = this.bubbles.slice(0);
	}
//
//	setup as a gesture observer
//
	if (this.gesture != null) {
		this.gesture.observe(this);
	}
};

PlotJax.BubbleChart.prototype.getChartID = function() { return this.myid; };

PlotJax.BubbleChart.prototype.addBubble = function(bubble) {
	var plot = this.plot;
	var b = (this.chartstyle.shape == 'square') ?
		new PlotJax.SquareBubble(plot, this, this.bubbleid++, bubble, 
			plot.getCanvas(), plot.getAreamap(), this.balloon, this.helper) :
		new PlotJax.RoundBubble(plot, this, this.bubbleid++, bubble, 
			plot.getCanvas(), plot.getAreamap(), this.balloon, this.helper);
	if (b != null) {
		this.bubbles.push(b);
		return true;
	}
	return false;
};

/*
 *	interpolate intensity value into the color list
 */
PlotJax.BubbleChart.prototype.computeIntensity = function(intensity) {
	var level = intensity * this.intensityFactor;
	var bot = Math.floor(level);
	level -= bot;
	var rgb = [];
	if (bot < this.colors.length - 1) {
	}
	for (var i = 0; i < 3; i++) {
		rgb[i] = (bot == this.colors.length - 1) ?
			this.colors[bot][i] :
			Math.round(this.colors[bot][i] + (level * (this.colors[bot + 1][i] - this.colors[bot][i])));
	}
	return "rgba(" + rgb.join(",") + ", 0.3)";
};


PlotJax.BubbleChart.prototype.getElement = function(i) { 
	var vals = this.bubbles[i].getNormalized();
	var dims = [ this.xType.display(vals[0]), this.yType.display(vals[1]) ];
	if (this.sizeType != null) {
		dims.push(this.sizeType.display(vals[2]));
	}
	if (this.intensityType != null) {
		dims.push(this.intensityType.display(vals[3]));
	};
	return dims;
};

PlotJax.BubbleChart.prototype.getElements = function() { return this.bubbles; };

PlotJax.BubbleChart.prototype.getUndos = function() { return this.undos.slice(0); };

PlotJax.BubbleChart.prototype.layout = function(sticky, limits) {
	if ((this.helper != null) && (this.helper.onlayout != null)) {
		this.helper.onlayout(this.plot, this);
	}
/*
 *	sort bubbles over area in descending order
 *	!!!NEED TO APPLY NORMAL VS. REVERSE MAPPING
 */
	this.visible_bubbles = this.visible_bubbles.sort(this.sortBySize);
/*
 *	determine bounds of each dimension;
 *	size/intensity aren't computed until redraw time
 */
	var min_x = Number.MAX_VALUE;
	var max_x = Number.MIN_VALUE;
	var min_y = Number.MAX_VALUE;
	var max_y = Number.MIN_VALUE;

	var i;
	for (i = 0; i < this.visible_bubbles.length; i++) {
		var dimensions = this.visible_bubbles[i].getNormalized();
		max_x = Math.max(max_x, dimensions[0]);
		min_x = Math.min(min_x, dimensions[0]);
		max_y = Math.max(max_y, dimensions[1]);
		min_y = Math.min(min_y, dimensions[1]);
	}
/*
 *	merge our limits with the global limits
 */
	limits[0] = Math.min(min_x, limits[0]);
	limits[1] = Math.max(max_x, limits[1]);
	limits[2] = Math.min(min_y, limits[2]);
	limits[3] = Math.max(max_y, limits[3]);
	return i;
};

/*
 * draw bubbles
 */
PlotJax.BubbleChart.prototype.redraw = function(sticky) {
	if ((this.helper != null) && (this.helper.ondraw != null)) {
		this.helper.ondraw(this.plot, this);
	}
	if (this.visible_bubbles.length == 0) {
		return;
	}
//	diagnose("compute bubble size factors");
	var maxarea = PlotJax.Bubble.MAX_FACTOR * (this.plot.getChartWidth() * this.plot.getChartHeight()); 
		/* max is 10% of total area */
	var minarea = PlotJax.Bubble.MIN_DIAM * PlotJax.Bubble.MIN_DIAM;	/* 3 pixel radius */
	/*
	 *	compute max/min size and intensity of visible bubbles
	 */
	var max_size = Number.MIN_VALUE;
	var min_size = Number.MAX_VALUE;
	var max_intensity = Number.MIN_VALUE;
	var min_intensity = Number.MAX_VALUE;
	var size = 0;
	var intensity = 0;
	var i;
	for (i = 0; i < this.visible_bubbles.length; i++) {
		size = this.visible_bubbles[i].getSize();
		intensity = this.visible_bubbles[i].getIntensity();
		
		max_size = Math.max(max_size, size);
		min_size = Math.min(min_size, size);
		if (intensity != null) {		
			max_intensity = Math.max(max_intensity, intensity);
			min_intensity = Math.min(min_intensity, intensity);
		}
	}
	if ((this.sizeType != null) && (max_size - min_size > 0.00000001)) {
		this.sizeSlope = (this.chartdesc.Size.Mapping == 'normal') ?
			(maxarea - minarea)/(max_size - min_size) :
			(maxarea - minarea)/(min_size - max_size);
		this.sizeIntercept = (this.chartdesc.Size.Mapping == 'normal') ?
			maxarea - (this.sizeSlope * max_size) :
			maxarea - (this.sizeSlope * min_size);
	}
	else {	// all the same size
		this.sizeSlope = 0;
		this.sizeIntercept = maxarea >> 1;
	}
/*
 *	should we recompute intensity on a zoom ?
 */
	this.intensityFactor = ((this.intensityType != null) && (max_intensity - min_intensity > 0.00000001)) ? 
		(this.colors.length - 1)/(max_intensity - min_intensity) : 0;
//	var shadow = this.plot.useShadow();
	var shadow = false;
	var left = this.plot.getLeftMargin();
	var edge = this.plot.getVertEdge();
	for (i = 0; i < this.visible_bubbles.length; i++) {

		this.visible_bubbles[i].draw(
			this.xType, 
			this.yType, 
			this.sizeSlope, 
			this.sizeIntercept, 
			(this.intensityFactor != 0) ? 
				this.computeIntensity(this.visible_bubbles[i].getIntensity() - min_intensity) : 
				"rgba(" + this.colors[0].join(",") + ", 0.3)",
			shadow, left, edge);
	}
};

PlotJax.BubbleChart.prototype.sortBySize = function(a,b) {
	return a.getSize() - b.getSize();
};

PlotJax.BubbleChart.prototype.redo = function() {
	var redo = this.redos.pop();
	var temp = [];
	for (var i = this.visible_bubbles.length - 1; (i >= 0) && (redo.length > 0); i--) {
		var bblid = this.visible_bubbles[i].getBubbleID();
		/*
		 *	locate the bubble in the redo list; if found, remove it and push
		 *	into undo list
		 */
		for (var j = 0; (j < redo.length) && (redo[j] != bblid); j++) {}
		if (j < redo.length) {
			this.visible_bubbles.splice(i, 1);
			temp.push(bblid);
			redo.splice(j, 1);
		}
	}
	this.undos.push(temp);
};

PlotJax.BubbleChart.prototype.undo = function() {
	var redo = this.undos.pop();
	for (var i = 0; i < redo.length; i++) {
		this.visible_bubbles.push(this.bubbles[redo[i]]);
	}
	this.redos.push(redo);
};
/*
 *	 discard a bubble (no undo)
 */
PlotJax.BubbleChart.prototype.discardElement = function(id) {
	if ((this.bubbles.length < id) && (this.bubbles[id] != null)) {
		this.bubbles[id].clear();
		this.bubbles[id] = null;
	}
	return this;
};

/*
 *	recoverably hide bubbles in the ids list
 *	input is a list of ids, an optimization to avoid
 *	multiple call overhead and add'l undo list mgmt
 */
PlotJax.BubbleChart.prototype.hideElements = function(ids) {
	var bblid;
	var undos = [];
	var i,j;
/*
 *	traverse in reverse order so we don't mess up the indexing when we splice
 */
	for (i = this.visible_bubbles.length - 1; i >= 0 ; i--) {
		bblid = this.visible_bubbles[i].getBubbleID();
		for (j = 0; (j < ids.length) && (bblid != ids[j]); j++) {}
		if (j < ids.length) {
			this.visible_bubbles.splice(i, 1);
			undos.push(bblid);
			ids.splice(j, 1);	// don't need to look at this id again
		}
	}
	if (undos.length > 0) {
		this.undos.push(undos);
	}
	return this;
};

/*
 *	recoverably hide bubbles that are NOT in the specified list
 */
PlotJax.BubbleChart.prototype.keepElements = function(ids) {
	var bblid;
	var undos = [];
	var i,j;
	for (i = this.visible_bubbles.length - 1; (i >= 0 ) && (ids.length > 0); i--) {
		bblid = this.visible_bubbles[i].getBubbleID();
		for (j = 0; (j < ids.length) && (bblid != ids[j]); j++) {}
		if (j == ids.length) {	// not in keep list
			this.visible_bubbles.splice(i, 1);
			undos.push(bblid);
		}
		else {
			ids.splice(j, 1); 	// don't need to look at it again
		}
	}
	while (i >= 0) {
/*
 *	all remaining visible were not in keeplist, so remove them
 */
		undos.push(this.visible_bubbles[i].getBubbleID());
		this.visible_bubbles.splice(i, 1);
		i--;
	}
	if (undos.length > 0) {
		this.undos.push(undos);
	}
	return this;
};

/*
 *	called by Gesture on an event; it will scan the visibles
 *	to locate any effected elements; note that we return a copy
 */
PlotJax.BubbleChart.prototype.getVisibleElements = function() {
	return this.visible_bubbles.slice(0);
};

/*
 *	called by a click in the chart; check if any elements
 *	contain the specified coordinate
 *	returns a list of bubble elements, sorted from largest to smallest
 *	with max width/height as 1st elements, along with the
 *	formatted hover text for the element
 *	if no elements match, returns null
 */
PlotJax.BubbleChart.prototype.onclick = function(chartx, charty) {
	var clicked_bubbles = [];
	for (var i = 0; i < this.visible_bubbles.length; i++) {
		if (this.visible_bubbles[i].contains(chartx, charty)) {
			clicked_bubbles.push(this.visible_bubbles[i]);
		}
	}
	if (clicked_bubbles.length == 0) {
		return null;
	}
	/*
	 *	sort by size descending, and get the hover text for each element
	 */
	clicked_bubbles = clicked_bubbles.sort(this.sortBySize);
	for (i = 0; i < clicked_bubbles.length; i++) {
		clicked_bubbles[i] = clicked_bubbles[i].getBubbleID();
	}
	
	return clicked_bubbles;
};

/*
 *	draw the specified element on the specified canvas centered on the
 *	specified coordinates
 */
PlotJax.BubbleChart.prototype.drawAt = function(canvas, x, y, peepdiam, elem) {

 	var maxdiam = 2 * ((this.visible_bubbles[0].getRadius() > this.visible_bubbles[this.visible_bubbles.length - 1].getRadius()) ? 
 		this.visible_bubbles[0].getRadius() : this.visible_bubbles[this.visible_bubbles.length - 1].getRadius());
	var scale = (maxdiam > peepdiam) ? peepdiam/maxdiam : 1;
	for (var i = 0; (i < this.bubbles.length) && (this.bubbles[i].getBubbleID() != elem); i++) {}
	if (i < this.bubbles.length) {
		this.bubbles[i].drawAt(canvas, x, y, scale);
	}
	return this;
};

/*
 *	called by hovering in the chart; check if any elements
 *	contain the specified coordinate; if so, fetch hover content from helper
 */
PlotJax.BubbleChart.prototype.onhover = function(x, y) {
	var clicked_bubbles = [];
	for (var i = 0; i < this.visible_bubbles.length; i++) {
		if (this.visible_bubbles[i].contains(x, y)) {
			return this.helper.onhover(this.plot, this, this.visible_bubbles[i].getBubbleID());
		}
	}
	return null;
};

PlotJax.BubbleChart.prototype.clear = function() {
/*
 *	delete any members that might cause memory leaks
 */
	for (var i = 0; i < this.bubbles.length; i++) {
		if (this.bubbles[i] != null) {
			this.bubbles[i].clear();
			this.bubbles[i] = null;
		}
	}
	this.bubbles = null;
	this.visible_bubbles = null;
	this.plot = null;
	this.helper = null;
	this.balloon = null;
	this.gesture = null;
	this.zooms = null;
	this.redos = null;
	this.undos = null;
	this.chartstyle = null;
	this.colors = null;
};

/*
 *	observe a zoom event
 *		input is array of zoom objects that implement contains()
 *		return a list of element IDs that are contained 
 *		in the zoom. contains() takes the x,y of the
 *		center of an element
 *	always return array, even if empty
 */
PlotJax.BubbleChart.prototype.observeZoom = function(zooms) {
	var contained_bubbles = [];
	for (var i = 0; i < this.visible_bubbles.length; i++) {
		for (var j = 0; j < zooms.length; j++) {
//			var dit = this.getElement(this.visible_bubbles[i].getBubbleID()).join(",");
			if (zooms[j].contains(this.visible_bubbles[i].getCenterX(), this.visible_bubbles[i].getCenterY())) {
				contained_bubbles.push(this.visible_bubbles[i].getBubbleID());
			}
		}
	}
	return contained_bubbles;
};

/*
 *	observe a flick event
 *		input is zoom object that implements intersects()
 *		return a list of element IDs that are within some radius
 *		orthogonal to the flick. intersects() takes the x,y of the
 *		center of an element
 */
PlotJax.BubbleChart.prototype.observeFlick = function(flick) {
	var flicked_bubbles = [];
	for (var i = 0; i < this.visible_bubbles.length; i++) {
		if (flick.intersects(
			this.visible_bubbles[i].getCenterX(),
			this.visible_bubbles[i].getCenterY(),
			this.visible_bubbles[i].getRadius())) {
			flicked_bubbles.push(this.visible_bubbles[i].getBubbleID());
		}
	}
	return flicked_bubbles;
};

}