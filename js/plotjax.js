
function diagnose(msg) { alert(msg); }


if (window.PlotJax == null) {

registerNS("PlotJax");
registerNS("PlotJax.Foundry");
registerNS("PlotJax.Font");
registerNS("PlotJax.BaseType");
registerNS("PlotJax.TimeType");
registerNS("PlotJax.TimestampType");
registerNS("PlotJax.DateType");
registerNS("PlotJax.NumberType");
registerNS("PlotJax.SymbolType");
registerNS("PlotJax.IntervalType");

/*
 *	class method to return the RGB value array for a color specification
 */
function pljxParseColor(color) 
{
	var c = null;
	if (PlotJax.COLORS[color.toLowerCase()] != null) 
	{
		return PlotJax.COLORS[color.toLowerCase()];
	}
	if (color.substr(0,1) == '#') {
		return (color.length == 7) ? 
			[ parseInt(color.substr(1,2), 16),
				parseInt(color.substr(3,2), 16),
				parseInt(color.substr(5,2), 16) ] : 
			(color.length == 4) ? 
				[ parseInt(color.substr(1,1), 16) * 16,
					parseInt(color.substr(2,1), 16) * 16,
					parseInt(color.substr(3,1), 16) * 16 ] :
			(color.length == 9) ? 
				[ parseInt(color.substr(1,2), 16),
					parseInt(color.substr(3,2), 16),
					parseInt(color.substr(5,2), 16),
					parseInt(color.substr(7,2), 16)/255 ] : 
			(color.length == 5) ? 
				[ parseInt(color.substr(1,1), 16) * 16,
					parseInt(color.substr(2,1), 16) * 16,
					parseInt(color.substr(3,1), 16) * 16,
					parseInt(color.substr(4,1), 16)/16 ] :
			null;
	}
	if (color.match(/(rgb[a]?)\s*\(([^\)]+)\)/) == null)
	{
		return null;
	}
	var rgbmethod = RegExp.$1;
	c = RegExp.$2.split(/\s*,\s*/);
	if (rgbmethod == "rgba") 
	{
		if ((c.length != 4) || (parseFloat(c[3]) == null) || 
			(c[3] < 0) || (c[3] > 1.0))
		{
			return null;
		}
	}
	else if (c.length != 3) 
	{
		return null;
	}
	for (var i = 0; i < 3; i++) 
	{
		if ((parseInt(c[i], 10) == null) || 
			(c[i] < 0) || 
			(c[i] > 255))
		{ 
			return null;
		}
	}
	return [ c[0], c[1], c[2] ];
}

/*
 *	class method to return the rgba() function string for a color specification
 */
function pljxGetColor(color, dfltcolor, alpha) 
{
	if (color == null) { return dfltcolor; }
	var c = null;
	if (PlotJax.COLORS[color.toLowerCase()] != null) 
	{
		c = PlotJax.COLORS[color.toLowerCase()];
	}
	else if (color.substr(0,1) == '#') {
		c = (color.length == 7) ? 
			[ parseInt(color.substr(1,2), 16),
				parseInt(color.substr(3,2), 16),
				parseInt(color.substr(5,2), 16) ] : 
			(color.length == 4) ? 
				[ parseInt(color.substr(1,1), 16) * 16,
					parseInt(color.substr(2,1), 16) * 16,
					parseInt(color.substr(3,1), 16) * 16 ] :
			(color.length == 9) ? 
				[ parseInt(color.substr(1,2), 16),
					parseInt(color.substr(3,2), 16),
					parseInt(color.substr(5,2), 16),
					parseInt(color.substr(7,2), 16)/255 ] : 
			(color.length == 5) ? 
				[ parseInt(color.substr(1,1), 16) * 16,
					parseInt(color.substr(2,1), 16) * 16,
					parseInt(color.substr(3,1), 16) * 16,
					parseInt(color.substr(4,1), 16)/16 ] :
			null;
	}
	else if (color.match(/(rgb[a]?)\s*\(([^\)]+)\)/) != null)
	{
		var rgbmethod = RegExp.$1;
		c = RegExp.$2.split(/\s*,\s*/);
		if (rgbmethod == "rgba") 
		{
			if ((c.length != 4) || (parseFloat(c[3]) == null) || 
				(c[3] < 0) || (c[3] > 1.0))
			{
				c = null;
			}
		}
		else if (c.length == 3) 
		{
			for (var i = 0; i < 3; i++) 
			{
				if ((parseInt(c[i], 10) == null) || 
					(c[i] < 0) || 
					(c[i] > 255))
				{ 
					c = null;
				}
			}
		}
		else
		{
			c = null;
		}
	}
	if (c == null)
	{
		alert("Invalid color specification.");
		return null;
	}

	return (c.length == 3) 
		? ((alpha == null) 
			? "rgb(" + c.join(',') + ")" 
			: "rgba(" + c.join(',') + ',' + alpha + ")" )
		: "rgba(" + c.join(',') + ")";
}

// Ctrl-Z kepress capture
/*
function KeyPress(e) {
      var evtobj = window.event? event : e
      if (evtobj.keyCode == 90 && evtobj.ctrlKey) alert("Ctrl+z");
}

document.onkeydown = KeyPress;
*/
/*
 *	creates a chart canvas in the div identified by chartid
 *	located at (x,y) with a size fo (w,h)
 *	contentJson is a JSON object describing the chart properties
 *	and data
 */
PlotJax = function (chartid, w, h, contentJson, helper, imagePath, debugid) {
	this.origwidth = w;	// save copy in case of shadow
	this.origheight = h;
	this.width = w;
	this.height = h;
	this.charts = [];
	this.xType = null;
	this.yType = null;
	this.sizeType = null;
	this.intensityType = null;
	this.myid = chartid;
	this.chartdiv = document.getElementById(chartid);
	this.helper = helper;
	this.imagePath = imagePath;
	if ((this.imagePath != null) && (this.imagePath.length > 0) && 
		(this.imagePath.charAt(this.imagePath.length - 1) != "/")) { 
		this.imagePath += "/"; 
	}

	if (this.chartdiv == null) {
		alert("Unknown chart '" + chartid + "'");
		return;
	}
	if ((w < 100) || (h < 100)) {
		alert("Minimum chart dimensions are 100 x 100");
		return;
	}
/*
 *	create base canvas div
 */
	this.chartcnvs = document.createElement("canvas");
	this.chartcnvs.setAttribute("width", w);
	this.chartcnvs.setAttribute("height", h);
    this.chartcnvs.setAttribute("id", chartid + "_canvas");
	this.chartcnvs.style.position = "absolute";
	this.chartcnvs.style.left = "0px";
	this.chartcnvs.style.top = "0px";
	this.chartcnvs.style.zIndex = 1;
    this.chartdiv.appendChild(this.chartcnvs);
    this.chartdiv.chart = this;

/*
 *	create hover div
 */
	this.hoverdiv = document.createElement("div");
    this.hoverdiv.setAttribute("id", chartid + "_hover");
	this.hoverdiv.style.position = "absolute";
	this.hoverdiv.style.left = 0;
	this.hoverdiv.style.top = 0;
	this.hoverdiv.style.zIndex = -1;
	this.hoverdiv.style.visibility = "hidden";
	this.hoverdiv.style.background = "#FAFCBA";
	this.hoverdiv.style.border = "thin solid black";
	this.hoverdiv.style.fontFamily = "arial, sans-serif, helvetica";
	this.hoverdiv.style.color = "black";
	this.hoverdiv.style.fontSize = "x-small";
    this.chartdiv.appendChild(this.hoverdiv);
/*
 * for some stupid reason, mouseup/down on img doesn't 
 * work properly if we move the mouse with button pressed 
 * - it loses the mouseup event -, so we have to use this div
 */
	this.zoommap = document.createElement("div");
    this.zoommap.setAttribute("id", chartid + "_zoommap");
	this.zoommap.style.position = "absolute";
	this.zoommap.style.left = 0;
	this.zoommap.style.top = 0;
	this.zoommap.style.width = "20px";
	this.zoommap.style.height = "20px";
	this.zoommap.style.zIndex = 2;
	this.zoommap.style.overflow = "hidden";
	this.zoommap.chart = this;
    this.chartdiv.appendChild(this.zoommap);
	this.zoommap.onmousemove = function(event) {
		this.chart.mouseMove(event);
	};
	this.zoommap.onmousedown =  function(event) {
		this.chart.mouseDown(event);
	};
	this.zoommap.onmouseup =  function(event) {
		this.chart.mouseUp(event);
	};
	this.zoommap.onclick = function(event) {
		this.chart.mouseClick(event);
	};
	this.zoomcnvs = document.createElement("canvas");
	this.zoomcnvs.setAttribute("width", w);
	this.zoomcnvs.setAttribute("height", h);
    this.zoomcnvs.setAttribute("id", chartid + "_zoomcanvas");
	this.zoomcnvs.style.position = "absolute";
	this.zoomcnvs.style.left = 0;
	this.zoomcnvs.style.top = 0;
    this.zoommap.appendChild(this.zoomcnvs);

	this.debugdiv = (debugid != null) ? document.getElementById(debugid) : null;

	this.chartctx = this.chartcnvs.getContext('2d');
	if (this.chartctx == null) {
 		alert("Sorry, your browser is not supported.");
 		return null;
	}
	this.zoomctx = this.zoomcnvs.getContext('2d');

	if (PlotJax.Gesture != null) {
		this.gesture = new PlotJax.Gesture(0, 0, w, h, this.zoomctx, this, debugid, chartid);
	}

	if (PlotJax.Balloon != null) {
		var balloonDims = ((helper != null) && (helper.getBalloonDimensions != null)) ?
			helper.getBalloonDimensions() : [ 360, 360 ];
		var peepdiam = ((helper != null) && (helper.getPeepDiameter != null)) ?
			helper.getPeepDiameter() : 40;
//
//	use 2 different balloons: one w/ streambox, the other without
//
		this.streamballoon = new PlotJax.Balloon(this, chartid + "_stream", balloonDims[0], balloonDims[1], 20, this.helper, peepdiam);
		this.balloon = new PlotJax.Balloon(this, chartid, balloonDims[0], balloonDims[1], 20, this.helper);
	}
	this.shapes = new PlotJax.Shapes(this, this.chartctx);
	if (PlotJax.Balloon != null) {
		this.buttonbar = new PlotJax.ButtonBar(this, chartid, this.gesture, h);
	}
	if ((this.gesture != null) || (this.balloon != null)) {
		this.zoommap.style.cursor = "pointer";
	}
	this.chartdiv.plotjax = this;
//
//	we keep a cache of image references for faster reloads
//
	this.images = {};
/*
 *	create a font foundry, and install default fonts
 *	note that we may have to do continuation here before drawing
 *	any provided chart data, since the foundry takes awhile to load
 *	contentJson may be JSON or an object or a function; if the latter,
 *	its an onload
 */
	this.foundry = new PlotJax.Foundry(this.chartctx, PlotJax.dfltFonts);
 	if (contentJson != null) {
		this.loadData(contentJson);
 	}
};

PlotJax.SHADOW = "rgba(0,0,0,0.1)";
PlotJax.SHADOW_OFFSET = 10;

PlotJax.PLOT_LINE  = 1;
PlotJax.PLOT_POINT  = 2;
PlotJax.PLOT_AREA  = 4;
PlotJax.PLOT_BOX  = 8;
PlotJax.PLOT_PIE  = 16;
PlotJax.PLOT_HBAR  = 32;
PlotJax.PLOT_VBAR  = 64;
PlotJax.PLOT_CANDLE  = 128;
PlotJax.PLOT_GANTT  = 256;
PlotJax.PLOT_QUADTREE  = 512;
PlotJax.PLOT_PARETO  = 2048;
PlotJax.PLOT_BUBBLE  = 4096;
PlotJax.PLOT_GAUGE  = 8192;
PlotJax.PLOT_LINEFIT  = 16384;
PlotJax.PLOT_POLYFIT  = 32768;
PlotJax.PLOT_EXPFIT  = 65536;
PlotJax.PLOT_BULLET  = 131072;

/*
 *	linestyles
 */
PlotJax.DASH_LEN = 6;
PlotJax.DOT_LEN = 3;

PlotJax.NEEDSY = {
	"box":false,
    "bubble":true,
	"line":true,
	"point":true,
	"scatter":true,
	"area":true,
	"vbar":true,
	"cvbar":true,
	"hbar":true,
	"chbar":true,
	"pie":true,
	"segpie":true,
	"candle":true,
	"pareto":true,
	"linefit":true,
	"polyfit":true,
	"expfit":true
};

PlotJax.NEED_AXIS = {
    "box":true,
    "bubble":true,
	"line":true,
	"point":true,
	"scatter":true,
	"area":true,
	"vbar":true,
	"cvbar":true,
	"hbar":true,
	"chbar":true,
	"candle":true,
	"pareto":true,
	"linefit":true,
	"polyfit":true,
	"expfit":true,

	"pie":false,
	"segpie":false,
	"quadtree":false,
	"treemap":false,
	"bullet":false,
	"gauge":false,
	"halfgauge":false,
	"vstripgauge":false,
	"hstripgauge":false
};

PlotJax.NEEDS_SYMDOMAIN = {
	"box":false,
    "bubble":false,
	"line":false,
	"point":false,
	"scatter":false,
	"area":false,
	"vbar":true,
	"cvbar":true,
	"hbar":true,
	"chbar":true,
	"pie":true,
	"segpie":true,
	"candle":true,
	"pareto":true,
	"linefit":false,
	"polyfit":false,
	"expfit":false
};

PlotJax.PLOTMASK = {
    "box":PlotJax.PLOT_BOX,
    "bubble":PlotJax.PLOT_BUBBLE,
	"line":PlotJax.PLOT_LINE,
	"point":PlotJax.PLOT_POINT,
	"scatter":PlotJax.PLOT_POINT,
	"area":PlotJax.PLOT_AREA,
	"vbar":PlotJax.PLOT_VBAR,
	"cvbar":PlotJax.PLOT_VBAR,
	"hbar":PlotJax.PLOT_HBAR,
	"chbar":PlotJax.PLOT_HBAR,
	"pie":PlotJax.PLOT_PIE,
	"segpie":PlotJax.PLOT_PIE,
	"candle":PlotJax.PLOT_CANDLE,
	"pareto":PlotJax.PLOT_PARETO,
	"linefit":PlotJax.PLOT_LINEFIT,
	"polyfit":PlotJax.PLOT_POLYFIT,
	"expfit":PlotJax.PLOT_EXPFIT,
	"quadtree":PlotJax.PLOT_QUADTREE,
	"treemap":PlotJax.PLOT_QUADTREE,
	"bullet":PlotJax.PLOT_BULLET,
	"gauge":PlotJax.PLOT_GAUGE,
	"halfgauge":PlotJax.PLOT_GAUGE,
	"vstripgauge":PlotJax.PLOT_GAUGE,
	"hstripgauge":PlotJax.PLOT_GAUGE
};

PlotJax.COMPATIBLE = {
	"line":{
		"box":true,
		"expfit":true,
		"linefit":true,
		"polyfit":true,
		"point":true,
		"scatter":true,
		"line":true,
		"area":true,
		"vbar":true,
		"candle":true
	},
	"point":{
		"box":true,
		"expfit":true,
		"linefit":true,
		"polyfit":true,
		"point":true,
		"scatter":true,
		"line":true,
		"area":true,
		"vbar":true,
		"candle":true
	},
	"scatter":{
		"box":true,
		"expfit":true,
		"linefit":true,
		"polyfit":true,
		"point":true,
		"scatter":true,
		"line":true,
		"area":true,
		"vbar":true,
		"candle":true
	},
	"area":{
		"box":true,
		"expfit":true,
		"linefit":true,
		"polyfit":true,
		"point":true,
		"scatter":true,
		"line":true,
		"area":true,
		"vbar":true,
		"candle":true
	},
	"vbar":{
		"box":true,
		"expfit":true,
		"linefit":true,
		"polyfit":true,
		"point":true,
		"scatter":true,
		"line":true,
		"area":true,
		"vbar":true,
		"candle":true
	},
	"cvbar":{
		"cvbar" : true
	},
	"hbar":{
		"hbar" : true
	},
	"chbar":{
		"chbar" : true
	},
	"candle":{
		"box":true,
		"expfit":true,
		"linefit":true,
		"polyfit":true,
		"point":true,
		"scatter":true,
		"line":true,
		"area":true,
		"vbar":true,
		"candle":true
	},
	"linefit":{
		"box":true,
		"expfit":true,
		"linefit":true,
		"polyfit":true,
		"point":true,
		"scatter":true,
		"line":true,
		"area":true,
		"vbar":true,
		"candle":true
	},
	"polyfit":{
		"box":true,
		"expfit":true,
		"linefit":true,
		"polyfit":true,
		"point":true,
		"scatter":true,
		"line":true,
		"area":true,
		"vbar":true,
		"candle":true
	},
	"expfit":{
		"box":true,
		"expfit":true,
		"linefit":true,
		"polyfit":true,
		"point":true,
		"scatter":true,
		"line":true,
		"area":true,
		"vbar":true,
		"candle":true
	},

	"bullet":{ "bullet" : true },
    "bubble":{},
	"pareto":{},
	"pie":{},
	"segpie":{},
	"quadtree":{},
	"gauge":{},
	"halfgauge":{},
	"vstripgauge":{},
	"hstripgauge":{}
};


PlotJax.COLORS = {
'aliceblue' : [240,248,255],
'antiquewhite' : [250,235,215],
'aqua' : [0,255,255],
'aquamarine' : [127,255,212],
'azure' : [240,255,255],
'beige' : [245,245,220],
'bisque' : [255,228,196],
'black' : [0,0,0],
'blanchedalmond' : [255,235,205],
'blue' : [0,0,255],
'blueviolet' : [138,43,226],
'brown' : [165,42,42],
'burlywood' : [222,184,135],
'cadetblue' : [95,158,160],
'chartreuse' : [127,255,0],
'chocolate' : [210,105,30],
'coral' : [255,127,80],
'cornflowerblue' : [100,149,237],
'cornsilk' : [255,248,220],
'crimson' : [220,20,60],
'cyan' : [0,255,255],
'darkblue' : [0,0,139],
'darkcyan' : [0,139,139],
'darkgoldenrod' : [184,134,11],
'darkgray' : [169,169,169],
'darkgreen' : [0,100,0],
'darkgrey' : [169,169,169],
'darkkhaki' : [189,183,107],
'darkmagenta' : [139,0,139],
'darkolivegreen' : [85,107,47],
'darkorange' : [255,140,0],
'darkorchid' : [153,50,204],
'darkred' : [139,0,0],
'darksalmon' : [233,150,122],
'darkseagreen' : [143,188,143],
'darkslateblue' : [72,61,139],
'darkslategray' : [47,79,79],
'darkslategrey' : [47,79,79],
'darkturquoise' : [0,206,209],
'darkviolet' : [148,0,211],
'deeppink' : [255,20,147],
'deepskyblue' : [0,191,255],
'dimgray' : [105,105,105],
'dimgrey' : [105,105,105],
'dodgerblue' : [30,144,255],
'firebrick' : [178,34,34],
'floralwhite' : [255,250,240],
'forestgreen' : [34,139,34],
'fuchsia' : [255,0,255],
'gainsboro' : [220,220,220],
'ghostwhite' : [248,248,255],
'gold' : [255,215,0],
'goldenrod' : [218,165,32],
'gray' : [128,128,128],
'green' : [0,128,0],
'greenyellow' : [173,255,47],
'grey' : [128,128,128],
'honeydew' : [240,255,240],
'hotpink' : [255,105,180],
'indianred' : [205,92,92],
'indigo' : [75,0,130],
'ivory' : [255,255,240],
'khaki' : [240,230,140],
'lavender' : [230,230,250],
'lavenderblush' : [255,240,245],
'lawngreen' : [124,252,0],
'lemonchiffon' : [255,250,205],
'lightblue' : [173,216,230],
'lightcoral' : [240,128,128],
'lightcyan' : [224,255,255],
'lightgoldenrodyellow' : [250,250,210],
'lightgray' : [211,211,211],
'lightgreen' : [144,238,144],
'lightgrey' : [211,211,211],
'lightpink' : [255,182,193],
'lightsalmon' : [255,160,122],
'lightseagreen' : [32,178,170],
'lightskyblue' : [135,206,250],
'lightslategray' : [119,136,153],
'lightslategrey' : [119,136,153],
'lightsteelblue' : [176,196,222],
'lightyellow' : [255,255,224],
'lime' : [0,255,0],
'limegreen' : [50,205,50],
'linen' : [250,240,230],
'magenta' : [255,0,255],
'maroon' : [128,0,0],
'mediumaquamarine' : [102,205,170],
'mediumblue' : [0,0,205],
'mediumorchid' : [186,85,211],
'mediumpurple' : [147,112,216],
'mediumseagreen' : [60,179,113],
'mediumslateblue' : [123,104,238],
'mediumspringgreen' : [0,250,154],
'mediumturquoise' : [72,209,204],
'mediumvioletred' : [199,21,133],
'midnightblue' : [25,25,112],
'mintcream' : [245,255,250],
'mistyrose' : [255,228,225],
'moccasin' : [255,228,181],
'navajowhite' : [255,222,173],
'navy' : [0,0,128],
'oldlace' : [253,245,230],
'olive' : [128,128,0],
'olivedrab' : [107,142,35],
'orange' : [255,165,0],
'orangered' : [255,69,0],
'orchid' : [218,112,214],
'palegoldenrod' : [238,232,170],
'palegreen' : [152,251,152],
'paleturquoise' : [175,238,238],
'palevioletred' : [216,112,147],
'papayawhip' : [255,239,213],
'peachpuff' : [255,218,185],
'peru' : [205,133,63],
'pink' : [255,192,203],
'plum' : [221,160,221],
'powderblue' : [176,224,230],
'purple' : [128,0,128],
'red' : [255,0,0],
'rosybrown' : [188,143,143],
'royalblue' : [65,105,225],
'saddlebrown' : [139,69,19],
'salmon' : [250,128,114],
'sandybrown' : [244,164,96],
'seagreen' : [46,139,87],
'seashell' : [255,245,238],
'sienna' : [160,82,45],
'silver' : [192,192,192],
'skyblue' : [135,206,235],
'slateblue' : [106,90,205],
'slategray' : [112,128,144],
'slategrey' : [112,128,144],
'snow' : [255,250,250],
'springgreen' : [0,255,127],
'steelblue' : [70,130,180],
'tan' : [210,180,140],
'teal' : [0,128,128],
'thistle' : [216,191,216],
'tomato' : [255,99,71],
'turquoise' : [64,224,208],
'violet' : [238,130,238],
'wheat' : [245,222,179],
'white' : [255,255,255],
'whitesmoke' : [245,245,245],
'yellow' : [255,255,0],
'yellowgreen' : [154,205,50]
};

PlotJax.dfltFonts = {
	'TickFont': {
		"Description": {
			"Family": "Arial",
			"Size": 9,
			"Color": "black",
			"Opacity": 1.0
		},
	},
	'LabelFont': {
		"Description": {
			"Family": "Arial",
			"Size": 14,
			"Color": "black",
			"Opacity": 1.0,
			"Weight": "bold"
		},
	},
	'TitleFont': {
		"Description": {
			"Family": "Arial",
			"Size": 18,
			"Color": "black",
			"Opacity": 1.0,
			"Weight": "bold"
		},
	},
	'SignatureFont': {
		"Description": {
			"Family": "Arial",
			"Size": 10,
			"Color": "black",
			"Opacity": 1.0
		},
	},
	'LegendFont': {
		"Description": {
			"Family": "Arial",
			"Size": 8,
			"Color": "black",
			"Opacity": 1.0
		},
	}
};

PlotJax.prototype = {
/*
 *	compute absolute position of the canvas, then the
 *	position of the chart area
 */
	getCanvasCoords : function() {
		var curleft = 0;
		var curtop = 0;
		var obj = this.chartcnvs;
		if (obj.offsetParent) {
			do {
				curleft += obj.offsetLeft;
				curtop += obj.offsetTop;
				obj = obj.offsetParent;
			} while (obj != null);
		}
		return [curleft, curtop];
	},


	clearHover : function() {
		this.hoverdiv.style.zIndex = -1;
		this.hoverdiv.style.visibility = "hidden";
		this.hoverx = this.hovery = null;
	 	if (this.hovertime) {
	 		clearTimeout(this.hovertime);
	 		this.hovertime = null;
	 	}
	},

	mouseClick : function(evt) {
		this.clearHover();
		if (evt == null) {	// for IE
			evt = window.event;
		}
		var x = (evt.clientX != null) ? evt.clientX : evt.pageX;
		var y = (evt.clientY != null) ? evt.clientY : evt.pageY;
		var scrollx, scrolly;
		scrollX = window.pageXOffset;
		scrollY = window.pageYOffset;
//
//	adapt to any scrolling
//
		x += scrollX;
		y += scrollY;

		var cnvscoords = this.getCanvasCoords();
		var chartx = x - cnvscoords[0];
		var charty = y - cnvscoords[1];

		if (this.gesture != null) {
			var rc = this.gesture.mouseClick(chartx, charty);
			if (rc > 0) {
				this.buttonbar.update();
				this.redraw();
			}
			if (rc >= 0) {
				return false;
			}
		}

		if (PlotJax.Balloon != null) {
			this.balloon.close();
			this.streamballoon.close();
			if ((this.helper != null) && (this.helper.getBalloonContent != null)) {
/*
 *	collect captured points from all graphs
 *	if more than 1 data point, open with capture list
 */
 				var captured = [];
 				var capture_list = [];
 				var capture_count = 0;
 				var capture_width = 0;
 				var capture_height = 0;
				for (var i = 0; (i < this.charts.length); i++) {
					captured = this.charts[i].onclick(chartx, charty);
					if (captured != null) {
						capture_list.push(this.charts[i], captured);
						capture_count += captured.length;
					}
				}
				if (capture_count == 1) {
				//
				//	single element, just open balloon
				//
					if (this.balloonIsOffset) {
						this.balloon.offsetBalloon();
					}
					else {
						this.balloon.centerBalloon();
					}
					var content = this.helper.getBalloonContent(this, capture_list[0], capture_list[1][0]);
					if (content != null) {
		 				this.balloon.open(x, y, content);
					}
				}
				else if (capture_count > 1) {
					if (this.balloonIsOffset) {
						this.streamballoon.offsetBalloon();
					}
					else {
						this.streamballoon.centerBalloon();
					}
					this.streamballoon.open(x, y, capture_list);
				}
			}
		}
		return false;	// don't propagate
	},

	keyDown : function(evt) {
//	alert("keydown");
		this.clearHover();
	
		if (this.gesture != null) {
			if (evt == null) {	// for IE
				evt = window.event;
			}
			if (evt.ctrlKey && (!evt.shiftKey) && (!evt.altKey) && (!evt.metaKey)) {
				var code = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
				var c = String.fromCharCode(code).toLowerCase();
				if (((c == "z") && (this.gesture.undo() > 0)) ||
					((c == "y") && (this.gesture.redo() > 0))) {
					this.redraw();
				}
			}
		}
	},

	onZoom : function() {
		if (this.gesture.zoom() > 0) {
			this.redraw();
		}
		else {
			this.gesture.clearZooms();
		}
		this.buttonbar.update();
		return false;
	},

	onUndo : function() {
		if (this.gesture.undo() > 0) {
			this.redraw();
		}
		this.buttonbar.update();
		return false;
	},

	onRedo : function() {
		if (this.gesture.redo() > 0) {
			this.redraw();
		}
		this.buttonbar.update();
		return false;
	},

	onClear : function() { 
		this.gesture.clearZooms();
		this.buttonbar.update();
		return false;
	},

	mouseMove : function(evt) {
		this.clearHover();
		if (evt == null) {	// for IE
			evt = window.event;
		}

		var button = this.mousedown;
		if ((this.gesture != null) && button) {
			return this.gesture.updateMouseXY(evt);
		}
		if ((this.helper == null) || (this.helper.onhover == null) ||
			((this.balloon != null) && (this.balloon.isOpen() || this.streamballoon.isOpen()))) {
			return false;
		}
/*
 *	start new hover timer
 */
		this.hoverx = (evt.clientX != null) ? evt.clientX : evt.pageX;
		this.hovery = (evt.clientY != null) ? evt.clientY : evt.pageY;

		var scrollx, scrolly;
		scrollX = window.pageXOffset;
		scrollY = window.pageYOffset;
//
//	adapt to any scrolling
//
		this.hoverx += scrollX;
		this.hovery += scrollY;

 		this.hovertime = setTimeout("document.getElementById('" + this.myid + "').plotjax.hover()", 400);
		return true;
	},

	hover : function() {
		if (this.hoverx != null) { 
			var cnvscoords = this.getCanvasCoords();
			var chartx = this.hoverx - cnvscoords[0];
			var charty = this.hovery - cnvscoords[1];
			for (var i = 0; (i < this.charts.length); i++) {
				var content = this.charts[i].onhover(chartx, charty);
				if (content != null) {
					this.hoverdiv.innerHTML = content;
					this.hoverdiv.style.left = chartx + 5;
					this.hoverdiv.style.top = charty - 13;
					this.hoverdiv.style.zIndex = 100;
					this.hoverdiv.style.visibility = "visible";
					break;
				}
			}
		}
	},

	mouseDown : function(evt) {
		this.clearHover();
		this.mousedown = true;
		return ((this.gesture != null) && (evt != null) && (evt.button == 0)) ?
			this.gesture.startDraw() : false;
	},

	mouseUp : function(evt) {
		this.clearHover();
		this.mousedown = false;
		if ((this.gesture != null) && this.gesture.endDraw()) {
			this.buttonbar.update();
		}
		return false;
	},

	// compute left/right/top/bottom margins based on various
	// decorations; may be called recursively if x axis ticks
	// require rotation
	computeMargins : function(xticks, yticks, zticks) {

		var leftmargin = 10;
		var botmargin = (this.buttonbar != null) ? 3 + this.buttonbar.getHeight() : 3;
		var rtmargin = 10;
		var topmargin = 10;
		var bbox = null;

		if (this.signature != null) 
		{
			botmargin += this.signatureFont.getHeight() + 4;
		}
		if (this.title != null) 
		{
			if (typeof this.titlePos == "string") 
			{
				if (this.titlePos == "bottom") 
				{
					botmargin += this.titleFont.getHeight(this.title) + 10;
				}
				else 
				{
					topmargin += this.titleFont.getHeight(this.title) + 10;
				}
			}
			else 
			{	// absolute position, determine if its top or bottom
				var y = this.titlePos[1];
				if (y > (this.height >> 1))
				{	// bottom half
					botmargin = (this.height - y) + 5;
				}
				else 
				{	// top half
					topmargin = y + 5;
				}
			}
		}

		botmargin += this.labelFont.getHeight() + 5;
		rtmargin += 20;
		this.xLabelPos = [ 
			this.width - rtmargin - (this.labelFont.getWidth(this.xType.getLabel()) >> 1), 
			this.height - botmargin - 5
		];
		if (xticks == null)
		{ // no axes, use minimal margins
			this.leftmargin = leftmargin;
			this.botmargin = botmargin;
			this.rtmargin = rtmargin;
			this.topmargin = topmargin;
			this.chartw = this.width - leftmargin - rtmargin;
			this.charth = this.height - topmargin - botmargin;
			return;
		}
		/*
		 *	get a template minimum from the type, in case a zoom
		 *	causes a precision expansion
		 */
		var maxtick = 0;
		var ticksw = 5 * xticks.length;
		var sumticks = 5 * (xticks.length >> 1);
		if (this.tickFont != null)
		{
			maxtick = this.tickFont.getHeight();
			ticksw = (this.tickFont.getWidth(this.xType.getTemplate()) + 5) * xticks.length;
			sumticks = 0;
			var i = 0;
			var ht = this.tickFont.getHeight();
			var wd = (this.xAngle == 90) ? this.tickFont.getHeight() : 0;
			var ticksin = Math.sin((Math.PI/180) * this.xAngle);
			var tickcos = Math.cos((Math.PI/180) * this.xAngle);

			for (i = 0; i < xticks.length; i += 2) 
			{
				if (this.xAngle != 0)
				{
					ht = (this.xAngle == 90) 
						? this.tickFont.getWidth(xticks[i]) 
						: Math.ceil(ticksin * this.tickFont.getWidth(xticks[i]));
				}
				maxtick = Math.max(maxtick, ht);

				if (this.xAngle != 90)
				{
					wd = (this.xAngle == 0) 
						? this.tickFont.getWidth(xticks[i])
						: Math.ceil(tickcos * this.tickFont.getWidth(xticks[i]));
				}
				sumticks += 5 + wd;
			}
		}
		/*
		 *	!!!NOTE: for horizontal bars, we may need to apply this
		 *	to the Y axis instead
		 */
		ticksw = Math.max(ticksw, sumticks);
		if ((this.xAngle == 0) && (ticksw > this.width))
		{
			this.xAngle = 135;
			return this.computeMargins(xticks, yticks, zticks);
		}
		botmargin += maxtick + 4;
		/*
		 *	get a template minimum from the type, in case a zoom
		 *	causes a precision expansion
		 *	alas, it looks terrible in cases where the tick format is
		 *	much smaller than the template, so we'll avoid this for now,
		 *	and just cheat by adding a couple spaces
		 */
		maxtick = 0;
		/*
		 *	this may be a waste of effort...
		 */
		if (this.tickFont != null)
		{
			for (i = 0; i < yticks.length; i += 2) 
			{
				maxtick = Math.max(maxtick, this.tickFont.getWidth(yticks[i] + "00"));
			}
		}
		if (!this.yRotate) 
		{	// y label is centered atop axis
			maxtick = Math.max(maxtick, (this.labelFont.getWidth(this.yType.getLabel()) >> 1));
			leftmargin += maxtick + 2;
			topmargin += this.labelFont.getHeight() + 6;
			this.yLabelPos = [ leftmargin, topmargin - 6 ];
		}
		else 
		{ // y label is rotated and centered along left
			this.yLabelPos = [ leftmargin,
				topmargin + ((this.height - botmargin - topmargin) >> 1) + 
					(this.labelFont.getWidth(this.yType.getLabel()) >> 1)
			];
			leftmargin += 10 + maxtick + 2 + 5 + this.labelFont.getHeight();
			topmargin += 6;
		}
		this.leftmargin = leftmargin;
		this.botmargin = botmargin;
		this.rtmargin = rtmargin;
		this.topmargin = topmargin;
		this.chartw = this.width - leftmargin - rtmargin;
		this.charth = this.height - topmargin - botmargin;
		if ((this.xAngle == 0) && (ticksw > this.chartw))
		{
			this.xAngle = 135;
			return this.computeMargins(xticks, yticks, zticks);
		}
		if (this.xCenterLabel) 
		{
			this.xLabelPos[0] = this.leftmargin + (this.chartw >> 1);
		}
	},

//
//	sets xscale, yscale, and edge values used in pt2pxl
//	also adjusts min or max of barcharts to clip away origin
//	also adjusts tick arrays for any padding
//
	computeScales : function(sticky, xticks, yticks, zticks) {
		if ((this.plottypes == PlotJax.PLOT_PIE) || (this.plottypes == PlotJax.PLOT_QUADTREE)) {
			return this;
		}

		var yrange = this.yType.getLimits();
		var yl = yrange[0];
//
//	for barcharts, we may have clustered bars, and need to force all
//	bars to clip away from the same limit
//
		if (((this.plottypes & (PlotJax.PLOT_VBAR|PlotJax.PLOT_HBAR)) != 0) && 
			(yl > 0) && (! (this.yType.needOrigin() && sticky))) {
//
//	adjust mins to clip away from origin
//
			for (var i = 0; i < this.charts.length; i++) {
				var datastack = this.charts[i].chartdesc.Data;
				var j = 1;
				while (j < datastack.length) {
					datastack[j][0] = yl;
					j += 3;
				}
			}
		}
//
// calculate axis scales 
		if ((this.zType != null) || this.threed) {
			var tht = this.charth;
			var twd = this.chartw;
			var xcard = this.xType.cardinality();
			var zcard = (this.zType != null) ? this.zType.cardinality() : 1;
//
//	compute ratio of Z values to X values
//	to adjust percent of plot area reserved for
//	depth. Max is 40%, min is 10%
//
			var xzratio = zcard/(xcard * this.data.length);
//
//	compute actual height as adjusted height x (1 - depth ratio)
//	actual depth is based on 30 deg. rotation of adjusted
//	width x depth ratio
//	actual width is adjust width - the 30 deg. rotation effect
//
			if ((this.plottypes & (PlotJax.PLOT_HBAR|PlotJax.PLOT_GANTT)) == 0) {
				this.plotWidth = Math.floor(twd / (xzratio*Math.sin(Math.PI/6) + 1));
				this.plotDepth = Math.floor((twd - this.plotWidth)/Math.sin(Math.PI/6));
				this.plotHeight = Math.floor(tht - (this.plotDepth*Math.cos(Math.PI/3)));
				this.xType.setScale(this.plotWidth);
				this.yType.setScale(this.plotHeight);
				this.zType.setScale(this.plotDepth);
			}
			else {
				this.plotHeight = Math.floor(tht / (xzratio*Math.cos(Math.PI/6) + 1));
				this.plotDepth = Math.floor((tht - this.plotHeight)/Math.cos(Math.PI/6));
				this.plotWidth = Math.floor(twd - (this.plotDepth*Math.sin(Math.PI/6)));
				this.yType.setScale(this.plotWidth);
				this.xType.setScale(this.plotHeight);
				this.zType.setScale(this.plotDepth);
			}
		}
		else {
//	keep true width/height for future reference
			if ((this.plottypes & (PlotJax.PLOT_HBAR|PlotJax.PLOT_GANTT)) == 0) {
		//
		//	bubble charts need some padding to assure the bubble
		//	fits entirely in the grid
		//
				var padding = ((this.plottypes & PlotJax.PLOT_BUBBLE) != 0) ?
					Math.ceil(Math.sqrt(PlotJax.Bubble.MAX_FACTOR * (this.chartw * this.charth))/2) : 0;
				this.xType.setScale(this.chartw, padding, xticks);
				this.yType.setScale(this.charth, padding, yticks);
			}
			else {	// horiz. bars ar Gantt only
				this.yType.setScale(this.chartw);
				this.xType.setScale(this.charth);
			}
		}
//
//	compute spacing info for bar/candles
//
		if (((this.plottypes & (PlotJax.PLOT_VBAR|PlotJax.PLOT_HBAR)) != 0) && 
			(this.zType == null) && (! this.computeSpacing(this.plottypes))) {
			return null;
		}

		this.vertEdge = this.height - this.botmargin;
		return this;
	},

//
//	compute bar spacing
//
	computeSpacing : function(plottype) {
//
//	get number of domain values and number of bars per domain value
//
		var xcard = this.xType.cardinality();
		var domains = this.bars_per_domain;
		var bars = (xcard != 0) ? xcard : this.barCount;
		var spacer = 10;
		var width = ((plottype & PlotJax.PLOT_HBAR) != 0) ? this.charth : this.chartw;
		var pxlsperdom = Math.floor(width/(xcard+1)) - spacer;

		if (pxlsperdom < 2) {
			alert('Insufficient width for number of domain values.');
			return null;
		}
//
//	compute width of each bar from number of bars per domain value
//
		var pxlsperbar = Math.floor(pxlsperdom/domains);
		if (pxlsperbar < 2) {
			alert('Insufficient width for number of ranges or values.');
			return null;
		}
//
//	now traverse the charts and specify their offset;
//	they report back the updated how much space they used (e.g., if an iconic bar)
//	until we run out of space OR all charts report. Note that iconics will always
//	use the specified pxlsperbar minimum
//
		var offset = - (pxlsperdom >> 1);
		for (var i = 0; i < this.charts.length; i++) {
			if (this.charts[i].setBarOffsets != null) {
				offset = this.charts[i].setBarOffsets(offset, pxlsperbar);
				if (offset == null) {
					return null;
				}
			}
		}
		return this;
	},

	drawBottomAxis : function(xticks, yticks) {
/*
 *	draw axis and label
 *	if not numeric, OR number range does not include origin, draw
 *	at baseline
 */
		var i, txt, bbox;
	   	var prevx = 0;
		var xl = this.leftmargin;
		var tfh = (this.tickFont != null) ? this.tickFont.getHeight() : 0;
/*
 *	precompute label skips
 */
	 	var skip = 0;
	 	if (this.tickFont != null) 
	 	{
	 		for (i = 0; (i < xticks.length) && (skip == 0); i += 2) 
	 		{
				xl = this.xType.pt2pxl(xticks[i+1], this.leftmargin);
				txt = xticks[i];
				if ((txt.length > 25) && this.isSymbolDomain) 
				{
					txt = txt.substr(0, 22) + '...';
				}

				if ((this.xAngle != null) && (this.xAngle != 0)) 
				{
					if (tfh + 2 > (xl - prevx)) 
					{
						skip = 1;
					}
				}
				else if (this.tickFont.getWidth(txt) > (xl - prevx)) 
				{
					skip = 1;
				}
				prevx = xl;
			}
		}

		xl = this.leftmargin;
		var drawGrid = ((this.yType != null) && (this.gridDirection != null) && 
			((this.gridDirection == 'vertical') || (this.gridDirection == 'both')));

		var yl = this.height - this.botmargin;
		var labeldelta = this.xLabelPos[1] - yl;
		if ((this.yType != null) && (this.yType instanceof PlotJax.NumberType) &&
			(yticks[1] <= 0) && (yticks[yticks.length-1] >= 0)) {
			yl = this.vertEdge - this.yType.pt2pxl(0, 0);
			if ((this.gridDirection == null) && (this.axisColor != null) && (!this.xCenterLabel)) {
		// adjust label position too
				this.xLabelPos[1] = yl + labeldelta;
			}
		}
		this.axisCoords[1] = yl;
//
//	only draw if color defined
//
		if (this.axisColor != null) {
			this.drawLine([xl, yl, xl + this.chartw, yl], this.axisColor, 2, 'solid');
		}
	 	this.labelFont.drawBelow(this.xType.getLabel(), this.xLabelPos[0], this.xLabelPos[1], 4);
//
// draw ticks at either the axis location, or, if an offset origin w/ a grid, at
//	the bottom
// 
		var tickpos = ((this.gridDirection != null) || (this.axisColor == null)) ? this.height - this.botmargin : yl;
/*
 *	note that logarithmic axis ticks are computed in the type object
 */
	 	for (i = 0; i < xticks.length; i += 2) 
	 	{
			xl = this.xType.pt2pxl(xticks[i+1], this.leftmargin);
			if ((this.gridDirection == null) && (this.axisColor != null)) {
				this.drawLine([xl, tickpos + 4, xl, tickpos], this.axisColor, 1, 'solid');
			}
			if (this.tickFont == null) { continue; }
//
//	we're not gonna worry about label overlap for now
//	truncate long labels
//
			txt = xticks[i];
			if ((txt.length > 25) && this.isSymbolDomain) {	txt = txt.substr(0, 22) + '...'; }

			if (skip != 2) 
			{
				if ((this.xAngle != null) && (this.xAngle != 0)) {
					this.tickFont.drawAlignedTo(txt, xl + (tfh >> 1), tickpos + 3, 0, this.xAngle);
				}
				else {
					this.tickFont.drawBelow(txt, xl, tickpos + 2, 2, 0);
				}
				if (skip == 1) { skip++; }
			}
			else 
			{
				skip = 1;
			}
		}
		return this;
	},

	drawVerticalGrid : function(xticks, yticks) {
/*
 *	draw grid
 */
		var xl = this.leftmargin;
		var yl = this.height - this.botmargin;
		var yh = this.topmargin;
		this.drawLine([xl, yh, xl + this.chartw, yh], this.gridColor, 1, this.gridPattern);
		if ((this.axisColor == null) ||
			((this.yType != null) && (this.yType instanceof PlotJax.NumberType) &&
				(yticks[1] <= 0) && (yticks[yticks.length-1] >= 0))) {
	//
	//	if no axis, or axis is not at bottom, then close out the grid
	//
			this.drawLine([xl, yl, xl + this.chartw, yl], this.gridColor, 1, this.gridPattern);
		}
/*
 *	note that logarithmic axis ticks are computed in the type object
 */
	 	for (var i = 1; i < xticks.length; i += 2) {
			xl = this.xType.pt2pxl(xticks[i], this.leftmargin);
			this.drawLine([xl, yh, xl, yl], this.gridColor, 1, this.gridPattern);
		}
		return this;
	},

	drawBands : function(ticks) {
		var xl = this.leftmargin;
		var xh = xl + this.chartw;
		var yh = 0;
		var yl = this.vertEdge - this.yType.pt2pxl(this.bands[0], 0);
		var i = 0;
		if (this.bands instanceof Array) {
			for (i = 1; i < this.bands.length; i += 2) {
				yh = this.vertEdge - this.yType.pt2pxl(this.bands[i+1], 0);
				this.chartctx.fillStyle = this.bands[i];
				this.chartctx.fillRect(xl, yl, this.chartw, yh - yl);
				yl = yh;
			}
			return this;
		}
		var band = false;
		this.chartctx.fillStyle = this.bands;
		for (i = 1; i < ticks.length - 2; i += 2) {
			if (band && ((i + 2) < ticks.length) ) {
				band = false;
				yl = this.vertEdge - this.yType.pt2pxl(ticks[i], 0);
				yh = this.vertEdge - this.yType.pt2pxl(ticks[i+2], 0);
				this.chartctx.fillRect(xl, yl, this.chartw, yl - yh);
			}
			else {
				band = true;
			}
		}
		return this;
	},

	drawLeftAxis : function(xticks, yticks) {
/*
 *	draw axis and label
 */
		var drawGrid = ((this.gridDirection != null) && 
			((this.gridDirection == 'horizontal') || (this.gridDirection == 'both')));
		var xl = ((!(this.xType instanceof PlotJax.NumberType)) ||
			(xticks[1] >= 0) || (xticks[xticks.length-1] < 0)) ?
			this.leftmargin :
			this.xType.pt2pxl(0, this.leftmargin);
		this.axisCoords[0] = xl;
//
//	draw only if color defined
//
		if (this.axisColor != null)	{
			this.drawLine([xl, this.topmargin, xl, this.topmargin + this.charth], this.axisColor, 2, 'solid');
		}
		if (this.yRotate) {
			this.labelFont.drawAngledString(this.yType.getLabel(), this.yLabelPos[0], this.yLabelPos[1], 3, 'left', 270);
		}
		else {
			this.labelFont.drawAbove(this.yType.getLabel(), this.yLabelPos[0], this.yLabelPos[1], 2, 2);
		}
//
// draw ticks at either the axis location, or, if an offset origin w/ a grid, at
//	the left
// 
		var tickpos = ((this.gridDirection != null) || (this.axisColor == null)) ? this.leftmargin : xl;
/*
 *	note that logarithmic axis ticks are computed in the type object
 */
		var xh = xl - 4;
		var tfh = (this.tickFont != null) ? this.tickFont.getHeight() : 0;
		var prevy = this.botmargin + tfh + 10;
		var yl = 0;
		for (var i = 0; i < yticks.length; i += 2) {
			yl = this.vertEdge - this.yType.pt2pxl(yticks[i+1], 0);
			if ((this.gridDirection == null) && (this.axisColor != null)) {
				this.drawLine([xl, yl, xh, yl], this.axisColor, 1, 'solid');
			}
			if (this.tickFont == null) {
				continue;
			}
//		if (prevy - yl > tfh) {
//
//	only print label if it won't overlap prior
//
//			this.tickFont.drawLeftOf(yticks[i], tickpos, yl - (tfh >> 1), 5);
				this.tickFont.drawLeftOf(yticks[i], tickpos, yl, 5);
				prevy = yl;
//		}
		}
		return this;
	},

	drawHorizontalGrid : function(xticks, yticks) {
		var xl = this.leftmargin;
		var xh = xl + this.chartw;
		var yl = this.topmargin;
		this.drawLine([xl, yl, xh, yl], this.gridColor, 1, this.gridPattern);
		this.drawLine([xh, yl, xh, yl + this.charth], this.gridColor, 1, this.gridPattern);
//
//	close out left side if no axis, or axis is offset from edge
//
		if ((this.axisColor != null) ||
			((this.xType instanceof PlotJax.NumberType) &&
				(xticks[1] <= 0) && (xticks[xticks.length-1] >= 0))) {
			this.drawLine([xl, yl, xl, yl + this.charth], this.gridColor, 1, this.gridPattern);
		}

		for (var i = 1; i < yticks.length; i += 2) {
			yl = this.vertEdge - this.yType.pt2pxl(yticks[i], 0);
			this.drawLine([xl, yl, xh, yl], this.gridColor, 1, this.gridPattern);
		}
		return this;
	},
/*
 *	!!!NEED to add lineJoin style
 */
	drawLine : function(coords, color, width, pattern, debug) {
		if (debug == null) { debug = false; }
		if (pattern == null) {
			pattern = 'solid';
		}
		this.chartctx.strokeStyle = color;
		this.chartctx.lineWidth = (width == null) ? 1 : width;

		var i;		
		if (pattern == 'solid') {	// simplest case
			this.chartctx.beginPath();
			this.chartctx.moveTo(coords[0],coords[1]);
			for (i = 2; i < coords.length; i += 2) {
				this.chartctx.lineTo(coords[i], coords[i+1]);
			}
			this.chartctx.stroke();
			return;
		}

		var dashlen = (pattern == 'dash') ? PlotJax.DASH_LEN : PlotJax.DOT_LEN;
		var tx, ty;
		if (coords.length == 4) {	// single line segment
			if (coords[0] > coords[2]) {
				tx = coords[0];
				coords[0] = coords[2];
				coords[2] = tx;
				ty = coords[1];
				coords[1] = coords[3];
				coords[3] = tx;
			}
			if (coords[0] == coords[2]) {	// optimize for vertical line
				ty = 0;
				while (ty < (coords[3] - coords[1])) {
					this.chartctx.beginPath();
					this.chartctx.moveTo(coords[0], coords[1] + ty);
					ty += dashlen;
					this.chartctx.lineTo(coords[0], coords[1] + ty);
					this.chartctx.stroke();
					ty += PlotJax.DASH_LEN;
				}
				return;
			}

			if (coords[1] == coords[3]) {	// optimize for horizontal line
				tx = 0;
				while (tx < (coords[2] - coords[0])) {
					this.chartctx.beginPath();
					this.chartctx.moveTo(coords[0] + tx, coords[1]);
					tx += Math.min(((coords[2] - coords[0]) - tx), dashlen);
					this.chartctx.lineTo(coords[0] + tx, coords[1]);
					this.chartctx.stroke();
					tx += PlotJax.DASH_LEN;
				}
				return;
			}
		}
/*
 *	complex case: patterned angled lines
 */
	 	for (i = 0; i < coords.length - 2; i += 2) {
			var x = coords[i];
			var y = coords[i + 1];
			var dy = coords[i + 3] - y;
			var dx = coords[i + 2] - x;
			var theta = Math.atan(dy/dx);
			var linelen = Math.sqrt((dy * dy) + (dx * dx));
			var runlen = 0;
			tx = 0; ty = 0;
			while (runlen < linelen) {
				this.chartctx.beginPath();
				this.chartctx.moveTo(tx + x, ty + y);
				runlen += Math.min((linelen - runlen), PlotJax.DASH_LEN);
				tx = Math.round(runlen * Math.cos(theta));
				ty = Math.round(runlen * Math.sin(theta));
				this.chartctx.lineTo(tx + x, ty + y);
				this.chartctx.stroke();
				runlen += PlotJax.DASH_LEN;
				tx = Math.round(runlen * Math.cos(theta));
				ty = Math.round(runlen * Math.sin(theta));
			}
		}
	},

	// embed a logo image into chart background
	drawLogo : function() {
		var x = this.leftmargin;
		var y = this.topmargin;
		var w = this.chartw;
		var h = this.charth;
		if (this.logo.width < w) {
			x += ((w - this.logo.width) >> 1);
			w = this.logo.width;
		}
		if (this.logo.height < h) {
			y += ((h - this.logo.height) >> 1);
			h = this.logo.height;
		}
		this.chartctx.drawImage(this.logo, x, y, w, h);
	},

	drawRightAxis : function(ticks) {
/*
 *	draw axis and label
 */
		var xl = this.width - this.rtmargin;
		if (this.rtaxisColor != null) {
			this.drawLine([
				xl, this.topmargin, 
				xl, this.topmargin + this.charth], 
				this.y2AxisColor, 2, 'solid');
		}
		if (this.yRotate) {
			this.labelFont.drawAlignedTo(this.y2Label, this.y2LabelPos[0], this.y2LabelPos[1], 270);
		}
		else {
			this.labelFont.drawString(this.y2Label, this.y2LabelPos[0], this.y2LabelPos[1]);
		}
//
// draw ticks
/*
 *	note that logarithmic axis ticks are computed in the type object
 */
		var xh = this.horizGrid ? xl - this.chartw : xl + 2;
		var tfh = (this.tickFont != null) ? this.tickFont.getHeight() : 0;
		var prevy = this.botmargin + tfh + 10;
		var linepat = this.horizGrid ? this.gridPattern : 'solid';
		var yl = 0;
		for (var i = 0; i < ticks.length; i += 2) {
			yl = this.yType.pt2pxl(ticks[i+1], this.topmargin);
			if (this.rtaxisColor != null) {
				this.drawLine([xl, yl, xh, yl], this.gridColor, 1, linepat);
			}
			if (this.tickFont == null) {
				continue;
			}
			if (prevy - yl > tfh) {
//
//	only print label if it won't overlap prior
//
				this.tickFont.drawRightOf(ticks[i], xl, yl, 5);
				prevy = yl;
			}
		}
		return this;
	},


	getBalloon : function() { return this.balloon; },
	getStreamBalloon : function() { return this.streamballoon; },
	getGesture : function() { return this.gesture; },
	getHelper : function() { return this.helper; },
	getCanvas : function() { return this.chartctx; },
	getXType : function() { return this.xType; },
	getYType : function() { return this.yType; },
	getY2Type : function() { return this.y2Type; },
	getZType : function() { return this.zType; },
	getTickFont : function() { return this.tickFont; },
	getLabelFont : function() { return this.labelFont; },
	getLeftMargin : function() { return this.leftmargin; },
	getRightMargin : function() { return this.rtmargin; },
	getTopMargin : function() { return this.topmargin; },
	getBottomMargin : function() { return this.botmargin; },
	getChartWidth : function() { return this.chartw; },
	getChartHeight : function() { return this.charth; },
	getVertEdge : function() { return this.vertEdge; },
	getAxisCoords : function() { return this.axisCoords; },

	getType : function(typespec, needs_symdomain) {
		if (typespec.Type == null) {
			alert("Invalid type specification: no Type");
			return null;
		}
		if ((needs_symdomain != null) && needs_symdomain)  {
			return new PlotJax.SymbolType(typespec);
		}
		switch (typespec.Type.toLowerCase()) {
			case "timestamp": return new PlotJax.TimestampType(typespec);
			case "time": return new PlotJax.TimeType(typespec);
			case "date": return new PlotJax.DateType(typespec);
/*
 *	NOT YET IMPLEMENTED
		case "interval year":
		case "interval year to month":
		case "interval month":
		case "interval day":
		case "interval day to hour":
		case "interval day to minute":
		case "interval day to second":
		case "interval hour":
		case "interval hour to minute":
		case "interval hour to second":
		case "interval minute":
		case "interval minute to second":
		case "interval second":
			return new PlotJax.IntervalType(typespec);
*/
			case "number": return new PlotJax.NumberType(typespec);

			case "symbol": return new PlotJax.SymbolType(typespec);

			default:
				alert("Unknown data type " + typespec.Type);
				return null;
		}
	},

	isLoaded : function() {
		this.resource_count--;
		if (this.resource_count == 0) {
			this.continueLoad();
		}
	},

	loadData : function(dataAsJSON) {
/*
 *	if its a String, (safely!) parse/eval it 
 */
 		if (typeof dataAsJSON == "string") {
			dataAsJSON = JSON.parse(dataAsJSON);
 		}
/*
 *	clear context to initial state
 */
		this.labelFont = null;
		this.tickFont = null;
		this.titleFont = null;
		this.signatureFont = null;
		this.legendFont = null;
		this.yRotate = 0;
		this.xAngle = 0;
		this.xCenterLabel = false;
		this.xType = this.yType = this.y2Type = this.zType = null;
		this.sizeType = this.intensityType = null;
		this.xLabel = this.yLabel = this.y2Label = null;
		this.xLabelPos = this.yLabelPos = this.y2LabelPos = null;
		this.isSymDomain = false;
		this.logo = null;
		this.legendPos = null;
		this.title = null;
		this.titlePos = null;
		this.signature = null;
		this.axisColor = null;
		this.rtaxisColor = null;
		this.gridColor = this.gridPattern = this.gridDirection = this.gridOver = null;
		this.fillColor = this.bgColor = this.bgGradient = this.bands = this.shadow = null;
		this.plottypes = 0;
		this.colors = null;
		this.charts = [];
		this.axisCoords = [];
		this.leftmargin = this.topmargin = this.botmargin = this.rtmargin = null;
		this.vertEdge = this.chartw = this.charth = null;
		this.intensityFactor = null;
		this.plotHeight = this.plotWidth = this.plotDepth = null;
		this.brushWidth = null;
		if (this.gesture != null) {
			this.gesture.clear();	// remove all observers from the gesture
		}
		
//
//	need to iterate over the multiple chart properties
//	and create separate objects for each
//
		if (dataAsJSON.X == null) {
			alert("No X datatype.");
			return;
		}
		var i;
	/*
	 *	validate
	 */
		var charts = dataAsJSON.Charts;
		if (charts == null) {
			alert("No charts defined.");
			return;
		}
	/*
	 * define the various fonts
	 */
		if (dataAsJSON.Labels != null)
		{
			this.labelFont = (dataAsJSON.Labels.Font != null) 
				? this.foundry.addFontsFromJSON(dataAsJSON.Labels.Font)
				: this.foundry.getFontByName("LabelFont");

			this.yRotate = ((dataAsJSON.Labels.YRotate != null) && dataAsJSON.Labels.YRotate);
			this.xCenterLabel = 
				((dataAsJSON.Labels.XPosition != null) && 
					(dataAsJSON.Labels.XPosition.toLowerCase() == 'center')) ? true : false;
		}

		if (dataAsJSON.Ticks != null)
		{
			if (typeof dataAsJSON.Ticks == "object") 
			{
				this.xAngle = dataAsJSON.Ticks.XAngle;
				this.tickFont = (dataAsJSON.Ticks.Font != null)
					? this.foundry.addFontsFromJSON(dataAsJSON.Ticks.Font)
					: this.foundry.getFontByName("TickFont");
			}
			else if (dataAsJSON.Ticks.toLowerCase() == "none") 
			{
				this.tickFont = null;
			}
			else
			{
				this.tickFont = this.foundry.getFontByName("TickFont");
			}
		}

		if (dataAsJSON.Legend != null)
		{
			this.legendFont = (dataAsJSON.Legend.Font != null)
				? this.foundry.addFontsFromJSON(dataAsJSON.Legend.Font)
				: this.foundry.getFontByName("LegendFont");
			this.legendPos = (dataAsJSON.Legend.Position != null) ? 
				dataAsJSON.Legend.Position.toLowerCase() : 'left';
		}

		if (dataAsJSON.Title != null)
		{
			this.titleFont = (dataAsJSON.Title.Font != null)
				? this.foundry.addFontsFromJSON(dataAsJSON.Title.Font)
				: this.foundry.getFontByName("TitleFont");

			this.title = dataAsJSON.Title.String;
//
//	check if title fits in bounds; if not, word wrap
//
			if (this.titleFont.getWidth(this.title) > (this.width - 20)) 
			{
				var parts = this.title.split(" ");
				var curlen = this.titleFont.getWidth(parts[0] + " ");
				this.title = parts[0];
				for (i = 1; i < parts.length; i++) 
				{
					var w = this.titleFont.getWidth(parts[i] + " ");
					if ((curlen + w) < (this.width - 20)) 
					{
						this.title += " " + parts[i];
						curlen += w;
					}
					else 
					{
						curlen = w;
						this.title += "\n" + parts[i];
					}
				}
			}
			this.titlePos = (dataAsJSON.Title.Position != null) ?
				((typeof dataAsJSON.Title.Position == "string") ?
					dataAsJSON.Title.Position.toLowerCase() :
					dataAsJSON.Title.Position) :
				"bottom";
		}

		if (dataAsJSON.Signature != null)
		{
			this.signatureFont = (dataAsJSON.Signature.Font != null)
				? this.foundry.addFontsFromJSON(dataAsJSON.Signature.Font)
				: this.foundry.getFontByName("SignatureFont");				
			this.signature = dataAsJSON.Signature.String;
			this.signaturePos = dataAsJSON.Signature.Position;
		}


		if (this.xAngle == null) {
			this.xAngle = 0;
		}
		if (this.yRotate == null) {
			this.yRotate = 0;
		}
/*
 *	set axis/grid style
 */
		this.axisColor = null;
		this.rtaxisColor = null;
 		if (dataAsJSON.AxisColor == null) {
 			this.axisColor = "rgb(0,0,0)";
 			this.rtaxisColor = "rgb(0,0,0)";
 		}
 		else if (dataAsJSON.AxisColor instanceof Array) {
 			this.axisColor = pljxGetColor(dataAsJSON.AxisColor[0], "rgb(0,0,0)");
 			this.rtaxisColor = pljxGetColor(dataAsJSON.AxisColor[1], "rgb(0,0,0)");
 		}
 		else if (dataAsJSON.AxisColor != "none") {
			this.axisColor = pljxGetColor(dataAsJSON.AxisColor, "rgb(0,0,0)");
		}

		if (dataAsJSON.Grid != null) {
			this.gridColor = pljxGetColor(dataAsJSON.Grid.Color, "rgba(0,0,0,0.5)");
			this.gridPattern = (dataAsJSON.Grid.Pattern != null) ? 
				dataAsJSON.Grid.Pattern.toLowerCase() : 'solid';
			this.gridDirection = ((dataAsJSON.Grid.Direction == null) || 
				(dataAsJSON.Grid.Direction.toLowerCase() == "none")) ? null : 
				dataAsJSON.Grid.Direction.toLowerCase();
			this.gridOver = ((dataAsJSON.Grid.Layer != null) && (dataAsJSON.Grid.Layer.toLowerCase() == 'over'));
		}
		else {
			this.gridColor = null;
			this.gridPattern = null;
			this.gridDirection = null;
			this.gridOver = false;
		}
		this.fillColor = dataAsJSON.FillColor ?
			pljxGetColor(dataAsJSON.FillColor, "rgb(255, 255, 255)") :
			pljxGetColor("rgb(255, 255, 255)");
/*
 *	set background style
 */
		if (dataAsJSON.Background != null) {
			this.bgColor = pljxGetColor(dataAsJSON.Background.Color, "rgb(255, 255, 255)");
			if (dataAsJSON.Background.Gradient != null) {
				this.bgGradient = (dataAsJSON.Background.Gradient.toLowerCase() == 'vertical');
			}
			else {
				this.bgGradient = null;
			}
			if (dataAsJSON.Background.Bands != null) {
				this.bands = (dataAsJSON.Background.Bands instanceof Array) ? 
					dataAsJSON.Background.Bands.slice(0) : 
					(dataAsJSON.Background.Bands == "") ?
						null :
						pljxGetColor(dataAsJSON.Background.Bands, "rgb(255, 255, 255)");
			}
			else {
				this.bands = null;
				this.bgGradient = null;
			}
		}
		else {
			this.bgColor = pljxGetColor("rgb(255, 255, 255)");
			this.bgGradient = null;
			this.bands = null;
		}
/*
 *	set shadow style
 */
		this.shadow = (dataAsJSON.Shadow != null) ? dataAsJSON.Shadow : false;
		if (this.shadow) {
			this.width = this.origwidth - PlotJax.SHADOW_OFFSET;
			this.height = this.origheight - PlotJax.SHADOW_OFFSET;
		}
		else {
			this.width = this.origwidth;
			this.height = this.origheight;
		}
/*
 *	set popup style
 */
		this.balloonIsOffset = ((this.balloon != null) && 
			((dataAsJSON.Balloon == null) || (dataAsJSON.Balloon != "center")));
/*
 *	set border style
 */
		if (dataAsJSON.Border != null) {
			if (typeof dataAsJSON.Border == "string") {
				this.border = (dataAsJSON.Border.toLowerCase() == "none") ?
					null : [ "black", 2 ];
			}
			else {
				this.border = [ 
					dataAsJSON.Border.Color || "black", 
					dataAsJSON.Border.Width || 2
					];
			}
		}
		else {
			this.border = [ "black", 2 ];
		}

	//
	//	accumulate all loadable resources, then initiate load on all of them
	//	and only continue when all are done
	//
		var resources = {};
		this.resource_count = 0;
		if (dataAsJSON.ImagePath != null) {
			this.imagePath = dataAsJSON.ImagePath;
			if ((this.imagePath != null) && (this.imagePath.length > 0) && 
				(this.imagePath.charAt(this.imagePath.length - 1) != "/")) { 
				this.imagePath += "/"; 
			}
		}
		var img;
		var images = {};
		if ((dataAsJSON.Logo != null) && (this.images[dataAsJSON.Logo] == null)) {
//
//	only reload if it doesn't exist
//
			img = new Image();
			img.appCtxt = this;
			img.onerror = function() {
				alert("cannot load image");
			};
			img.onload = function() {
				this.appCtxt.isLoaded();
			};
			this.logo = this.images[dataAsJSON.Logo] = images[dataAsJSON.Logo] = img;
			this.resource_count++;
		}
		for (var chart = 0; chart < dataAsJSON.Charts.length; chart++) {
			var chartstyle = dataAsJSON.Charts[chart].Style;
			if (chartstyle == null) {
				continue;
			}
			if (!(chartstyle instanceof Array)) {
				dataAsJSON.Charts[chart].Style = [ dataAsJSON.Charts[chart].Style ];
				chartstyle = dataAsJSON.Charts[chart].Style;
			}
			for (i = 0; i < chartstyle.length; i++) {
				if ((chartstyle[i].Icon != null) &&
					(this.images[chartstyle[i].Icon] == null)) {
					img = new Image();
					img.appCtxt = this;
					img.onload = function() {
						this.appCtxt.isLoaded();
					};
					this.images[chartstyle[i].Icon] = images[chartstyle[i].Icon] = img;
					this.shapes.addIcon(chartstyle[i].Icon, img);
					this.resource_count++;
				}
			}
		}
/*
 *	OK, we've catalogued everything, so start aloadin'
 */
		this.contentJson = dataAsJSON;
		if (this.resource_count == 0) {
			this.continueLoad();
		}

		for (var image in images) {
			images[image].src = this.adjustImagePath(image);
		}
	},

	adjustImagePath : function(imageurl) {
//
//	replace any existing path with provided path (if any)
//
		return (this.imagePath == null) ?
			imageurl :
			this.imagePath + imageurl.split("/").pop();
	},
	
	continueLoad : function() {
		var dataAsJSON = this.contentJson;
		var i = 0;

		var needsY = false;
		var needs_symdomain = false;
		var chart;
		for (chart = 0; chart < dataAsJSON.Charts.length; chart++) {
			if (PlotJax.PLOTMASK[dataAsJSON.Charts[chart].PlotKind] == null) {
				alert("Unrecognized PlotKind " + dataAsJSON.Charts[chart].PlotKind);
				continue;
			}
			this.plottypes |= PlotJax.PLOTMASK[dataAsJSON.Charts[chart].PlotKind];
			if ((PlotJax.NEEDSY[dataAsJSON.Charts[chart].PlotKind] != null) &&
				PlotJax.NEEDSY[dataAsJSON.Charts[chart].PlotKind]) {
				needsY = true;
			}
			if ((PlotJax.NEEDS_SYMDOMAIN[dataAsJSON.Charts[chart].PlotKind] != null) &&
				PlotJax.NEEDS_SYMDOMAIN[dataAsJSON.Charts[chart].PlotKind]) {
				needs_symdomain = true;
			}
		}

		if (dataAsJSON.X.Label == null) { dataAsJSON.X.Label = 'X Axis'; }
		if (dataAsJSON.X.KeepOrigin == null) { dataAsJSON.X.KeepOrigin = false; }
		this.xType = this.getType(dataAsJSON.X, needs_symdomain);
		if (this.xType == null) { return; }
		var limit_min_x = this.xType.getMinLimit();
		var limit_max_x = this.xType.getMaxLimit();

		if (this.xType instanceof PlotJax.SymbolType) {
/*
 *	do this once here to minimize IE instanceof memory leak
 */
			this.isSymDomain = true;
/*
 *	install all domain values to establish ordering for discrete temporal domain
 *	this simplifies later data point normalization
 */
			for (chart = 0; chart < dataAsJSON.Charts.length; chart++) {
				for (i = 0; i < dataAsJSON.Charts[chart].Data.length; i++) {
					this.xType.normalize(dataAsJSON.Charts[chart].Data[i][0]);
				}
			}
			this.xType.sortDomain();
		}

		var limit_min_y = null;
		var limit_max_y = null;
		var limit_min_y2 = null;
		var limit_max_y2 = null;
		if (needsY) {
			if (dataAsJSON.Y == null) {
				alert("No Y datatype.");
				return;
			}
			if (dataAsJSON.Y.Label == null) { dataAsJSON.Y.Label = 'Y Axis'; }
			if (dataAsJSON.Y.KeepOrigin == null) { dataAsJSON.Y.KeepOrigin = false; }
			if (dataAsJSON.Y.Mapping == null) { dataAsJSON.Y.Mapping = 'normal'; }

			this.yType = this.getType(dataAsJSON.Y);
			if (this.yType == null) { return; }
			limit_min_y = this.yType.getMinLimit();
			limit_max_y = this.yType.getMaxLimit();

			if (dataAsJSON.Y2 != null) {
				this.y2Type = this.getType(dataAsJSON.Y2);
				if (this.y2Type == null) { return; }
				limit_min_y2 = this.y2Type.getMinLimit();
				limit_max_y2 = this.y2Type.getMaxLimit();
			}
		}
		var limits = [limit_min_x, limit_max_x, limit_min_y, limit_max_y, limit_min_y2, limit_max_y2];
/*
 *	verify compatibility of multicharts
 */
		for (chart = 0; chart < dataAsJSON.Charts.length - 1; chart++) {
			if (PlotJax.COMPATIBLE[dataAsJSON.Charts[chart].PlotKind] == null) {
				alert("Unknown PlotKind " + dataAsJSON.Charts[chart].PlotKind);
				return;
			}
			for (var nextchart = chart + 1; nextchart < dataAsJSON.Charts.length; nextchart++) {
				if (!PlotJax.COMPATIBLE[dataAsJSON.Charts[chart].PlotKind][dataAsJSON.Charts[nextchart].PlotKind]) {
					alert(dataAsJSON.Charts[nextchart].PlotKind + " chart incompatible with " + 
						dataAsJSON.Charts[chart].PlotKind + " chart");
					return;
				}
			}
		}
		this.needs_axis = PlotJax.NEED_AXIS[dataAsJSON.Charts[0].PlotKind];
		for (chart = 0; chart < dataAsJSON.Charts.length; chart++) {
			switch (dataAsJSON.Charts[chart].PlotKind) {
    			case 'bubble':
    				this.charts.push(new PlotJax.BubbleChart(dataAsJSON.Charts[chart], this, limits, this.shapes));
    				break;
    			
    			case 'line':
    				this.charts.push(new PlotJax.LineGraph(dataAsJSON.Charts[chart], this, limits, this.shapes));
    				break;
    			
    			case 'point':
    			case 'scatter':
    				this.charts.push(new PlotJax.PointGraph(dataAsJSON.Charts[chart], this, limits, this.shapes));
    				break;
    			
    			case 'area':
    				this.charts.push(new PlotJax.AreaGraph(dataAsJSON.Charts[chart], this, limits, this.shapes));
    				break;
    			
    			case 'vbar':
    			case 'cvbar':
    				this.charts.push(new PlotJax.BarChart(dataAsJSON.Charts[chart], this, limits, this.shapes));
    				break;
    			
    			case 'hbar':
    			case 'chbar':
    				this.charts.push(new PlotJax.HorizBarChart(dataAsJSON.Charts[chart], this, limits, this.shapes));
    				break;
    			
    			case 'pie':
    			case 'segpie':
    				this.charts.push(new PlotJax.PieChart(dataAsJSON.Charts[chart], this, limits));
    				break;
    			
    			case 'box':
    				this.charts.push(new PlotJax.BoxChart(dataAsJSON.Charts[chart], this, limits));
    				break;
    			
    			case 'candle':
    				this.charts.push(new PlotJax.CandleChart(dataAsJSON.Charts[chart], this, limits));
    				break;
    			
    			case 'quadtree':
    			case 'treemap':
    				this.charts.push(new PlotJax.Treemap(dataAsJSON.Charts[chart], this, limits));
    				break;
    			
    			case 'pareto':
    				this.charts.push(new PlotJax.ParetoChart(dataAsJSON.Charts[chart], this, limits));
    				break;
    			
    			case 'gantt':
    				this.charts.push(new PlotJax.GanttChart(dataAsJSON.Charts[chart], this, limits));
    				break;
    			
    			case 'linefit':
    				this.charts.push(new PlotJax.LineFit(dataAsJSON.Charts[chart], this, limits, this.shapes));
    				break;
    			
    			case 'polyfit':
    				this.charts.push(new PlotJax.PolyFit(dataAsJSON.Charts[chart], this, limits, this.shapes));
    				break;
    			
    			case 'expfit':
    				this.charts.push(new PlotJax.ExpFit(dataAsJSON.Charts[chart], this, limits, this.shapes));
    				break;
    			
    			case 'bullet':
    				this.charts.push(new PlotJax.BulletChart(dataAsJSON.Charts[chart], this, limits));
    				break;
    			
    			case 'gauge':
    			case 'halfgauge':
    			case 'vstripgauge':
    			case 'hstripgauge':
    				this.charts.push(new PlotJax.Gauge(dataAsJSON.Charts[chart], this, limits));
    				break;
			}
		}
/*
 *	now scale and draw everything
 */
		this.redraw(true);
		if ((this.helper != null) && (this.helper.onload != null)) { this.helper.onload(); }
	},


// compute pixel coordinates from datapoint
	pt2pxl : function(x, y, z) {
		var plottype = (this.plottypes & (PlotJax.PLOT_HBAR|PlotJax.PLOT_GANTT));
		var tx = this.xType.pt2pxl(x);
		var ty = this.yType.pt2pxl(y);
		var tz = (z == null) ? null : this.zType.pt2pxl(z);
		return (z == null) ? 
			(plottype == 0) ? 
				[ this.leftmargin + tx, this.vertEdge - ty ] : 
				[ this.leftmargin + ty, this.vertEdge - tx ] :
			(plottype == 0) ?  //	translate x,y,z into x,y 
				[ this.leftmargin + tx + Math.round(tz * 0.433),
					this.vertEdge - ty - Math.round(tz * 0.25) ] : 
				[ this.leftmargin + ty + Math.round(tz * 0.433),
					this.vertEdge - tx - Math.round(tz * 0.25) ];
	},

/*
 *	position zoom canvas over chart area
 */
	positionZooms : function() {
/*
 *	now place the gesture div over the chart area
 */
	 	this.zoommap.style.width = this.chartw + "px";
	 	this.zoommap.style.height = this.charth + "px";
	 	this.zoommap.style.left = this.leftmargin;
	 	this.zoommap.style.top = this.topmargin;
	 	if (this.gesture != null) {
	 		this.gesture.setBBox(this.leftmargin, this.topmargin, 
	 			this.leftmargin + this.chartw, this.topmargin + this.charth);
	 	}
	 	return this;
	},

/*
 *	draw warning string if no data to plot
 */
	redraw_nodata : function(sticky) {
		if (sticky) { this.computeMargins(); }
		this.decorate(sticky);
/*
 *	draw warning: compute center pt to draw above
 */
		var midy = (this.botmargin + this.topmargin) >> 1;
		var midx = (this.leftmargin + this.rtmargin) >> 1;
		midy += (this.titleFont.getHeight("No data\navailable.", 4) >> 1);
		this.titleFont.drawAbove("No data\navailable.", midx, midy, 0, 4);
		return this.positionZooms();
	},

/*
 *	apply common decorations (title, signature, legend, etc.)
 */
	decorate : function(sticky) {
/*
 *	clear existing content
 */
		this.chartctx.clearRect(0, 0, this.origwidth, this.origwidth);
/*
 *	draw chart shadow (if any)
 */
		var baroff = this.origheight - 3;
		if (this.shadow) {
			if (this.border == null) {
				this.border = [ "black", 1 ];
			}
			this.chartctx.fillStyle = PlotJax.SHADOW;
			this.chartctx.fillRect(PlotJax.SHADOW_OFFSET, PlotJax.SHADOW_OFFSET, 
				this.origwidth - PlotJax.SHADOW_OFFSET - 1, 
				this.origheight - PlotJax.SHADOW_OFFSET - 1);
			baroff -= (PlotJax.SHADOW_OFFSET + this.border[1]);
			if (this.buttonbar != null) {
				this.buttonbar.move(this.origwidth - PlotJax.SHADOW_OFFSET - 1, baroff);
			}
		}
		else {
			if (this.border != null) {
				baroff -= this.border[1];
			}
			if (this.buttonbar != null) {
				this.buttonbar.move(this.origwidth, baroff);
			}
		}
	
		this.chartctx.fillStyle = this.fillColor;
		this.chartctx.fillRect(0, 0, this.width, this.height);
/*
 *	fill background
 */
		var i;
		if (this.bgGradient != null) {
			var stops = [];
			var ybot = this.height - this.botmargin;
			var lingrad = (this.bgGradient) ? 
				this.chartctx.createLinearGradient(0, this.topmargin + this.charth, 0, this.topmargin) :
				this.chartctx.createLinearGradient(this.leftmargin, this.charth, this.leftmargin + this.chartw, this.charth);

			if (this.bands instanceof Array) {
				for (i = 0; i < this.bands.length; i += 2) {
					stops.push(this.bands[i], this.bands[i+1]);
				}
			}
			else {
				stops.push(0.5, this.bands);
			}
			for (i = 0; i < stops.length; i += 2) {
				lingrad.addColorStop(stops[i], stops[i+1]);
			}
			this.chartctx.fillStyle = lingrad;
		}
		else {
			this.chartctx.fillStyle = this.bgColor;
		}
		this.chartctx.fillRect(this.leftmargin, this.topmargin, this.chartw, this.charth);

		if (this.border != null) {
/*
 *	draw chart border
 */
			this.chartctx.strokeStyle = this.border[0];
			this.chartctx.lineWidth = this.border[1];
			this.chartctx.beginPath();
			this.chartctx.rect(1, 1, this.width - this.border[1], this.height - this.border[1]);
			this.chartctx.stroke();
		}
/*
 * add any decorations
 */
 		if (this.buttonbar != null)
 		{
 			baroff -= this.buttonbar.getHeight();
 		}
		var titlepos = baroff - 10;
		if (this.signature != null)
		{
			this.signatureFont.drawString(this.signature, this.width - 5 - this.signatureFont.getWidth(this.signature),
				baroff - 4 - this.signatureFont.getHeight());
			titlepos -= this.signatureFont.getHeight();
		}
		if (this.title != null) 
		{
			var spos = (typeof this.titlePos == "string") 
				? (this.width - this.titleFont.getWidth(this.title)) >> 1 
				: this.titlePos[0];
			titlepos = (typeof this.titlePos != "string")
				? this.titlePos[1]
				: (this.titlePos == "bottom") 
					? titlepos - this.titleFont.getHeight(this.title) 
					: 10;
			this.titleFont.drawString(this.title, spos, titlepos, 3);
		}
		if (this.legend != null) { this.drawLegend(); }
		if (this.logo != null) { this.drawLogo(); }
		return this;
	},

/*
 *	(re)draw chart content for non-axial charts
 */
	redraw_noaxis : function(sticky) {
/*
 *	prepare contents
 */
		var i, pt_count = 0;
		for (i = 0; i < this.charts.length; i++) {
			if (this.charts[i] != null) {
				pt_count += this.charts[i].layout(sticky);
			}
		}
/*
 *	first time thru, compute margins and decoration placement
 *	any succeding redraw will always use same margins/placement
 *	to avoid chart "jitter"
 */
		if (sticky) { this.computeMargins(); }
		this.decorate(sticky);
/*
 *	draw contents
 */
		for (i = 0; i < this.charts.length; i++) {
			if (this.charts[i] != null) {
				this.charts[i].redraw(sticky);
			}
		}
		return this.positionZooms();
	},

/*
 *	(re)draw the chart content
 */
	redraw : function(sticky) {
		if (!this.needs_axis) { return this.redraw_noaxis(sticky); }
/*
 *	determine bounds of each dimension
 *	Note that Z axis doubles as Y2 axis
 */
		var limits = [
			Number.MAX_VALUE, // min_x
			Number.MIN_VALUE, // max_x
			Number.MAX_VALUE, // min_y
			Number.MIN_VALUE, // max_y
			Number.MAX_VALUE, // min_z
			Number.MIN_VALUE // max_z
		];
/*
 *	prepare contents
 */
		var i;
		var pt_count = 0;
		this.bars_per_domain = 0;
		for (i = 0; i < this.charts.length; i++) {
			if (this.charts[i] != null) {
				pt_count += this.charts[i].layout(sticky, limits);
				if (this.charts[i].getBarsPerDomain != null) {
					this.bars_per_domain += this.charts[i].getBarsPerDomain();
				}
			}
		}
		if ((pt_count == 0) && (
			(this.xType.getMinLimit() == null) ||
			(this.xType.getMaxLimit() == null) ||
			((this.yType != null) &&
			(this.yType.getMinLimit() == null) ||
			(this.yType.getMaxLimit() == null))
			)) {
			return this.redraw_nodata(sticky);
		}

	 	var min_x = limits[0];
	 	var max_x = limits[1];
	 	var min_y = limits[2];
	 	var max_y = limits[3];
	 	var min_z = limits[4];
	 	var max_z = limits[5];
/*
 *	limits do *not* apply on a zoom; note that we threw out any original
 *	data points outside the limits
 */
		if (sticky) {
			var limit_min_x = this.xType.getMinLimit();
			var limit_max_x = this.xType.getMaxLimit();
			if (limit_min_x != null) { min_x = limit_min_x; }
			if (limit_max_x != null) { max_x = limit_max_x; }
			var limit_min_y = (this.yType != null) ? this.yType.getMinLimit() : null;
			var limit_max_y = (this.yType != null) ? this.yType.getMaxLimit() : null;
			var limit_min_z = (this.y2Type != null) ? this.y2Type.getMinLimit() : null;
			var limit_max_z = (this.y2Type != null) ? this.y2Type.getMaxLimit() : null;
			if (limit_min_y != null) { min_y = limit_min_y; }
			if (limit_max_y != null) { max_y = limit_max_y; }
			if (limit_min_z != null) { min_z = limit_min_z; }
			if (limit_max_z != null) { max_z = limit_max_z; }
		}
/*
 *	compute tick labels, then compute bbox to draw them
 */
		var xticks = this.xType.setInterval(min_x, max_x, sticky);
		var yticks = (this.yType != null) ? this.yType.setInterval(min_y, max_y, sticky) : [];
		var zticks = (this.zType != null) ? this.zType.setInterval(min_z, max_z, sticky) :
			(this.y2Type != null) ? this.y2Type.setInterval(min_z, max_z, sticky) :
			[];
/*
 *	first time thru, compute margins and decoration placement
 *	any succeding redraw will always use same margins/placement
 *	to avoid chart "jitter"
 */
		if (sticky) { this.computeMargins(xticks, yticks, zticks); }
		this.computeScales(sticky, xticks, yticks, zticks);
		this.decorate(sticky);

		if ((this.bands != null) && (this.bgGradient == null)) { this.drawBands(yticks); }
/*
 *	draw grid if its layered under
 */
		if ((this.gridDirection != null) && (!this.gridOver)) {
			if ((this.gridDirection == 'vertical') || (this.gridDirection == 'both')) {
				this.drawVerticalGrid(xticks, yticks);
			}
			if ((this.gridDirection == 'horizontal') || (this.gridDirection == 'both')) {
				this.drawHorizontalGrid(xticks, yticks);
			}
		}

		this.drawBottomAxis(xticks, yticks);
		if (this.yType != null) { this.drawLeftAxis(xticks, yticks); }
		if (this.y2Type != null) { this.drawRightAxis(xticks, zticks); }
/*
 *	set the clipping path to constrain the chart area
 *	!!!NOTE: need to use slightly different values for 3-Ds
 */
		if (this.yType != null) {
			this.chartctx.save();
			this.chartctx.beginPath();
			this.chartctx.rect(this.leftmargin, this.topmargin, this.chartw, this.charth);
			this.chartctx.clip();
		}
/*
 *	draw contents
 */
		for (i = 0; i < this.charts.length; i++) {
			if (this.charts[i] != null) {
				this.charts[i].redraw(sticky);
			}
		}
/*
 *	reset clipping region
 */
		if (this.yType != null) { this.chartctx.restore(); }
/*
 * draw grid last if overlaid
 */
		if ((this.gridDirection != null) && (this.gridOver)) {
			if ((this.gridDirection == 'vertical') || (this.gridDirection == 'both')) {
				this.drawVerticalGrid(xticks, yticks);
			}
			if ((this.gridDirection == 'horizontal') || (this.gridDirection == 'both')) {
				this.drawHorizontalGrid(xticks, yticks);
			}
		}
		return this.positionZooms();
	},

	appendChild : function(element) { this.chartdiv.appendChild(element); },
	useShadow : function() { return ((this.shadow != null) && this.shadow); },
	hide : function(chart, elemid) { 
		this.gesture.hide(chart, elemid);
		this.buttonbar.update();
	},

	getUndos : function() { return this.charts[0].getUndos(); },

	applyUndos : function(undos) {
		for (var i = 0; i < undos.length; i++) {
			this.charts[0].hideElements(undos[i]);
		}
		this.redraw();
	},

	openHelp : function() { 
/*
 *	open a balloon centered over the chart div
 *	describing the gesture system
 *	-- probably need to supply a helper hook here...
 *	!!!and may need a path to the pljxhelp.html file
 */
 		if (this.balloon == null) { return false; }
		var cnvscoords = this.getCanvasCoords();
		cnvscoords[0] += (this.width >> 1);
		cnvscoords[1] += (this.height >> 1);
		if ((this.helper != null) && (this.helper.getHelpContent != null)) {
 			this.balloon.centerBalloon().open(cnvscoords[0], cnvscoords[1], this.helper.getHelpContent());
		}
		else {
 			this.balloon.centerBalloon().open(cnvscoords[0], cnvscoords[1], "url:../html/pljxhelp.html");
		}
		return false;
	}
};

/**
 *	Button bar.
 */
PlotJax.ButtonBar = function(plot, chartid, gestures, top) {
	this.plot = plot;
	this.gestures = gestures;

	this.menudiv = document.createElement("div");
    this.menudiv.setAttribute("id", chartid + "_menu");
	this.menudiv.style.position = "absolute";
	this.menudiv.style.left = 0;
	this.menudiv.style.bottom = top;
	this.menudiv.style.width = "100%";
	this.menudiv.style.zIndex = 1;
	this.menudiv.style.visibility = "visible";
    plot.appendChild(this.menudiv);
/*
 *	maybe add tooltips to these buttons ?
 */
    this.menudiv.innerHTML = (gestures != null) ?
    	"<table align=center border=0 cellspacing=0 cellpadding=0><tr>" +
			"<td><button id='" + chartid + "_zoombtn' style='background: white; border: none; width: 20px; height: 20px;' onclick='this.plot.onZoom();'><b><img src='" + plot.adjustImagePath("../imgs/viewmagplus16.gif") + "' alt='Zoom' title='Zoom'></b></button></td>\n" +
			"<td><button id='" + chartid + "_undobtn' style='background: white; border: none; width: 20px; height: 20px;' onclick='this.plot.onUndo();'><b><img src='" + plot.adjustImagePath("../imgs/actundo16.gif") + "' alt='Undo' title='Undo'></b></button></td>\n" +
			"<td><button id='" + chartid + "_redobtn' style='background: white; border: none; width: 20px; height: 20px;' onclick='this.plot.onRedo();'><b><img src='" + plot.adjustImagePath("../imgs/actredo16.gif") + "' alt='Redo' title='Redo'></b></button></td>\n" +
			"<td><button id='" + chartid + "_clearbtn' style='background: white; border: none; width: 22px; height: 22px;' onclick='this.plot.onClear();'><b><img src='" + plot.adjustImagePath("../imgs/tinybroom.gif") + "' alt='Clear' title='Clear'></b></button></td>\n" +
			"<td><button id='" + chartid + "_helpbtn' style='background: white; border: none; width: 20px; height: 20px;' onclick='return this.plot.openHelp()'><b><img src='" + plot.adjustImagePath("../imgs/acthelp16.gif") + "' alt='Help' title='Help'></b></button></td></tr></table>" :
    	"<table align=right border=0 cellspacing=0 cellpadding=0><tr>" +
			"<td><button id='" + chartid + "_helpbtn' style='background: white; border: none; width: 20px; height: 20px;' onclick='return this.plot.openHelp()'><b><img src='" + plot.adjustImagePath("../imgs/acthelp16.gif") + "' alt='Help' title='Help'></b></button></td></tr></table>";

	if (gestures != null) {
		this.zoombtn = document.getElementById(chartid + "_zoombtn");
		this.zoombtn.disabled = true;
		this.zoombtn.plot = plot;

		this.undobtn = document.getElementById(chartid + "_undobtn");
		this.undobtn.disabled = true;
		this.undobtn.plot = plot;

		this.redobtn = document.getElementById(chartid + "_redobtn");
		this.redobtn.disabled = true;
		this.redobtn.plot = plot;

		this.clearbtn = document.getElementById(chartid + "_clearbtn");
		this.clearbtn.disabled = true;
		this.clearbtn.plot = plot;
	}
	this.helpbtn = document.getElementById(chartid + "_helpbtn");
	this.helpbtn.disabled = false;
	this.helpbtn.plot = plot;
	
	this.ht = this.menudiv.offsetHeight;
	
	this.update = function() {
	//
	//	only gets called if there are gestures
	//
		this.undobtn.disabled = (!(this.gestures.hasUndos()));
		this.redobtn.disabled = (!(this.gestures.hasRedos()));
		this.zoombtn.disabled = (!(this.gestures.hasZooms()));
		this.clearbtn.disabled = (!(this.gestures.hasZooms()));
	};
	
	this.getHeight = function() { return this.ht; };

	this.move = function(w, bot) {
		this.menudiv.style.width = ((this.gestures != null) ? w : w - 16) + "px";
		this.menudiv.style.top = bot - this.ht; 
	};
};

//
//	don't really like polluting the namespace, but we'll live
//
Date.prototype.pljxSetTimeInSecs = function(secs) {
	this.setTime(Math.round(secs * 1000));
};

/*******************************************************
 *
 *	Datatype classes
 *
 *	Each Datatype classe provides the following methods:
 *
 *	constructor: takes a PlotJax typespec object,
 *		as defined in "PlotJax Chart Data JSON Format"
 *
 *	getMinLimit() - returns any minimum limit value specified
 *		in the typespec
 *
 *	getMaxLimit() returns any maximum limit value specified
 *		in the typespec
 *	
 *	needOrigin() returns the typespec KeepOrigin value (if any);
 *		returns false if KeepOrigin was not specified, or
 *		if the datatype does not support the concept of an origin
 *	
 *	displayLength() returns the maximum character length of a 
 *		data item rendered using the current display format
 *
 *  display(val) returns the formatted version of the normalized
 *		value using the current display format
 *
 *	normalize(val) returns a normalized (numeric) value for
 *		the input value, which may be a symbol (for SymbolType)
 *		or a date/time encoded string
 *
 *	isLog10() returns the typespec Log10 value (if any), or false
 *		if none was specified, or hte type does not support logarithmic
 *		axes
 *
 *	getTemplate() returns a template string used to measure a default
 *		expected maximum character length for values returned from
 *		the display() method; used for establishing a minimum margin
 *		for tick labels
 *
 *	getTicks() returns an array of (tick label, tick value) pairs;
 *		tick value is the normalized equivalent of tick label, which
 *		can be used later to compute the pixel location for the label
 *		Note that the returned pairs are NOT arrays, but individual
 *		elements in the returned array (i.e., even indexes are labels,
 *		odd indexes are normalized values)
 *
 *	updateTicks(delta, ticks) adds leading and trailing tick values
 *		in order to adjust for padding (usually for bubblecharts)
 *		Returns the input array of (tick label, tick value) pairs with
 *		the added values.
 *
 *	setInterval(minval, maxval, sticky) returns the tick label/value
 *		array of getTicks() after computing an optimal tick interval
 *		and precision based in the input (minval, maxval) range. The
 *		sticky parameter indicates if this scaling is for the initial
 *		chart rendering (true), or subsequent zoom operations (false).
 *		In the sticky case, the tick format uses any format specified
 *		in the original typespec; otherwise, the computed format will
 *		be used.
 *
 *	getLimits() returns the current interval/precision adjusted minimum
 *		and maximum values for the axis
 *
 *	setScale(pixels[, padding, ticks]) computes and stores the per-pixel scaling factor 
 *		for the associated axis, using the supplied size of the axis
 *		in pixels
 *
 *	pt2pxl(pt, offset) computes and returns the absolute pixel position 
 *		for the specified normalized pt value, using the scaling previously
 *		established via setScale(), and the specified offset value.
 *
 **********************************************************************/

PlotJax.BaseType = function(typespec) {
	this.minLimit = this.maxLimit = this.minval = this.maxval = this.scale = null;
	this.keepOrigin = this.log10 = false;
	this.label = typespec.Label;
	this.log10 = (typespec.Log10 != null) ? typespec.Log10 : false;
	this.tickFormat = typespec.TickFormat;
};

PlotJax.BaseType.MONTHS = {	"JAN":0, "FEB":1, "MAR":2, "APR":3, "MAY":4, "JUN":5, "JUL":6, "AUG":7, "SEP":8, "OCT":9, "NOV":10,"DEC":11 };
PlotJax.BaseType.MONTH = ['JAN','FEB','MAR','APR','MAY','JUN', 'JUL','AUG','SEP','OCT','NOV','DEC'];
PlotJax.BaseType.PRETTY_MONTH = ['Jan','Feb','Mar','Apr','May','Jun', 'Jul','Aug','Sep','Oct','Nov','Dec'];
PlotJax.BaseType.logsteps = [ 
	0, Math.log(2)/Math.log(10), Math.log(3)/Math.log(10), 
	Math.log(4)/Math.log(10), Math.log(5)/Math.log(10), 1.0 
];

PlotJax.BaseType.prototype = {
	getLimits : function() { return [ this.minval, this.maxval ]; },
	getMaxLimit : function() { return this.maxLimit; },
	getMinLimit : function() { return this.minLimit; },
	isLog10 : function() { return this.log10; },
	needOrigin : function() { return this.keepOrigin; },
	pt2pxl : function(pt, offset) {
		return offset + Math.round((pt - this.scalemin)* this.scale); 
	},
	setScale : function(pixels, padding, ticks) { 
//
//	if padding, adjust min/max values; its up to PlotJax to
//	recompute the ticks
//
		if ((padding != null) && (padding != 0)) {
//
//	compute increment per pixel, then adjust bounds values
//	for the padding
//
			this.updateTicks(pixels, padding, ticks);
			this.scalemin = ticks[1];
			this.scalemax = ticks[ticks.length - 1];
		}
		else {
			padding = 0;
			this.scalemin = this.minval;
			this.scalemax = this.maxval;
		}
		this.scale = pixels/(this.scalemax - this.scalemin); 
		return this;
	},

	getLabel : function() { return this.label; },
/*
 *	strftime() adapted from http://whytheluckystiff.net/ruby/strftime.js
 * other support functions -- thanks, ecmanaut!
 */
	zeropad : function( n ){ return n>9 ? n : "0"+n; },

	subsecs : function( t, prec ) {
		if (prec == 0) { return t.getUTCSeconds(); }
		if (prec > 3) { prec = 3; }
		var n = t.getMilliseconds();
		if (n.length < 3) {
			n = (n.length == 1) ? "00" + n : "0" + n;
		}
		return t.getUTCSeconds() + '.' + n.substr(0, prec);
	},

	strftime : function (fmt, dateval) {
	    var t = dateval;
		var twelvehour = (fmt.match(/\%p/i) != null);
		var fmtpieces = fmt.split("%");
		var prefix;
		for (var i = 1; i < fmtpieces.length; i++) {
			switch (fmtpieces[i].substr(0,1)) {
	  			case 'b': 
	  				prefix = PlotJax.BaseType.MONTH[t.getUTCMonth()];
	  				break;
  				
	  			case 'B': 
	  				prefix = PlotJax.BaseType.PRETTY_MONTH[t.getUTCMonth()];
	  				break;
  				
	  			case 'd': 
	  				prefix = this.zeropad(t.getUTCDate());
	  				break;
  				
	  			case 'H': 
	  				prefix = this.zeropad(t.getUTCHours());
	  				break;

	  			case 'i': 
	  				prefix =  (t.getUTCHours() + 12) % 12;
	  				if (prefix == 0) { prefix = 12; }
	  				break;

	  			case 'I': 
	  				prefix = this.zeropad((t.getUTCHours() + 12) % 12); 
	  				if (prefix == "00") { prefix = 12; }
	  				break;

	  			case 'm': 
	  				prefix = this.zeropad(t.getUTCMonth()+1);
	  				break;

	  			case 'M': 
	  				prefix = this.zeropad(t.getUTCMinutes());
	  				break;

	  			case 'p': 
	  				prefix = (t.getUTCHours() < 12) ? 'AM' : 'PM';
	  				break;

	  			case 'P': 
	  				prefix = (t.getUTCHours() < 12) ? 'A' : 'P';
	  				break;

	  			case 'S': 
	  				prefix = this.zeropad(t.getUTCSeconds());
	  				break;

	  			case 'T': 
	  				prefix = this.zeropad(t.getUTCHours()) + ":" + 
	  					this.zeropad(t.getUTCMinutes()) + ":" + 
	  					this.zeropad(t.getUTCSeconds());
	  				break;

	  			case 'y': 
	  				prefix = this.zeropad(this.Y(t) % 100);
	  				break;

	  			case 'Y': 
	  				prefix = t.getUTCFullYear();
	  				break;

	  			case '.':
				    fmtpieces[i] = fmtpieces[i].replace(/^\.(\d)S/, this.subsecs(t, RegExp.$1));
				    prefix = "";
				    break;

				default: 
				    prefix = "";
					break;
			}
			if (prefix != "") {
				fmtpieces[i] =  "" + prefix + fmtpieces[i].substr(1);
			}
		}
	    return fmtpieces.join("");
	},

//
//	base method does datetime version
//
	displayLength : function () {
		var fmt = this.displayFormat;
		var len = fmt.length;
		var specs = fmt.match(/\%([bcdHImMpSTyY])/g);
		for (var spec in specs) {
			switch (spec) {
				case 'b': 
				case 'B':
					len += 1;
					break;

				case 'T':
					len += 6;
					break;

				case 'Y':
					len += 2;
					break;
			}
		}
/*
 *	capture seconds w/ subsec precision
 */
		specs = fmt.match(/%\.(\d)S/g);
		for (var i = 0; i < specs.length; i++) {
			len += 
				((specs[i] == 0) ? -2	// no subsecs
				:(specs[i] >= 3) ? 2	// max is millisecs
				:(specs[i] == 2) ? 1
				: 0);
		}
		return len;
	},

/* Function printf(format_string,arguments...)
 * Javascript emulation of the C printf function (modifiers and argument types
 *    "p" and "n" are not supported due to language restrictions)
 *
 * Copyright 2003 K&L Productions. All rights reserved
 * http://www.klproductions.com
 *
 * Terms of use: This function can be used free of charge IF this header is not
 *               modified and remains with the function code.
 *
 * Legal: Use this code at your own risk. K&L Productions assumes NO resposibility
 *        for anything.
 *
 *	NOTE: modified (trimmed) for use with PlotJax
 *
 */
	printf : function(fstring) {
		var pad = function(str,ch,len) {
			var ps='';
 			for(var i=0; i<Math.abs(len); i++) { ps += ch; }
 		       return len>0 ? str+ps : ps+str;
		};
 		var processFlags = function(flags,width,rs,arg) {
 		   	var pn = function(flags,arg,rs) {
				if(arg>=0) {
					if(flags.indexOf(' ')>=0) { rs = ' ' + rs; }
                	else if(flags.indexOf('+')>=0) { rs = '+' + rs; }
				}
            	else { rs = '-' + rs; }
            	return rs;
			};
        	var iWidth = parseInt(width,10);
        	if(width.charAt(0) == '0') {
        		var ec=0;
        	    if(flags.indexOf(' ')>=0 || flags.indexOf('+')>=0) { ec++; }
	            if(rs.length<(iWidth-ec)) {
	            	rs = pad(rs,'0',rs.length-(iWidth-ec));
	            }
	            return pn(flags,arg,rs);
			}
	        rs = pn(flags,arg,rs);
	        return (rs.length<iWidth) ? 
	        	(flags.indexOf('-')<0) ? 
	        		pad(rs,' ',rs.length-iWidth) :
	        		pad(rs,' ',iWidth - rs.length) : rs;
		};
	    var converters = [];
	    converters.d = function(flags,width,precision,arg) {
	    	var iPrecision = parseInt(precision, 10);
	        var rs = ((Math.abs(arg)).toString().split('.'))[0];
	        if(rs.length<iPrecision) {
	        	rs = pad(rs,' ',iPrecision - rs.length);
	        }
	        return processFlags(flags,width,rs,arg);
	    };
	    converters.e =  function(flags,width,precision,arg) {
	    	var iPrecision = parseInt(precision, 10);
	        if(isNaN(iPrecision)) { iPrecision = 6; }
	        var rs = (Math.abs(arg)).toExponential(iPrecision);
	        if(rs.indexOf('.')<0 && flags.indexOf('#')>=0) {
	        	rs = rs.replace(/^(.*)(e.*)$/,'$1.$2');
	        }
	        return processFlags(flags,width,rs,arg);
	    };
	    converters.f = function(flags,width,precision,arg) {
	    	var iPrecision = parseInt(precision, 10);
	        if(isNaN(iPrecision)) { iPrecision = 6; }
	        var rs = (Math.abs(arg)).toFixed(iPrecision);
	        if(rs.indexOf('.')<0 && flags.indexOf('#')>=0) { rs = rs + '.'; }
	        return processFlags(flags,width,rs,arg);
	    };
	    converters.s = function(flags,width,precision,arg) {
	    	var iPrecision=parseInt(precision, 10);
	        var rs = arg;
	        if(rs.length > iPrecision) {
	        	rs = rs.substring(0,iPrecision);
	        }
	        return processFlags(flags,width,rs,0);
	    };
	    var farr = fstring.split('%');
	    var retstr = farr[0];
	    var fpRE = /^([\-+ #]*)(\d*)\.?(\d*)([cdieEfFgGosuxX])(.*)$/;
	    for(var i=1; i<farr.length; i++) {
	    	var fps=fpRE.exec(farr[i]);
	        if(!fps) { continue; }
	        if (arguments[i] != null) {
	        	retstr += converters[fps[4]](fps[1],fps[2],fps[3],arguments[i]);
	        }
	        retstr += fps[5];
	    }
	    return retstr;
	},

	strptime : function(val) {
		var year = 0;
		var month = 0;
		var day = 0;
		val = val.toUpperCase();
		var fmt = this.displayFormat;
		var parts = fmt.split("%");
		var j = (parts[0] != null) ? parts[0].length : 0;
		var i;
		for (i = 1, j = 0; i < parts.length; i++) {
			switch (parts[i].substr(0,1)) {
				case 'Y':
					if (val.length - j < 4) { return null; }
					year = parseInt(val.substr(j,4), 10);
					if (year == null) { return null; }
					j += parts[i].length + 3;
					break;

				case 'y':
					if (val.length - j < 2) { return null; }
					year = parseInt(val.substr(j,2), 10);
					if (year == null) { return null; }
					year += 2000;
					j += parts[i].length + 1;
					break;

				case 'm':
					if (val.substr(j).match(/(\d{1,2})/) == null) { return null; }
					month = parseInt(RegExp.$1, 10) - 1;
					if ((month < 0) || (month > 11)) { return null; }
					j += parts[i].length + RegExp.$1.length - 1;
					break;

				case 'b':
					if (((val.length - j) < 3) || 
						(PlotJax.BaseType.MONTHS[val.substr(j,3)] == null)) {
						return null;
					}
					month = PlotJax.BaseType.MONTHS[val.substr(j,3)];
					j += parts[i].length + 2;
					break;

				case 'd':
					if (val.substr(j).match(/(\d{1,2})/) == null) { return null; }
					day = parseInt(RegExp.$1, 10);
					if ((day < 1) || (day > 31)) { return null; }
					j += parts[i].length + RegExp.$1.length - 1;
					break;
			}
		}
		return new Date(year, month, day);
	}
};

/*******************************************
 *
 *	NOTE: can't compress prototype here, due to the
 *	inheritance mechanism
 ******************************************/
PlotJax.TimeType = function(typespec) {
	PlotJax.TimeType.baseConstructor.call(this, typespec);
	this.displayFormat = (typespec.Format == null) ? '%H:%M:%.3S' : typespec.Format;
	this.twelveHourTick = (this.tickFormat != null) ? this.tickFormat.match(/%p/i) : false;
	this.twelveHourValue = this.displayFormat.match(/%p/i);
	this.regex = new RegExp(/^(\d{1,2})(?:\:(\d{2})(?:\:(\d{2}(?:\.\d+)))?)?([aApP][Mm]?)?$/);

	this.minLimit = (typespec.Min != null) ? this.normalize(typespec.Min) : null;
	this.maxLimit = (typespec.Max != null) ? this.normalize(typespec.Max) : null;
	if ((this.minLimit != null) && (this.maxLimit != null) &&
		(this.maxLimit <= this.minLimit)) {
		alert("TimeType: Invalid limits: max less than min.");
		return null;
	}
	if (this.keepOrigin) {
		if ((this.minLimit == null) || (this.minLimit > 0)) {
			this.minLimit = 0;
		}
	}
};

JSPPTrustee.extend(PlotJax.TimeType, PlotJax.BaseType);

PlotJax.TimeType.prototype.display = function(val) {
	var tdate = new Date();
	tdate.pljxSetTimeInSecs(val);
	return this.strftime(this.displayFormat, tdate);
};

PlotJax.TimeType.prototype.getTemplate = function() { return 'HH:MM:SS.SSSAM'; };

PlotJax.TimeType.prototype.getTicks = function() {
	var labels = [];
	var tick = this.minval;
	var tdate = new Date();
	while (tick <= this.maxval) {
		tdate.pljxSetTimeInSecs(tick);
		labels.push(this.strftime(this.tickfmt, tdate), tick);
		tick += this.interval;
	}
	return labels;
};

//
//	adjust the ticks based on the number of interval that contains the 
//	specified delta value
//
PlotJax.TimeType.prototype.updateTicks = function(width, padding, ticks) {

	var intvls = Math.ceil((padding * (this.maxval - this.minval)/(width - (padding * 2)))/this.interval);
	var minval = this.minval;
	var maxval = this.maxval;
	var tdate = new Date();
	for (; intvls > 0; intvls--) {
		minval -= this.interval;
		maxval += this.interval;
//		if (minval < 0) 
//			minval += 86400;
		tdate.pljxSetTimeInSecs(minval);
		ticks.unshift(this.strftime(this.tickfmt, tdate), minval);
		tdate.pljxSetTimeInSecs(maxval);
		ticks.push(this.strftime(this.tickfmt, tdate), maxval);
	}
	return ticks;
};

PlotJax.TimeType.prototype.normalize = function(val) {
	var ampm = val.substr(-1).toUpperCase();
	if (ampm == "M") {
		ampm = val.substr(-2, 1).toUpperCase();
	}
	var offset = (ampm == 'P') ? 12 * 3600 : 0;
	var twelvehour = ((ampm == 'A') || (ampm == 'P'));
	val = val.replace(/[aApP][mM]?/, "");
	var parts = val.split(":");
	if ((parts == null) || (parts.length == 0) || (parts.length > 3)) {
		alert("Invalid time value " + val);
		return null;
	}
	switch (parts.length) {
		case 1:
			parts.push(0,0);
			break;
		case 2:
			parts.push(0);
			break;
	}
	var hours = parseInt(parts[0], 10);
	var mins = parseInt(parts[1], 10);
	var secs = parseFloat(parts[2]);
	if (twelvehour && (hours == 12) && (offset == 0)) {
		hours = 0;
	}
	if ((twelvehour && (hours > 11)) ||
		(hours > 23) || (mins > 59) || (secs >= 60)) {
		alert("Invalid time value " + val);
		return null;
	}
	var result = (hours * 3600) + offset + (mins * 60) + secs;
	return result;
};

PlotJax.TimeType.prototype.setInterval = function(minval, maxval, sticky) {
	if (maxval == minval) {
		minval = minval - 3600;
		maxval = maxval + 3600;
	}

	if (this.log10) {
		minval = Math.floor(minval) - (minval < 0 ? 1 : 0);
		maxval = Math.floor(maxval) + 1;
	}
	var intvl = maxval - minval;
	var fmt = this.twelveHourTick ? '%I:%M%p' : '%H:%M';
//
//	intervals are selected based on range:
//	<= 100 sec: acts like NumberType (1, 2, 5, 10)
//	<= 4 min : 20 sec intervals
//	<= 7 min : 30 sec intervals
//	<= 10 min : 1 minute intervals
//	<= 20 min : 2 min intervals
//	<= 60 min : 5 min intervals 
//	<= 2 hour : 10 min intervals 
//	<= 4 hour : 20 min intervals 
//	<= 8 hour : 30 min intervals 
//	<= 12 hour : 1 hour intervals 
//	<= 24 hour : 2 hour intervals 
//	cannot be > 24 hours!
//
	if (intvl <= 100) {
		var xr = Math.log(intvl)/Math.log(10);
		var xd = xr - Math.floor(xr);
		var precision = Math.floor(xr) - 1;
		if (precision < 1) {
			if (xd < 0.15) {	//
				this.interval = 1;
			}
			else if (xd < 0.4) {
				this.interval = 2;
			}
			else if (xd < 0.87) {
				this.interval = 5;
			}
			else {
				this.interval = 1;
				precision = Math.floor(xr);
			}
		}
//
//	NOTE: we currently can't support submillsecond times
//
		this.interval *= Math.pow(10, precision);
		fmt = this.twelveHourTick ?
			(precision < 0) ?
				'%I:%M:%.' + Math.abs(precision) + 'S%p' :
				'%I:%M:%S%p' :
			(precision < 0) ? 
				'%H:%M:%.' + Math.abs(precision) + 'S' :
				'%T';
	}
	else { 
		if (intvl <= 420) {
			fmt = this.twelveHourTick ? '%I:%M%p' : '%T';
		}
		this.interval = 
		      (intvl <= 240) ? 20 :
		      (intvl <= 420) ? 30 :
		      (intvl <= 600) ? 60 :
		      (intvl <= 1200) ? 120 :
		      (intvl <= 3600) ? 300 :
		      (intvl <= 7200) ? 600 :
		      (intvl <= 14400) ? 1200 :
		      (intvl <= 25200) ? 1800 :
		      (intvl <= 43200) ? 3600 :
		      7200;	// (intvl <= 86400)
	}
	this.minval = this.interval * Math.floor(minval/this.interval);
	this.maxval = this.interval * Math.ceil(maxval/this.interval);
//
//	assume "origin" means 12:00:00AM aka 00:00:00
//	and we can't have a negative max!
//
	if (sticky && this.keepOrigin && (this.minval > 0)) {
		this.minval = 0;
	}
//
//	override tick format if we zoom in
//
//	this.tickfmt = (sticky && (this.tickFormat != null)) ?
	this.tickfmt = (this.tickFormat != null) ? this.tickFormat : fmt;

	return this.getTicks();
};

PlotJax.TimestampType = function(typespec) {
	PlotJax.TimestampType.baseConstructor.call(this, typespec);
	this.re = new RegExp(/^(\d{4})-(\d{1,2})-(\d{1,2})\s+(\d{1,2}):(\d{2}):(\d{2}(?:\.\d+)?)$/);
	this.minLimit = (typespec.Min != null) ? this.normalize(typespec.Min) : null;
	this.maxLimit = (typespec.Max != null) ? this.normalize(typespec.Max) : null;
	if ((this.minLimit != null) && (this.maxLimit != null) &&
		(this.maxLimit <= this.minLimit)) {
		alert("TimestampType: Invalid limits: max " + typespec.Max + " less than min " + typespec.Min);
		return null;
	}
	this.displayFormat = (typespec.Format != null) ? typespec.Format : "%Y-%m-%d %H:%M:%.3S";
	this.keepOrigin = false;
};

JSPPTrustee.extend(PlotJax.TimestampType, PlotJax.BaseType);

PlotJax.TimestampType.prototype.display = function(val) {
	var tdate = new Date();
	tdate.pljxSetTimeInSecs(val);
	return this.strftime(this.displayFormat, tdate);
};

//
//	!!!NEED TO FINISH THIS
//
PlotJax.TimestampType.prototype.getLog10Ticks = function() {
	var labels = [];
/*
	var i = xl;
	var n = 0;
	var k = i;
	while (i < xh) {
		k = i + PlotJax.BaseType.logsteps[n++];

		var ulcoords = this.pt2pxl(k, yl);
		var lrcoords = this.vertGrid ? this.pt2pxl(k, yh) : [ 0,0 ];
//
//	don't draw tick labels if we're overwriting the axis label
//
		if ((n == 1) && (px + tfh < xStart)) {
			this.tickFont.drawBelow(this.display(Math.pow(10, k)), 
				ulcoords[0], ulcoords[1]);
		}
		if (n >= PlotJax.BaseType.logsteps.length) {
			n = 0;
			i = k;
		}
	}
*/
};

PlotJax.TimestampType.prototype.getTemplate = function() { return 'YYYY-MM-DD HH:MM:SS.SSS'; };

PlotJax.TimestampType.prototype.getTicks = function() {
//
//	NEED TO HANDLE LOG10 HERE!!!
//
	var labels = [];
	var tick = this.minval;
	var year = 0;
	var month = 0;
	var tdate = new Date();
	tdate.pljxSetTimeInSecs(tick);
	if (this.useMonths) {
		year = tdate.getUTCFullYear();
		month = tdate.getUTCMonth();
	}
	else if (this.useYears) {
		year = tdate.getUTCFullYear();
	}
//
//	start with min, so we can capture max
//
	labels.push(this.strftime(this.tickfmt, tdate), tdate.getTime()/1000);
	while (tick < this.maxval) {
		if (this.useYears) {
			year += this.interval;
			tdate.setUTCFullYear(year, 0, 1);
		}
		else if (this.useMonths) {
			month += this.interval;
			if (month > 11) {
				year++;
				month = 0;
			}
			tdate.setUTCFullYear(year, month, 1);
		}
		else {
			tick += this.interval;
			tdate.pljxSetTimeInSecs(tick);
		}
		tick = tdate.getTime()/1000;
		labels.push(this.strftime(this.tickfmt, tdate), tick);
	}
	return labels;
};

//
//	adjust the ticks based on the number of interval that contains the 
//	specified delta value
//
PlotJax.TimestampType.prototype.updateTicks = function(width, padding, ticks) {
//
//	NEED TO HANDLE LOG10 HERE!!!
//
//	NEED TO compensate for years and months intervals
//
	var mindate = new Date();
	var minyear = 0;
	var minmonth = 0;

	var maxdate = new Date();
	var maxyear = 0;
	var maxmonth = 0;
	var minval = this.minval;
	var maxval = this.maxval;
	mindate.pljxSetTimeInSecs(this.minval);
	maxdate.pljxSetTimeInSecs(this.maxval);

	var tmin = this.minval;
	var tmax = this.maxval;
	var intvls = 0;
	if (this.useMonths) {
		minyear = mindate.getUTCFullYear();
		minmonth = mindate.getUTCMonth();
		tmin = (minyear * 12) + minmonth;

		maxyear = maxdate.getUTCFullYear();
		maxmonth = maxdate.getUTCMonth();
		tmax = (maxyear * 12) + maxmonth;
	}
	else if (this.useYears) {
		minyear = mindate.getUTCFullYear();
		tmin = minyear;
		maxyear = maxdate.getUTCFullYear();
		tmax = maxyear;
	}
	intvls = Math.ceil((padding * (tmax - tmin)/(width - (padding * 2)))/this.interval);
	for (; intvls > 0; intvls--) {
		if (this.useYears) {
			minyear -= this.interval;
			maxyear += this.interval;
			mindate.setUTCFullYear(minyear, 0, 1);
			maxdate.setUTCFullYear(maxyear, 0, 1);
		}
		else if (this.useMonths) {
			minmonth -= this.interval;
			if (minmonth < 0) {
				minyear--;
				minmonth += 12;	// wrap back
			}
			mindate.setUTCFullYear(minyear, minmonth, 1);

			maxmonth += this.interval;
			if (maxmonth > 11) {
				maxyear++;
				maxmonth = 0;
			}
			maxdate.setUTCFullYear(maxyear, maxmonth, 1);
		}
		else {
			minval -= this.interval;
			mindate.pljxSetTimeInSecs(minval);
			maxval += this.interval;
			maxdate.pljxSetTimeInSecs(maxval);
		}
		ticks.unshift(this.strftime(this.tickfmt, mindate), mindate.getTime()/1000);
		ticks.push(this.strftime(this.tickfmt, maxdate), maxdate.getTime()/1000);
	}
	return ticks;
};

PlotJax.TimestampType.prototype.normalize = function(val) {
	if (this.re.test(val) == null) {
		alert("Invalid timestamp value " + val);
		return null;
	}
	var year = RegExp.$1;
	var month = RegExp.$2;
	var day = RegExp.$3;
	var hour = RegExp.$4;
	var mins = RegExp.$5;
	var secs = RegExp.$6;
	var msecs = RegExp.$7;
	if (msecs == null) {
		msecs = 0;
	}
	else if (msecs > 999) {
		msecs = msecs.substr(0,3);
	}
	if ((month < 1) || (month > 12) || (day < 1) || (day > 31) ||
		(hour > 23) || (mins > 59) || (secs >= 60)) {
		alert("Invalid timestamp value" + val);
		return null;
	}
	var tdate = new Date(year, month - 1, day);
	tdate.setUTCHours(hour, mins, secs, msecs);
	if (tdate == null) {
		alert("Invalid timestamp value " + val);
		return null;
	}
//	alert("normalize: " + val + " to " + (tdate.getTime()/1000));
	return tdate.getTime()/1000;
};

PlotJax.TimestampType.prototype.setInterval = function(minval, maxval, sticky) {
// alert("setInterval: minval " + minval + " maxval " + maxval);
	if (maxval == minval) {
		minval = minval - 86400;
		maxval = maxval + 86400;
	}
	if (this.log10) {
		minval = Math.floor(minval) - (minval < 0 ? 1 : 0);
		maxval = Math.floor(maxval) + 1;
	}
	var intvl = maxval - minval;
	this.useMonths = false;
	this.useYears = false;
	var fmt = '%Y-%m-%d %T';
	var mindate = new Date();
	var maxdate = new Date();
	mindate.pljxSetTimeInSecs(minval);
	maxdate.pljxSetTimeInSecs(maxval);
//
//	intervals are selected based on range:
//	<= 100 sec: acts like NumberType (1, 2, 5, 10)
//	<= 4 min : 20 sec intervals
//	<= 7 min : 30 sec intervals
//	<= 10 min : 1 minute intervals
//	<= 20 min : 2 min intervals
//	<= 60 min : 5 min intervals 
//	<= 2 hour : 10 min intervals 
//	<= 4 hour : 20 min intervals 
//	<= 8 hour : 30 min intervals 
//	<= 12 hour : 1 hour intervals 
//	<= 24 hour : 2 hour intervals 
//	<= 2 days : 4 hour intervals 
//	<= 4 days : 8 hour intervals 
//	<= 7 days : 12 hour intervals 
//	<= 15 days : 1 day intervals 
//	<= 1 month : 2 day intervals 
//	<= 2 months : 5 day intervals
//	<= 3 months : 10 day intervals
//	<= 6 months : 15 day intervals
//	<= 1 Year : 1 month intervals 
//	<= 2 Year : 2 month intervals 
//	<= 4 Year : 4 month intervals 
//	<= 7 Year : 6 month intervals 
//	<= 10 Year : 1 year intervals 
//	> 10 year: treat as NumberType over years range
//
	var xr, xd, precision;
	if (intvl <= 100) {
		xr = Math.log(intvl)/Math.log(10);
		xd = xr - Math.floor(xr);
		precision = Math.floor(xr) - 1;
		if (precision < 1) {
			if (xd < 0.15) {	//
				this.interval = 1;
			}
			else if (xd < 0.4) {
				this.interval = 2;
			}
			else if (xd < 0.87) {
				this.interval = 5;
			}
			else {
				this.interval = 1;
				precision = Math.floor(xr);
			}
		}
//
//	NOTE: we currently can't support submillsecond times
//
		this.interval *= Math.pow(10, precision);
		if (precision < 0) {
			fmt =  '%Y-%m-%d %H:%M:%.' + Math.abs(precision) + 'S';
		}
	}
	else if (maxdate.getUTCFullYear() - mindate.getUTCFullYear() > 10) {
//
//	Year only interval
//
		intvl = maxdate.getUTCFullYear() - mindate.getUTCFullYear() + 1;
		xr = Math.log(intvl)/Math.log(10);
		xd = xr - Math.floor(xr);
		precision = Math.floor(xr) - 1;
		if (xd < 0.15) {
			this.interval = 1;
		}
		else if (xd < 0.4) {
			this.interval = 2;
		}
		else if (xd < 0.87) {
			this.interval = 5;
		}
		else {
			this.interval = 1;
			precision = Math.floor(xr);
		}
		this.interval *= Math.pow(10, precision);
		fmt = '%Y';
		this.useYears = true;
	}
	else { 
//	<= 6 months : 15 day intervals
//	<= 1 Year : 1 month intervals 
//	<= 2 Year : 2 month intervals 
//	<= 4 Year : 4 month intervals 
//	<= 7 Year : 6 month intervals 
//	<= 10 Year : 1 year intervals 
		fmt = 
			(intvl <= 3600) ? '%Y-%m-%d %T' :
			(intvl <= 86400) ? '%Y-%m-%d %H:%M' :
			(maxdate.getUTCFullYear() - mindate.getUTCFullYear() < 1) ? 
			'%Y-%b-%d' : '%Y-%b';

		if (intvl <= 15552000) {
			this.interval = 
			      (intvl <= 240) ? 20 :
			      (intvl <= 420) ? 30 :
			      (intvl <= 600) ? 60 :
			      (intvl <= 1200) ? 120 :
			      (intvl <= 3600) ? 300 :
			      (intvl <= 7200) ? 600 :
			      (intvl <= 14400) ? 1200 :
			      (intvl <= 25200) ? 1800 :
			      (intvl <= 43200) ? 3600 :
			      (intvl <= 86400) ? 7200 :
			      (intvl <= 172800) ? 14400 :
			      (intvl <= 345600) ? 28800 :
			      (intvl <= 604800) ? 43200 :
			      (intvl <= 1296000) ? 86400 :
			      (intvl <= 2592000) ? 172800 :
			      (intvl <= 5184000) ? 432000 :
			      (intvl <= 10368000) ? 864000 :
			      1296000; // (intvl <= 15552000)
		}
		else {
			this.useMonths = true;
			intvl = ((maxdate.getUTCFullYear() * 12) + maxdate.getUTCMonth() + 1) -
				((mindate.getUTCFullYear() * 12) + mindate.getUTCMonth());
			this.interval = 
				(intvl <= 12) ? 1 :
				(intvl <= 24) ? 2 :
				(intvl <= 48) ? 4 :
				(intvl <= 84) ? 6 :
				12;
		}
	}
	if (this.useYears) {
		intvl = mindate.getUTCFullYear();
		intvl = this.interval * Math.floor(intvl/this.interval);
		mindate.setUTCFullYear(intvl, 0, 1);
		this.minval = mindate.getTime()/1000;
		intvl = maxdate.getUTCFullYear() + 1;
		intvl = this.interval * Math.ceil(intvl/this.interval);
		maxdate.setUTCFullYear(intvl, 0, 1);
		this.maxval = maxdate.getTime()/1000;
	}
	else if (this.useMonths) {
		intvl = (mindate.getUTCFullYear() * 12) + mindate.getUTCMonth();
		intvl = this.interval * Math.floor(intvl/this.interval);
		mindate = new Date(Math.floor(intvl/12), intvl%12, 1);
		this.minval = mindate.getTime()/1000;
		intvl = (maxdate.getUTCFullYear() * 12) + maxdate.getUTCMonth() + 1;
		intvl = this.interval * Math.ceil(intvl/this.interval);
		maxdate = new Date(Math.floor(intvl/12), intvl%12, 1);
		this.maxval = maxdate.getTime()/1000;
	}
	else {
/*
 *	expand the range a bit
 */
 		minval -= (this.interval * 0.1);
 		maxval += (this.interval * 0.1);
		this.minval = this.interval * Math.floor(minval/this.interval);
		this.maxval = this.interval * Math.ceil(maxval/this.interval);
	}
//
//	override tick format if we zoom in
//
	this.tickfmt = (this.tickFormat != null) ? this.tickFormat : fmt;
	return this.getTicks();
};

PlotJax.DateType = function(typespec) {
	PlotJax.DateType.baseConstructor.call(this, typespec);
	this.tickFormat = (typespec.TickFormat != null) ?
		typespec.TickFormat : "%Y-%m-%d";
	this.displayFormat = (typespec.Format != null) ?
		typespec.Format : "%Y-%m-%d";
	this.keepOrigin = false;

	this.minLimit = (typespec.Min != null) ? this.normalize(typespec.Min) : null;
	this.maxLimit = (typespec.Max != null) ? this.normalize(typespec.Max) : null;
	if ((this.minLimit != null) && (this.maxLimit != null) &&
		(this.maxLimit <= this.minLimit)) {
		alert("DateType: Invalid limits: max less than min.");
		return null;
	}
};

JSPPTrustee.extend(PlotJax.DateType, PlotJax.BaseType);

PlotJax.DateType.prototype.display = function(val) {
	var tdate = new Date();
	tdate.pljxSetTimeInSecs(val);
	return this.strftime(this.displayFormat, tdate);
};

//
//	!!!NEED TO FINISH THIS
//
PlotJax.DateType.prototype.getLog10Ticks = function() {
	var labels = [];
	var i = this.minval;
	var n = 0;
	var k = i;
	while (i <= this.maxval) {
		k = i + PlotJax.BaseType.logsteps[n++];
		if (n >= PlotJax.BaseType.logsteps.length) {
			n = 0;
			i = k;
		}
//		labels.push(this.strftime(this.tickfmt, tdate), tdate.getTime()/1000);
	}
	return labels;
};

PlotJax.DateType.prototype.getTemplate = function() { return 'YYYY-MMM-DD'; };

PlotJax.DateType.prototype.getTicks = function() {
	if (this.log1) {
		return this.getLog10Ticks();
	}
	var labels = [];
	var tick = this.minval;
	var year = 0;
	var month = 0;
	var tdate = new Date();
	tdate.pljxSetTimeInSecs(tick);
	if (this.useMonths) {
		year = tdate.getUTCFullYear();
		month = tdate.getUTCMonth();
	}
	else if (this.useYears) {
		year = tdate.getUTCFullYear();
	}
	labels.push(this.strftime(this.tickfmt, tdate), tdate.getTime()/1000);
	while (tick < this.maxval) {
		if (this.useYears) {
			year += this.interval;
			tdate.setUTCFullYear(year, 0, 1);
		}
		else if (this.useMonths) {
			month += this.interval;
			if (month > 11) {
				year++;
				month = 0;
			}
			tdate.setUTCFullYear(year, month, 1);
		}
		else {
			tick += this.interval;
			tdate.pljxSetTimeInSecs(tick);
		}
		tick = tdate.getTime()/1000;
		labels.push(this.strftime(this.tickfmt, tdate), tick);
	}
	return labels;
};

//
//	adjust the ticks based on the number of interval that contains the 
//	specified delta value
//
PlotJax.DateType.prototype.updateTicks = function(width, padding, ticks) {
//
//	NEED TO HANDLE LOG10 HERE!!!
//
	var mindate = new Date();
	var minyear = 0;
	var minmonth = 0;

	var maxdate = new Date();
	var maxyear = 0;
	var maxmonth = 0;
	var minval = this.minval;
	var maxval = this.maxval;
	mindate.pljxSetTimeInSecs(this.minval);
	maxdate.pljxSetTimeInSecs(this.maxval);

	var tmin = this.minval;
	var tmax = this.maxval;
	var intvls = 0;
	if (this.useMonths) {
		minyear = mindate.getUTCFullYear();
		minmonth = mindate.getUTCMonth();
		tmin = (minyear * 12) + minmonth;

		maxyear = maxdate.getUTCFullYear();
		maxmonth = maxdate.getUTCMonth();
		tmax = (maxyear * 12) + maxmonth;
	}
	else if (this.useYears) {
		minyear = mindate.getUTCFullYear();
		tmin = minyear;
		maxyear = maxdate.getUTCFullYear();
		tmax = maxyear;
	}
	intvls = Math.ceil((padding * (tmax - tmin)/(width - (padding * 2)))/this.interval);

	for (; intvls > 0; intvls--) {
		if (this.useYears) {
			minyear -= this.interval;
			maxyear += this.interval;
			mindate.setUTCFullYear(minyear, 0, 1);
			maxdate.setUTCFullYear(maxyear, 0, 1);
		}
		else if (this.useMonths) {
			minmonth -= this.interval;
			if (minmonth < 0) {
				minyear--;
				minmonth += 12;	// wrap back
			}
			mindate.setUTCFullYear(minyear, minmonth, 1);

			maxmonth += this.interval;
			if (maxmonth > 11) {
				maxyear++;
				maxmonth = 0;
			}
			maxdate.setUTCFullYear(maxyear, maxmonth, 1);
		}
		else {
			minval -= this.interval;
			mindate.pljxSetTimeInSecs(minval);
			maxval += this.interval;
			maxdate.pljxSetTimeInSecs(maxval);
		}
		ticks.unshift(this.strftime(this.tickfmt, mindate), mindate.getTime()/1000);
		ticks.push(this.strftime(this.tickfmt, maxdate), maxdate.getTime()/1000);
	}
	return ticks;
};

PlotJax.DateType.prototype.normalize = function(val) {
	var tdate = this.strptime(val);
	if (tdate == null) {
		alert("Invalid date value " + val + " for format " + this.displayFormat);
		return null;
	}
	return tdate.getTime()/1000;
};

PlotJax.DateType.prototype.setInterval = function(minval, maxval, sticky) {
	if (maxval == minval) {
		minval = minval - 86400;
		maxval = maxval + 86400;
	}
	if (this.log10) {
		minval = Math.floor(minval) - (minval < 0 ? 1 : 0);
		maxval = Math.floor(maxval) + 1;
	}
		
	var intvl = maxval - minval;
	this.useMonths = false;
	this.useYears = false;
	var fmt = '%Y-%m-%d';
	var mindate = new Date();
	var maxdate = new Date();
	mindate.pljxSetTimeInSecs(minval);
	maxdate.pljxSetTimeInSecs(maxval);
//
//	intervals are selected based on range:
//	> 10 year: treat as NumberType over years range
//
	if (maxdate.getUTCFullYear() - mindate.getUTCFullYear() > 10) {
//
//	Year only interval
//
		intvl = maxdate.getUTCFullYear() - mindate.getUTCFullYear() + 1;
		var xr = Math.log(intvl)/Math.log(10);
		var xd = xr - Math.floor(xr);
		var precision = Math.floor(xr) - 1;
		if (xd < 0.15) {
			this.interval = 1;
		}
		else if (xd < 0.4) {
			this.interval = 2;
		}
		else if (xd < 0.87) {
			this.interval = 5;
		}
		else {
			this.interval = 1;
			precision = Math.floor(xr);
		}
		this.interval *= Math.pow(10, precision);
		fmt = '%Y';
		this.useYears = true;
	}
	else { 
		fmt = (maxdate.getUTCFullYear() - mindate.getUTCFullYear() <= 1) ?
			'%Y-%b-%d' : '%Y-%b';
//	<= 15 days : 1 day intervals 
//	<= 1 month : 2 day intervals 
//	<= 2 months : 5 day intervals (rounded to nearest end-of-month)
//	<= 3 months : 10 day intervals (rounded to nearest end-of-month)
//	<= 6 months : 15 day intervals (rounded to nearest end-of-month)
//	<= 1 Year : 1 month intervals 
//	<= 2 Year : 2 month intervals 
//	<= 4 Year : 4 month intervals 
//	<= 7 Year : 6 month intervals 
//	<= 10 Year : 1 year intervals 
		if (intvl <= 15552000) {
			this.interval = 
				(intvl <= 1296000) ? 86400 :	// 15 day
				(intvl <= 2592000) ? 172800 :	// 1 month
				(intvl <= 5184000) ? 432000 :	// 2 month
				(intvl <= 10368000) ? 864000 :	// 3 month
				1296000; 					// 6 month
		}
		else {
			this.useMonths = true;
			intvl = ((maxdate.getUTCFullYear() * 12) + maxdate.getUTCMonth() + 1) -
				((mindate.getUTCFullYear() * 12) + mindate.getUTCMonth());
			this.interval = 
				(intvl <= 12) ? 1 :
				(intvl <= 24) ? 2 :
				(intvl <= 48) ? 4 :
				(intvl <= 84) ? 6 :
				12;
		}
	}
	if (this.useYears) {
		intvl = mindate.getUTCFullYear();
		intvl = this.interval * Math.floor(intvl/this.interval);
		mindate.setUTCFullYear(intvl, 0, 1);
		this.minval = mindate.getTime()/1000;
		intvl = maxdate.getUTCFullYear() + 1;
		intvl = this.interval * Math.ceil(intvl/this.interval);
		maxdate.setUTCFullYear(intvl, 0, 1);
		this.maxval = maxdate.getTime()/1000;
	}
	else if (this.useMonths) {
		intvl = (mindate.getUTCFullYear() * 12) + mindate.getUTCMonth();
		intvl = this.interval * Math.floor(intvl/this.interval);
		mindate = new Date(Math.floor(intvl/12), intvl%12, 1);
		this.minval = mindate.getTime()/1000;
		intvl = (maxdate.getUTCFullYear() * 12) + maxdate.getUTCMonth() + 1;
		intvl = this.interval * Math.ceil(intvl/this.interval);
		maxdate = new Date(Math.floor(intvl/12), intvl%12, 1);
		this.maxval = maxdate.getTime()/1000;
	}
	else {
		this.minval = this.interval * Math.floor(minval/this.interval);
		this.maxval = this.interval * Math.ceil(maxval/this.interval);
	}
//
//	override tick format if we zoom in
//
	this.tickfmt = (this.tickFormat != null) ? this.tickFormat : fmt;

	return this.getTicks();
};

PlotJax.NumberType = function(typespec) {
	PlotJax.NumberType.baseConstructor.call(this, typespec);
	if (typespec.TickFormat != null) {
		this.tickFormat = typespec.TickFormat;
		this.tickfmt = function(val) {
			return this.printf(this.tickFormat, val);
		};
	}
	if (typespec.Format != null) {
		this.displayFormat = typespec.Format;
		this.dispfmt = function(val) {
			return this.printf(this.displayFormat, val);
		};
	}

	this.minLimit = (typespec.Min != null) ?
		this.normalize(typespec.Min) : null;
	this.maxLimit = (typespec.Max != null) ?
		this.normalize(typespec.Max) : null;
	if ((this.minLimit != null) && (this.maxLimit != null) &&
		(this.maxLimit <= this.minLimit)) {
		alert("NumberType: Invalid limits: max less than min.");
		return null;
	}

	if (this.keepOrigin) {
		if ((this.maxLimit == null) || (this.maxLimit < 0)) {
			this.maxLimit = 0;
		}
		else if ((this.minLimit == null) || (this.minLimit > 0)) {
			this.minLimit = 0;
		}
	}
};

JSPPTrustee.extend(PlotJax.NumberType, PlotJax.BaseType);

PlotJax.NumberType.prototype.display = function(val) { 
	return (this.dispfmt != null) ? this.dispfmt(val) : val;
};

PlotJax.NumberType.prototype.displayLength = function() { 
	var mindisp = this.tickfmt(this.minval);
	var maxdisp = this.tickfmt(this.maxval);
	return Math.max(mindisp.length, maxdisp.length); 
};

//
//	!!!NEED TO FINISH THIS
//
PlotJax.NumberType.prototype.getLog10Ticks = function() {
	var labels = [];
	var i = this.minval;
	var n = 0;
	var k = i;
	while (i <= this.maxval) {
		k = i + PlotJax.BaseType.logsteps[n++];
		if (n >= PlotJax.BaseType.logsteps.length) {
			n = 0;
			i = k;
		}
		labels.push(this.tickfmt(i), i);
	}
	return labels;
};

PlotJax.NumberType.prototype.getTemplate = function() { return '-0.000e-00'; };

PlotJax.NumberType.prototype.getTicks = function() {
	if (this.log10) {
		return this.getLog10Ticks();
	}
	var labels = [];
	var tick = this.minval;
	if (this.tickfmt == null) {
		return null;
	}
	while (tick <= this.maxval) {
		labels.push(this.tickfmt(tick), tick);
		tick += this.interval;
	}
	return labels;
};

//
//	adjust the ticks based on the number of interval that contains the 
//	specified delta value
//
PlotJax.NumberType.prototype.updateTicks = function(width, padding, ticks) {
//
//	NEED TO HANDLE LOG10 HERE!!!
//
	var minval = this.minval;
	var maxval = this.maxval;
	var intvls = Math.ceil((padding * (this.maxval - this.minval)/(width - (padding * 2)))/this.interval);
	if (ticks[ticks.length - 1] != maxval) {
		ticks.push(this.tickfmt(maxval), maxval);
	}
	for (; intvls > 0; intvls--) {
		minval -= this.interval;
		maxval += this.interval;
		ticks.unshift(this.tickfmt(minval), minval);
		ticks.push(this.tickfmt(maxval), maxval);
	}
	return ticks;
};

PlotJax.NumberType.prototype.normalize = function(val) { return val; };

/*
 *	override base getLabel to apply truncation
 */
PlotJax.NumberType.prototype.getLabel = function() { 
	return (this.label.match(/%truncate%/) == null) ? this.label :
		(this.interval >= 1000000000) ? this.label.replace(/%truncate%/, "(billions)") :
		(this.interval >= 1000000)    ? this.label.replace(/%truncate%/, "(millions)") :
		(this.interval >= 1000)       ? this.label.replace(/%truncate%/, "(thousands)") :
		                                this.label.replace(/%truncate%/, "");
};

PlotJax.NumberType.prototype.setInterval = function(minval, maxval, sticky) {
	if (maxval == minval) {
		minval *= 0.75;
		maxval *= 1.25;
	}
	if (this.log10) {
		minval = Math.floor(minval) - (minval < 0 ? 1 : 0);
		maxval = Math.floor(maxval) + 1;
	}
		
	var xr = Math.log(maxval - minval)/Math.log(10);
	var xd = xr - Math.floor(xr);
	var precision = Math.floor(xr);
	if (xd < 0.87) { precision--; }
	this.interval = 
		(xd < 0.15) ? 1 :
		(xd < 0.4) ? 2 :
		(xd < 0.87) ? 6 :
		1;

	if (precision > 0) {
		this.interval *= Math.pow(10, precision);
//		if ((!sticky) || (this.tickFormat == null)) {
		if (this.tickFormat == null) {
			this.tickfmt = function(val) {
				return Math.round(val/this.truncator);
			};
		}
	}
	else if (precision < 0) {
		if (precision < -4) {
			this.interval += '.0e' + precision;
//			if ((!sticky) || (this.tickFormat == null)) {
			if (this.tickFormat == null) {
				this.tickfmt = function(val) {
					return val.toExponential(precision);
				};
			}
		}
		else {
			this.interval *= Math.pow(10, precision);
//			if ((!sticky) || (this.tickFormat == null)) {
			if (this.tickFormat == null) {
				this.tickfmt = function(val) {
					return val.toFixed(precision);
				};
			}
		}
	}
//
//	if interval within a contraction interval, contract the ticks
//	and update the label
//
	this.truncator = 1;
	if (this.label.match(/%truncate%/)) {
		if (this.interval >= 1000000000) {
			this.truncator = 1000000000;
		}
		else if (this.interval >= 1000000) {
			this.truncator = 1000000;
		}
		else if (this.interval >= 1000) {
			this.truncator = 1000;
		}
	}
/*
 *	expand the range a bit
 */
 	minval -= (this.interval * 0.1);
 	maxval += (this.interval * 0.1);
	this.minval = this.interval * Math.floor(minval/this.interval);
	this.maxval = this.interval * Math.ceil(maxval/this.interval);
	
	if (sticky && this.keepOrigin) {
		if (this.minval > 0) { this.minval = 0; }
		else if (this.maxval < 0) { this.maxval = 0; }
	}
	return this.getTicks();
};

PlotJax.SymbolType = function(typespec) {
	PlotJax.SymbolType.baseConstructor.call(this, typespec);
	this.symbols = [];
	this.ordinal = [];
	this.maxlen = 0;
	this.maxsym = '';
	this.log10 = false;
	this.keepOrigin = true;
	this.shadowvals = [];
/*
 *	create a shadow type for temporals
 */
	switch (typespec.Type) {
		case "date":
			this.shadow = new PlotJax.DateType(typespec);
			break;
			
		case "time":
			this.shadow = new PlotJax.TimeType(typespec);
			break;
		
		case "timestamp":
			this.shadow = new PlotJax.TimestampType(typespec);
			break;
		
		default:
			this.shadow = null;
	}
		
};

JSPPTrustee.extend(PlotJax.SymbolType, PlotJax.BaseType);

/*
 *	additional method for symbolic axes: returns the
 *	number of symbols defined; used for scaling in 3D
 *	bar charts
 */
PlotJax.SymbolType.prototype.cardinality = function() { return this.symbols.length; };

PlotJax.SymbolType.prototype.display = function(val) { return this.ordinal[val-1]; };

PlotJax.SymbolType.prototype.displayLength = function() { return this.maxlen; };

PlotJax.SymbolType.prototype.getTemplate = function() { return this.maxsym; };

PlotJax.SymbolType.prototype.sortDomain = function() {
	if (this.shadow != null) {
/*
 *	sort the values, then get their display forms
 */
		this.shadowvals = this.shadowvals.sort(function(a, b) { return a - b; });
		for (var i = 0; i < this.shadowvals.length; i++) {
			var t = this.shadow.display(this.shadowvals[i]);
			this.symbols[t] = i + 1;
			this.ordinal[i+1] = t;
		}
	}
};

PlotJax.SymbolType.prototype.getTicks = function() {
	var labels = [
		(this.minval >= 1) ? this.ordinal[this.minval-1] : "", 
		this.minval];
	var i;
	for (i = this.minval; i < this.maxval; i++) {
		if ((i >= 0) && (i < this.ordinal.length)) {
			labels.push(this.ordinal[i], i+1);
		}
		else {
			labels.push("", i+1);
		}
	}
	return labels;
};

//
//	adjust the ticks based on the number of interval that contains the 
//	specified delta value
//	For symbols, we just pad with empty strings
//	NOTE: no need for shadow here, only applies to bubblecharts
//
PlotJax.SymbolType.prototype.updateTicks = function(width,padding, ticks) {
	var minval = this.minval;
	var maxval = this.maxval;
	var intvls = Math.ceil((padding * (this.maxval - this.minval)/(width - (padding * 2)))/this.interval);
	for (; intvls > 0; intvls--) {
		minval--;
		maxval++;
		ticks.unshift("", minval);
		ticks.push("", maxval);
	}
	return ticks;
};

PlotJax.SymbolType.prototype.normalize = function(val) {

	if (this.shadow) {
	/*
	 *	if temporal, normalize it into display form
	 */
		var t = this.shadow.normalize(val);
		val = this.shadow.display(t);
		if (this.symbols[val] == null) {
			this.shadowvals.push(t);
		}
	}
	if (this.symbols[val] == null) {
		this.ordinal.push(val);
		this.symbols[val] = this.ordinal.length;
		if (this.maxlen < val.length) {
			this.maxlen = val.length;
			this.maxsym = val;
		}
	}
	return this.symbols[val]; 
};

PlotJax.SymbolType.prototype.setInterval = function(minval, maxval, sticky) {
	this.interval = 1;
	this.minval = minval - 1;
	this.maxval = maxval + 1;
	return this.getTicks();
};

/*
 *	!!! INTERVALS NOT YET FULLY IMPLEMENTED !!!
 */
PlotJax.IntervalType = function(typespec) {
	PlotJax.IntervalType.baseConstructor.call(this, typespec);
	this.valid_units = { year:6, month:5, day:4, hour:3, minute:2, second:1 };
	this.valid_precision = { month:5, hour:3, minute:2, second:1 };
	this.maxLimit = (typespec.Max != null) ? typespec.Max : null;
	this.minLimit = (typespec.Min != null) ? typespec.Min : null;
	
	if (this.keepOrigin) {
		if ((this.maxLimit == null) || (this.maxLimit < 0)) {
			this.maxLimit = 0;
		}
		else if ((this.minLimit == null) || (this.minLimit > 0)) {
			this.minLimit = 0;
		}
	}

	if (typespec.precision == null) {
		typespec.precision = "seconds";
	}

	this.precision = this.valid_precision[typespec.precision];

	if (this.units == null) {
		alert("Invalid unit " + typespec.units + " for interval type.");
		return null;
	}
	if (this.precision == null) {
		alert("Invalid precision " + typespec.precision + " for interval type.");
		return null;
	}
	if (this.precision > this.units) {
		alert("Invalid precision " + typespec.precision + " for " + typespec.units + " interval type.");
		return null;
	}

	this.restrs = [
		[ /^([\-+])?\s+(\d+(?:\.\d+)?)$/ ],	/* seconds */
		[ /^([\-+])?\s+(\d+):(\d{1,2}(?:\.\d+)?)$/, /^([\-+])?\s+(\d+)$/ ], /* minutes */
		[ /^([\-+])?\s+(\d+):(\d{1,2}):(\d{1,2}(?:\.\d+)?)$/,
			/^([\-+])?\s+(\d+):(\d{1,2})$/,
			/^([\-+])?\s+(\d+)$/
		], /* hours */
		[ /^([\-+])?\s+(?:(\d+)\s+)?(\d{1,2}):(\d{1,2}):(\d{1,2}(?:\.\d+)?)$/,
			/^([\-+])?\s+(?:(\d+)\s+)?(\d{1,2}):(\d{1,2})$/,
			/^([\-+])?\s+(?:(\d+)\s+)?(\d{1,2})$/,
			/^([\-+])?\s+(\d+)$/
		]	/* days */
	];
	this.re = new RegExp(this.restrs[this.units - 1][this.precision - 1]);

	this.minLimit = this.normalize(typespec.Min);
	this.maxLimit = this.normalize(typespec.Max);
	if ((this.minLimit != null) && (this.maxLimit != null) &&
		(this.maxLimit <= this.minLimit)) {
		alert("IntervalType: Invalid limits: max less than min.");
		return null;
	}
};

JSPPTrustee.extend(PlotJax.IntervalType, PlotJax.BaseType);

PlotJax.IntervalType.prototype.display = function(val) {
	if (this.units == 1) {
		return val;
	}

	var days, hours, mins, secs;
	var sign = "";
	if (val < 0) {
		sign = "-";
		val *= -1;
	}
	secs = val % 60;
	mins = (val - secs) % 3600;
	hours = (val - secs - mins) % (3600 * 24);
	days = (val - secs - mins - hours) / (3600 * 24);
	switch (this.units) {
		case 4:
			switch (this.precision) {
				case 1:
					mins /= 60;
					if (mins < 10) { mins = "0" + mins; }
					hours /= 3600;
					if (hours < 10) { hours = "0" + hours; }
					if (secs < 10) { secs = "0" + secs; }
					return sign + days + " " + hours + ":" + mins + ":" + secs;
				case 2:
					mins /= 60;
					if (mins < 10) { mins = "0" + mins; }
					hours /= 3600;
					if (hours < 10) { hours = "0" + hours; }
					return sign + days + " " + hours + ":" + mins;
				case 3:
					hours /= 3600;
					if (hours < 10) { hours = "0" + hours; }
					return sign + days + " " + hours;
				case 4:
					return sign + days;
			}
		case 3:
			hours += (days * 24);
			switch (this.precision) {
				case 1:
					mins /= 60;
					if (mins < 10) { mins = "0" + mins; }
					if (secs < 10) { secs = "0" + secs; }
					return sign + hours + ":" + mins + ":" + secs;
				case 2:
					mins /= 60;
					if (mins < 10) { mins = "0" + mins; }
					return sign + hours + ":" + mins;
				case 3:
					return sign + hours;
			}
		case 2:
			mins += (hours * 60) + (days * 24 * 60);
			switch (this.precision) {
				case 1:
					if (secs < 10) { secs = "0" + secs; }
					return sign + mins + ":" + secs;
				case 2:
					return sign + mins;
			}
	}
};

PlotJax.IntervalType.prototype.displayLength = function() {
	if (this.units == 1) {
		var mindisp = this.tickfmt(this.minval);
		var maxdisp = this.tickfmt(this.maxval);
		return Math.max(mindisp.length, maxdisp.length); 
	}

	var days, hours, mins, secs;
	var val = this.minval;
	secs = val % 60;
	mins = (val - secs) % 3600;
	hours = (val - secs - mins) % (3600 * 24);
	days = (val - secs - mins - hours) / (3600 * 24);
	switch (this.units) {
		case 4:
			switch (this.precision) {
				case 1:
					return 1 + 13 + days.length;
				case 2:
					return 1 + 6 + days.length;
				case 3:
					return 1 + 3 + days.length;
				case 4:
					return 1 + days.length;
			}
		case 3:
			hours += (days * 24);
			switch (this.precision) {
				case 1:
					return 1 + 10 + hours.length;
				case 2:
					return 1 + 3 + hours.length;
				case 3:
					return 1 + hours.length;
			}

		case 2:
			mins += (hours * 60) + (days * 24 * 60);
			switch (this.precision) {
				case 1:
					return 1 + 7 + mins.length;
				case 2:
					return 1 + mins.length;
			}
	}
};

//
//	!!!NEED TO FINISH THIS
//
PlotJax.IntervalType.prototype.getLog10Ticks = function() {
	var labels = [];
	var i = this.minval;
	var n = 0;
	var k = i;
	while (i <= this.maxval) {
		k = i + PlotJax.BaseType.logsteps[n++];

		if (n >= PlotJax.BaseType.logsteps.length) {
			n = 0;
			i = k;
		}
//		labels.push(this.strftime(this.tickfmt, tdate), tdate.getTime()/1000);
	}
	return labels;
};

PlotJax.IntervalType.prototype.getTemplate = function() { 
/*
 *	depends on the datatype
 */
	return 'HH:MM:SS.SSS'; 
};

PlotJax.IntervalType.prototype.getTicks = function() {
	if (this.log10) { return this.getLog10Ticks(); }
	var labels = [];
};

PlotJax.IntervalType.prototype.updateTicks = function(width, padding, ticks) {
//
//	NEED TO HANDLE LOG10 HERE!!!
//
	var minval = this.minval;
	var maxval = this.maxval;
	var intvls = Math.ceil((padding * (this.maxval - this.minval)/(width - (padding * 2)))/this.interval);
	for (; intvls > 0; intvls--) {
		minval--;
		maxval++;
		ticks.unshift("", minval);
		ticks.push("", maxval);
	}
	return ticks;
};

PlotJax.IntervalType.prototype.normalize = function(val) {
	var sign = 1;
	var days, hours, mins, secs;
	if (!this.re.test(val)) {
		alert("Invalid data format for interval type.");
		return null;
	}
	if ((RegExp.$1 != null) && (RegExp.$1 == "-")) {
		sign = -1;
	}

	if (this.units == 4) {
		days = (RegExp.$2 != null) ? RegExp.$2 : 0;
		if (this.precision == 4) {
			return sign * days * 24 * 60 * 60;
		}

		hours = RegExp.$3;
		if (hours > 23) {
			alert("Invalid value for interval type.");
			return null;
		}
		if (this.precision == 3) {
			return sign * ((days * 24 * 60 * 60) + (hours * 60 * 60));
		}

		mins = RegExp.$4;
		if (mins > 59) {
			alert("Invalid value for Interval type.");
			return null;
		}
		if (this.precision == 2) {
			return sign * ((days * 24 * 60 * 60) + (hours * 60 * 60) + (mins * 60));
		}

		secs = RegExp.$5;
		if (secs >= 60) {
			alert("Invalid value for Interval type.");
			return null;
		}
		return sign * ((days * 24 * 60 * 60) + (hours * 60 * 60) + (mins * 60) + secs);
	}
	else if (this.units == 3) {
		hours = RegExp.$2;
		if (this.precision == 3) {
			return sign * (hours * 60 * 60);
		}

		mins = RegExp.$3;
		if (mins > 59) {
			alert("Invalid value for Interval type.");
			return null;
		}
		if (this.precision == 2) {
			return sign * ((hours * 60 * 60) + (mins * 60));
		}

		secs = RegExp.$4;
		if (secs >= 60) {
			alert("Invalid value for Interval type.");
			return null;
		}
		return sign * ((hours * 60 * 60) + (mins * 60) + secs);
	}
	else if (this.units == 2) {
		mins = RegExp.$2;
		if (this.precision == 2) {
			return sign * (mins * 60);
		}
		secs = RegExp.$3;
		if (secs >= 60) {
			alert("Invalid value for Interval type.");
			return null;
		}
		return sign * ((mins * 60) + secs);
	}
	else {
		secs = RegExp.$2;
		return sign * secs;
	}
};

PlotJax.IntervalType.prototype.setInterval = function(minval, maxval, sticky) {
	if (maxval == minval) {
		minval *= 0.75;
		maxval *= 1.25;
	}
	if (this.log10) {
		minval = Math.floor(minval) - (minval < 0 ? 1 : 0);
		maxval = Math.floor(maxval) + 1;
	}
		
	var xr = Math.log(maxval - minval)/Math.log(10);
	var xd = xr - Math.floor(xr);
	var precision = Math.floor(xr) - 1;

	if (xd < 0.15) {
		this.interval = 1;
	}
	else if (xd < 0.4) {
		this.interval = 2;
	}
	else if (xd < 0.87) {
		this.interval = 5;
	}
	else {
		this.interval = 1;
		precision = Math.floor(xr);
	}

	if (precision > 0) {
		this.interval *= Math.pow(10, precision);
		this.tickfmt = function(val) {
			return Math.round(val/this.truncator);
		};
	}
	else if (precision < 0) {
		if (precision < -4) {
			this.interval += '.0e' + precision;
			this.tickfmt = function(val) {
				return val.toExponential(precision);
			};
		}
		else {
			this.interval *= Math.pow(10, precision);
			this.tickfmt = function(val) {
				return val.toFixed(precision);
			};
		}
	}
	this.minval = this.interval * Math.floor(minval/this.interval);
	this.maxval = this.interval * Math.ceil(maxval/this.interval);
		
	if (sticky && this.keepOrigin) {
		if (this.minval > 0) { this.minval = 0; }
		else if (this.maxval < 0) { this.maxval = 0; }
	}
	return this.getTicks();
};

// define a font context for PlotJax
//	note that some of the original parameters are no longer relevant, since
//	canvas text API does not require an externally defined font image
//	bitmap
PlotJax.Font = function(
	fontname, // a name used to lookup this font context
	fontdesc, // hash of description: Family, Size, Style, Weight, Color, Opacity
	charset, // character set of font - N/A always uses ASCII for now
	canvasctx) // the canvas to apply text to
{
	this.mycanvas = canvasctx;
	this.CharSet = charset;
	this.fontname = fontname;
	this.fontdesc = fontdesc;
	if (this.fontdesc.Opacity == null) { this.fontdesc.Opacity = 1.0; }
	this.fontdesc.Color = pljxGetColor(this.fontdesc.Color,
		'rgba(0,0,0,' + this.fontdesc.Opacity + ')', this.fontdesc.Opacity);
	this.fontheight = fontdesc.Size;	// size is expected to be same as height
	this.fontdef = '';
	if (fontdesc.Style != null) { this.fontdef += fontdesc.Style + ' '; }
	if (fontdesc.Weight != null) { this.fontdef += fontdesc.Weight + ' '; }
	this.fontdef += fontdesc.Size + 'pt ' + fontdesc.Family;
// compute font height using 'M' width (approximation)
	var oldfont = this.mycanvas.font;  // recover the current font when done computing
	this.mycanvas.font = this.fontdef;
	this.fontheight = Math.ceil(canvasctx.measureText('M').width);
	this.mycanvas.font = oldfont;	
};

PlotJax.Font.prototype = {

	// draw text along an angled line
	drawAlignedTo : function(string, x, y, linespacing, angle)
	{
	/*
	 *	compute bounding box
	 */
		var bounds = this.getBounds(string, 0, 0, linespacing, 0);
		var halfh = bounds[3] >> 1;
		var len = bounds[2];
		var ht = bounds[3];
	 /*
	  *	normalize angle
	  */
		angle = angle % 360;
		if (angle < 0) { angle = 360 + angle; }
		if (angle > 270) { angle = angle - 360; }
		if ((angle <= 90) && (angle >= -90))  {
			return this.drawAngledString(string, x, y, linespacing, 'left', angle);
		}
		if (angle <= 180) {
	/*
	 *	compute angled bbox, then use
	 *	lower left corner as x, y, and rotate
	 *	angle - 180
	 */
			bounds = this.getBounds(string, x, y, linespacing, angle-180);
			return this.drawAngledString(string, x - halfh, y, linespacing, 'right', angle - 180, bounds);
		}
	/*  angle <= 270:
	 *	compute angled bbox, then use
	 *	upper left corner as x, y, and rotate
	 *	360 - angle
	 */
		bounds = this.getBounds(string, x, y, linespacing, angle);
		return this.drawAngledString(string, bounds[0] + halfh, bounds[1], linespacing, 'left', angle - 180, bounds);
	},

	// draw text above the specified point
	drawAbove : function(string, x, y, offset, linespacing) 
	{
		if (offset == null) { offset = 0; }
		x -= (this.getWidth(string) >> 1);
		y -= (this.getHeight(string, linespacing) + offset);
		return this.drawString(string, x, y, linespacing, 'center', 0);
	},

	// draw text below the specified point
	drawBelow : function(string, x, y, offset, linespacing) 
	{
		if (offset == null) { offset = 0; }
		var bounds = this.getBounds(string, 0, 0, linespacing, 0);
		x -= (this.getWidth(string) >> 1);
		y += offset + 1;
		return this.drawString(string, x, y, linespacing, 'center');
	},

	// draw text to the left of specified point
	drawLeftOf : function(string, x, y, offset, linespacing) 
	{
		if (offset == null) { offset = 0; }
		x -= offset;
		y -= (this.getHeight(string, linespacing)  >> 1);
		return this.drawString(string, x, y, linespacing, 'right');
	},

	// draw text to the right of specified point
	drawRightOf : function(string, x, y, offset, linespacing) 
	{
		if (offset == null) { offset = 0; }
		x += offset;
		y -= (this.getHeight(string, linespacing) >> 1);
		return this.drawString(string, x, y, linespacing, 'left');
	},

	// render a string into a canvas at a specified location
	drawString : function(string, x, y, linespacing, align) 
	{

		string = "" + string; // avoid nulls
		if (align == null) { align = 'left'; }
		y = Math.round(y);

		if (linespacing == null) { linespacing = 4; }
		var lines = string.split("\n");
		var imght = this.fontheight + linespacing;
		
		var oldfont = this.mycanvas.font;
		this.mycanvas.font = this.fontdef;
		this.mycanvas.textBaseline = 'top'; // not sure if we need to tweak this, esp for certain angles ?
		this.mycanvas.textAlign = align;
		this.mycanvas.fillStyle = this.fontdesc.Color; // includes opacity
		for (i = 0; i < lines.length; i++, y += imght) 
		{
			this.mycanvas.fillText(lines[i], x, y);
		}
		this.mycanvas.font = oldfont;
		return this;
	},

	// render a string into a canvas at a specified location, with the specified rotation angle
	drawAngledString : function(string, x, y, linespacing, align, angle) 
	{

		string = "" + string; // avoid nulls
		if (align == null) { align = 'left'; }
		y = Math.round(y);

		if (linespacing == null) { linespacing = 4; }
		var lines = string.split("\n");
		var imght = this.fontheight + linespacing;
		
		this.mycanvas.save();
		this.mycanvas.translate(x, y);
		if ((angle != null) && (angle != 0)) 
		{ 
			this.mycanvas.rotate((Math.PI/180) * angle); 
		}
		y = 0;
		this.mycanvas.font = this.fontdef;
		this.mycanvas.textBaseline = 'top'; // not sure if we need to tweak this, esp for certain angles ?
		this.mycanvas.textAlign = align;
		this.mycanvas.fillStyle = this.fontdesc.Color; // includes opacity
		for (i = 0; i < lines.length; i++, y += imght) 
		{
			this.mycanvas.fillText(lines[i], 0, y);
		}
		this.mycanvas.restore();
		return this;
	},
/*
 *	compute required area to render a (possibly rotated) string with
 *	a specified font; adapts to multiline strings as needed
 *	returns the [left, top, right, bottom]
 *	coordinates of the bounding box
 */
	getBounds : function(string, x, y, linespacing, angle) {
	/*
	 *	first, compute bounding box using measureText
	 */
		if (linespacing == null) { linespacing = 4; }
		string = "" + string;
		var lines = string.split("\n");
		var maxwidth = 0;
		var i, j, pos, width = 0;
		var height = 0;
		var myht = this.fontheight;
		var oldfont = this.mycanvas.font;  // recover the current font when done computing
		this.mycanvas.font = this.fontdef;
		for (i = 0; i < lines.length; i++) {
			height += myht + linespacing;
			width = Math.round(this.mycanvas.measureText(lines[i]).width);
			if (maxwidth < width) { maxwidth = width; }
		}
		this.mycanvas.font = oldfont;
		height -= linespacing;


		if (angle == null) { angle = 0; }
		angle = angle % 360;

		if (angle == 0) {
			return [ x, y, x + maxwidth - 1, y + height - 1];
		}
	/*
	 *	compute bbox of rotated text
	 *	Note that negative rotation differs from positive
	 *	due to location of (left, top)
	 */
		var xp_left, xp_rt, yp_top, yp_bot;
		var radians;
		if (angle > 0) {	// clockwise rotation
			if (angle <= 90) {
				radians = (Math.PI/180) * angle;
				xp_left = x - (height * Math.sin(radians));
				xp_rt = x + (maxwidth * Math.cos(radians));
				yp_top = y;
				yp_bot = y + (height * Math.cos(radians)) + (maxwidth * Math.sin(radians));
			}
			else if (angle <= 180) {
				radians = (Math.PI/180) * (angle - 90);
				xp_left = x - (maxwidth * Math.sin(radians)) + (height * Math.cos(radians));
				xp_rt = x;
				yp_top = y + (maxwidth * Math.cos(radians));
				yp_bot = y + 3;
			}
			else if (angle <= 270) {
				radians = (Math.PI/180) * (angle - 180);
				var p1 = height * Math.sin(radians);
				var p2 = maxwidth * Math.cos(radians);
				xp_left = x - p2;
				xp_rt = x + p1;
				yp_top = y - (maxwidth * Math.sin(radians)) - (height * Math.cos(radians));
				yp_bot = y;
			}
			else {
				radians = (Math.PI/180) * (360 - angle);
				xp_left = x - (height * Math.sin(radians));
				xp_rt = x + (maxwidth * Math.cos(radians));
				yp_top = y - (height * Math.sin(radians)) - (maxwidth * Math.sin(radians));
				yp_bot = y;
			}
		}
		else { // angle < 0, counter clockwise rotation
			if (angle > -90) {
				radians = (Math.PI/180) * (-angle);
				xp_left = x;
				xp_rt = x + (maxwidth * Math.sin(radians)) + (height * Math.sin(radians));
				yp_top = y + ((maxwidth * Math.cos(radians)));
				yp_bot = y + (height * Math.cos(radians));
			}
			else if (angle > -180) {
				radians = (Math.PI/180) * (angle + 180);
				xp_left = x - (maxwidth * Math.cos(radians));
				xp_rt = x + (height * Math.sin(radians));
				yp_top = y - (maxwidth * Math.sin(radians)) + (height * Math.cos(radians));
				yp_bot = y;
			}
			else if (angle > -270) {
				radians = (Math.PI/180) * (angle + 270);
				xp_left = x - (maxwidth * Math.sin(radians)) - (height * Math.sin(radians));
				xp_rt = x;
				yp_top = y - (height * Math.sin(radians));
				yp_bot = y + (width * Math.cos(radians));
			}
			else {
				radians = (Math.PI/180) * (angle + 360);
				xp_left = x - (height * Math.sin(radians));
				xp_rt = x + (maxwidth * Math.cos(radians));
				yp_top = y;
				yp_bot = y + (height * Math.sin(radians)) + (maxwidth * Math.sin(radians));
			}
		}
		return [ Math.round(xp_left), Math.round(yp_top), Math.round(xp_rt), Math.round(yp_bot)];
	},

	// compute pixel height of (possibly multiline) string
	// with specified linespacing
	getHeight : function(string, linespacing)
	{
		if (null == string)
		{
			return this.fontheight; 
		}
		
		var lines = string.split("\n");
		if (linespacing == null) { linespacing = 3; }
		return (this.fontheight + linespacing) * lines.length;
	},
	
	// compute pixel width of (possibly multiline) string
	getWidth : function(string)
	{
		var lines = string.split("\n");
		var oldfont = this.mycanvas.font;  // recover the current font when done computing
		this.mycanvas.font = this.fontdef;
		var maxwidth = Math.ceil(this.mycanvas.measureText(lines[0]).width);
		for (i = 1; i < lines.length; i++)
		{
			maxwidth = Math.max(maxwidth, Math.ceil(this.mycanvas.measureText(lines[i]).width));
		}
		this.mycanvas.font = oldfont;
		return maxwidth;
	}
	
};

/*********************************************************************
 *
 *	Font Foundry class: this should be the only way font objects are
 *	constructed
 *
 *********************************************************************/
PlotJax.Foundry = function(canvasctx, fontjson) {
	this.mycanvas = canvasctx;
	this.fonts = {};
	if (fontjson != null)
	{
		this.addFontsFromJSON(fontjson);
	}
};

/*
 *	add a font directly (ie, not using JSON)
 */
PlotJax.Foundry.prototype = {
	addFont : function(font, size, color, opacity, charset) {
		var fontdesc = {};
		fontdesc.Family = font;
		fontdesc.Size = size;
		fontdesc.Color = color;
		fontdesc.Opacity = opacity;
		fontdesc.Weight = null;
		fontdesc.Style = null;
		return this.addNamedFont(this.canonical(font, size, color, opacity), fontdesc, charset);
	},

/*
 * configures 1 or more font descriptions from a JSON description
 *	returns the 1st font object created from the JSON
 *	Note that this method permits overwriting/replacing existing fonts
 */
	addFontsFromJSON : function(json) {
		var fontobj = (json instanceof String) ? JSON.parse(json) : json;
		var addedFonts = [];
		if (fontobj == null) {
			alert("Invalid JSON received!");
			return null;
		}
		var font;
		for (font in fontobj) {
			this.fonts[font] = this.addNamedFont(
				font,
				fontobj[font].Description,
				fontobj[font].CharSet);
			addedFonts.push(font);
		}
		return addedFonts[0];
	},

	addNamedFont : function(fontname, fontdesc, charset) {
		this.fonts[fontname] = {};
		this.fonts[fontname].Font = new PlotJax.Font(
			fontname,
			fontdesc,
			charset,
			this.mycanvas);
		this.fonts[fontname].Font.foundry = this;
		return this.fonts[fontname];
	},
/*
 *	convert font, size, color, alpha specification to canonical
 *	name. Color is either '#RRGGBB' or '#RRGGBBAA'
 *	format; in the former case, if opacity parameter is specified, it will
 *	be appended, else the default 'FF' opacity is appended
 */
	canonical : function(font, size, color, opacity) {
		if (color.length == 7) {
			color += "" + ((opacity == null) ? 'ff' : opacity.toString(16).toLowerCase());
		}
		return [
			font.replace(/\W+/g, '_').toLowerCase(),
			size,
			color.substr(1).toLowerCase()].join('_');
	},

	getFont : function(font, size, color, opacity) {
		return this.fonts[this.canonical(font, size, color, opacity)].Font;
	},

	getFontByName : function(fontname) {
		return this.fonts[fontname].Font;
	},
};

}