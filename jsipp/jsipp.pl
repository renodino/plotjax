=pod 

=head1 NAME

jsipp.pl - Javascript Class Inheritance Definition preprocessor

=begin html

<a href='jsipp-1.01.tar.gz'>Download jsipp-1.01.tar.gz</a>

=end html

=head1 SYNOPSIS

    jsipp.pl file1.jspp file2.jspp fileN.jspp >myfiles.js

=head1 DESCRIPTION

Converts Java-esque class definitions within Javascript source code
into properly prototyped Javascript class hierarchies. Requires the 
associated JSIPPTrustee function from the "jsipp.js" file, which 
provides the "trustee" to facilitate inheritance.

Classes are defined using a Java-esuqe syntax as follows:

    class ParentClass {
        ParentClass() {
            ... parent constructor code...
        }
        
        otherMethod() {
            ... some code ...
        }
    }
    
    class Classname extends ParentClass {
        Classname(I<params>) { // constructor
            ...constructor code...
            super(I<params>);    // call the parent constructor if needed
        }
        
        someMethod(I<params>) {    // a method
            ...method code...
            super.otherMethod();    // call method in ancestor class
            ...more code...
        }
    }

Command line arguments are 1 or more file names of files to be processed;
all output is written to STDOUT. Any parsing error will immediately
terminate the command.

=head1 CAVEATS

=over

=item *

methods (and their parameters) do not have types

=item *

no multiple inheritance support

=item *

method definition parameter lists cannot have embedded comments

=item *

a constructor *must* be defined for any subclass, even if it only invokes
the parent constructor. An empty constructor will be created for base 
classes without constructors

=item *

object members are not defined/instantiated until
the constructor method

=item *

obviously, cannot use "super" as an internal method or member

=item *

only a single constructor is permitted per class

=item *

each method can have only one implementation (ie, no
multi-signature methods)

=item *

no validation of parameter lists when calling super methods/constructor

=item *

parent classes must also be created using JSPPTrustee

=item *

The class inheritance mechanism and associated "trustee" helper function
is based on the code described at 
L<http://www.kevlindev.com/tutorials/javascript/inheritance/index.htm>

=back

=head1 AUTHOR, COPYRIGHT, AND LICENSE

Copyright(C) 2008, Dean Arnold, Presicient Corp. USA.

Permission is granted to use, copy, or redistribute this software
under the terms of the Academic Free License version 3.0.

=cut

use strict;
use warnings;

our $VERSION = '1.01';

my $debug = 0;
$/ = undef;

while (@ARGV) {
my $fname = shift @ARGV;
die "Unable to open $fname: $!"
	unless open(INF, $fname);
my $jstext = <INF>;
close INF;
my $jsout = '';
pos($jstext) = 0;
my $prefix;
my $done;
print "
/***********************************************
 *
 * BEGIN JSIPP OUTPUT FOR $fname
 *
 ***********************************************/
";
while ($jstext=~/\G(.*?)
	(?:
		(?:\bclass\s+(\w+)(?:\s+extends\s+(\w+))?\s*\{)|
		(['"]) |
		([^\w\)\/\s]\s*\/(?![\*\/])) |
		(\/\/.*?\n) | 
		(\/\*.*?\*\/)
	)/xgcs) {
	print STDERR "*** pos(jstext) = ", pos($jstext), "\n" if $debug;
	$jsout .= $1 if $1;
#
#	skip over comments
#
	$jsout .= ($6 || $7),
	next
		if ($6 || $7);

	my $t = ($4 || $5);
	if ($t) {
#
#	skip over strings and regexen...but watch out for escaped chars
#
		$prefix = ($t eq '"') ? 'double quoted string'
				: ($t eq "'") ? 'single quoted string'
				: 'regular expression';
		$jsout .= $t;
		$t = substr($t, -1);	# cuz regexen may have a prefix
		$done = undef;
		while ($jstext=~/\G((?:.*?)((?:\\.)|$t))/gcs) {
			$jsout .= $1;
			$done = 1,
			last 
				if ($2 eq $t);
		}
		next if $done;
		die "Unterminated $prefix near \"" . substr($jstext, pos($jstext), 30) . '"';
	}
#
#	accumulate class contents
#	locate constructor
#	variables, classname w/ args as constructor, and methods
#
	my $class = $2;
	my $parent = $3;
	$jsout .= $parent 
		? "/*\n *\tclass $class extends $parent {\n */"
		: "/*\n *\tclass $class {\n */";
	my $braces = 1;
	my %methods = ();
	my $body = '';
	my $inmethod;
	my $params;
	while ($braces && ($jstext=~/\G(.*?)
		(?:
			(\b(\w+)\s*\(([^\)]*)\)\s*\{) |
			(['"\{\}]) |
			([^\w\)\/\s]\s*\/(?![\*\/])) |
			(\/\/.*?\n) |
			(\/\*.*?\*\/)
		)/xgcs)) {
		print STDERR "*** pos(jstext) = ", pos($jstext), "\n" if $debug;
		$body .= $1 if $1;
		$body .= ($7 || $8),
		next
			if ($7 || $8);
		$t = ($5 || $6);
		if ($t) {
			if ($t eq '}') {
				$braces--;
				last 		#	closing brace for class
					unless $braces;

				$body .= '}';
				next
					if ($braces > 1);
			#
			#	must be ending a method
			#
				$body=~s/\n[ \t]+\}$/\n}/;
				$body .= ';' if ($inmethod ne $class);
				$methods{$inmethod} = process_method($class, $inmethod, $body, $parent);
				$inmethod = '';
				$body = '';
				next;
			}

			if ($t eq '{') {
				$body .= '{';
				$braces++;
				next;
			}

			$prefix = ($t eq '"') ? 'double quoted string'
					: ($t eq "'") ? 'single quoted string'
					: 'regular expression',
			$body .= $t;
			$t = substr($t, -1);	# cuz regexen may have a prefix
			$done = undef;
			while ($jstext=~/\G((?:.*?)((?:\\.)|$t))/gcs) {
				$body .= $1;
				$done = 1,
				last 
					if ($2 eq $t);
			}
			next if $done;
			die "Unterminated $prefix near \"" . substr($jstext, pos($jstext), 30) . '"';
		}
		else {	# maybe starting a new method
		#
		#	don't start new method if already in one
		#	OR its a try/catch
		#
			$braces++,
			$body .= $2,
			next
				if ($braces > 1);
		#
		#	must be starting a method
		#
			$braces = 2;
			$inmethod = $3;	# method name
			$params = $4;
			my $orig = $2;
			$body=~s/\n[ \t]+$/\n/;
			$body .= ($inmethod eq $class) 
				? "/* $orig */\nfunction $class($params) {"
				: "/* $orig */\n$class.prototype.$inmethod = function($params) {";
		}
	}	# end while matches in classbody

	die "Unterminated definition for class $class"
		if $braces;
#
#	end of class, finish it
#
	$jsout .= process_class($class, \%methods, $parent);
	$class = '';
	$parent = '';
	%methods = ();
	$body = '';
}
#
#	don't forget any trailing content
#
print $jsout, substr($jstext, pos($jstext)),
"
/***********************************************
 *
 * END JSIPP OUTPUT FOR $fname
 *
 ***********************************************/
";
}

sub process_method {
	my ($class, $method, $body, $parent) = @_;
	my $c;
	pos($body) = 0;
	my $content = '';
	print STDERR "\n*** method $class.$method:\n$body\n" if $debug;
	while ($body=~/\G(.*?)
		(?:
			(\bsuper(?:\.(\w+))?\s*\() |
			(['"]) |
			([^\w\)\/\s]\s*\/(?![\*\/])) |
			(\/\/.*?\n) |
			(\/\*.*?\*\/)
		)/xgcs) {
		$content .= $1 if $1;
		$content .= ($6 || $7),
		next
			if ($6 || $7);

		$c = defined $4 ? $4 : $5;

		if (defined $c) {
			$content .= $c;
			$c = substr($c, -1);	# cuz regexen may have a prefix
			while ($body=~/\G((?:.*?)((?:\\.)|$c))/gcs) {
				$content .= $1;
				last 
					if ($2 eq $c);
			}
			next;	# no error needed, we already parsed this
		}
	#
	#	must be a super call, scan to end of param list
	#
		my $parens = 1;
		my $super = $2;
		my $isconstr = (!$3);
		die "super constructor cannot be called from non-constructor method $class.$method."
			if $isconstr && ($method ne $class);
		while ($parens && ($body=~/\G(.*?)
			(?:
				(['"\(\)]) |
				([^\w\/\s]\s*\/(?![\*\/])) |
				(\/\/.*?\n) |
				(\/\*.*?\*\/)
			)/xgcs)) {
			$super .= $1 if $1;
			next	# omit embedded comments
				if ($4 || $5);
			$c = defined $2 ? $2 : $3;
			$super .= $c;
			
			$parens++,
			next
				if ($c eq '(');

			$parens--,
			next
				if ($c eq ')');

			$c = substr($c, -1);	# cuz regexen may have a prefix
			while ($body=~/\G((?:.*?)((?:\\.)|$c))/gcs) {
				$super .= $1;
				last 
					if ($2 eq $c);
			}
		}
		die "Unclosed parameter list for $class.$method."
			if $parens;

		if ($isconstr) {
			$super=~s/^super\s*\((.*)\)$/$class.baseConstructor.call(this, $1)/
				unless ($super=~s/^super\s*\(\s*\)$/$class.baseConstructor.call(this)/);
		}
		else {
			$super=~s/^super(\.\w+\s*)\((.*)\)$/$class.superClass$1.call(this, $2)/
				unless ($super=~s/^super(\.\w+\s*)\(\s*\)/$class.superClass$1.call(this)/);
		}
		$content .= $super;
	}
	$content .= substr($body, pos($body));
	return $content;
}

sub process_class {
	my ($class, $methods, $parent) = @_;
#
#	capture constructor and methods
#
	my $constructor = delete $methods->{$class};
	unless ($constructor) {
		die "No constructor defined for $class (subclass of $parent)"
			if $parent;
		$constructor = "/*\n * Default Constructor\n */\nfunction $class() { }\n";
	}
	return ($parent ?
"$constructor

JSPPTrustee.extend($class, $parent);
" 
: "$constructor\n"). join("\n", map $methods->{$_}, sort keys %$methods) . "\n";

}
