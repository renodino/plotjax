## PlotJax TO DO

 1. Retest w/ latest Chrome, Firefox, IE, Safari, Opera
 
 2. balloon position needs to adjust for scroll region

 3. barcharts
	- each chart must get full domain
	- if stacked
	- if floating
	- else

	   foreach domain value

	      compute bbox of bar from offset and pxlsperbar

	      if iconic

	         drawImage until filled, possibly clipping as needed

	      else

	         fill with assigned color

	   end for

 4. vertical barchart

 5. tests for v. barchart

 6. horiz. barchart

 7. tests for h. barchart

 8. vertical iconic barchart

 9. tests for v. iconic barchart

 10. horiz. iconic barchart

 11. tests for h. iconic barchart

 12. stacked vertical barchart

 13. tests for stacked v. barchart

 14. horiz. stacked barchart

 15. tests for h. stacked barchart

 16. vertical stacked iconic barchart

 17. tests for v. stacked iconic barchart

 18. horiz. stacked iconic barchart

 19. tests for h. stacked iconic barchart

 20. add ShowValues property ? (print value above/below datapt)

 21. support line/areagraph abutting the left axis

 22. areagraph

 23. tests for areagraph

 24. stacked areagraph

 25. tests for stacked areagraph

 26. validate all Helper i/f's (onflick, oncapture, etc.)

 27. treemap

 28. tests for treemap

 29. update homepage

 30. wizard page w/ form to enter style info, and textarea for datapts,
    and then generate the canvas, and print the JSON

 32. update docs to indicate	app must provide a
	tick format spec that applies to all possible zooms;
	the default will adjust to appropriate scaling

 33. document limitations of hotkeys; state that
	"document.getElementById("chartdivname").chart.keyDown(event);"
	will add ctrlZ => undo, ctrlY => redo to the chart
	(no way to move focus to a canvas/div)

 34. document behavior of bubble charts on zoom: bubble
   sizes/colors may be rescaled to new zoom limits

 35. implement zoom.intersects() for pie/barchart observers

 36. fix DateType/Timestamp object implementation for higher precision

 37. implement Y2 axis support

 38. boxchart

 39. tests for boxchart

 40. bulletchart

 41. tests for bulletchart

 42. 3d barchart

 43. tests for 3d barchart

 44. horiz. 3d barchart

 45. tests for horiz. 3d barchart

 46. cylinder barchart

 47. tests for cylinder barchart

 48. horiz. cylinder barchart

 49. tests for horiz. cylinder barchart

 50. stacked 3d barchart

 51. tests for stacked 3d barchart

 52. horiz. stacked 3d barchart

 53. tests for horiz. stacked 3d barchart

 54. stacked cylinder barchart

 55. tests for stacked cylinder barchart

 56. horiz. stacked cylinder barchart

 57. tests for horiz. stacked cylinder barchart

 58. 3D piechart

 59. tests for 3D piechart

 60. segmented 3D piechart

 61. tests for segmented 3D piechart

 62. candlestick

 63. tests for candlestick

 64. gauge

 65. tests for gauge

 66. halfgauge

 67. tests for halfgauge

 68. pareto chart

 69. test pareto chart

 70. linefit

 71. test linefit

 72. polyfit

 73. test polyfit

 74. expfit

 75. test expfit

 76. stripgauge

 77. test stripgauge

 78. Gantt

 79. test Gantt

 80. implement interval type classes

 81. JSON directives for streambox style

 82. animate sliding streambox

 83. undo all to streambox

 84. add press/hold mouse button support to streambox

 85. add flick support to streambox

 86. JSON directives for hover style

 87. helper hooks to add items to buttonbar

 88. Balloon: fix to permit clickthru the ballondiv to underlying
	balloon'ed elements (currently, the balloondiv blocks
	the click to the underlying bubbles)

 89. hilights: on mouseover bubble/bar/cylinder/treemap area,
	raise a brighter color; lower color on mouseout
	- may require using individual divs for chart areas

 90. create separate balloon divs for highlights/animation

 91. adjust vertical margins for numeric X axis with offset origin

 92. Trailing color for progress bars (visual effect)

 93. Check to keep: all ugestures down slash/backslash, followed by all
	up backslash/slash: generate square spanning ugestures,
	add bubble that fires in it to keep list
