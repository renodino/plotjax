/*
 *	namespace registry, as seen at 
 *	http://weblogs.asp.net/mschwarz/archive/2005/08/26/423699.aspx
 */
function registerNS(ns)
{
 var nsParts = ns.split(".");
 var root = window;

 for(var i=0; i<nsParts.length; i++)
 {
  if(typeof root[nsParts[i]] == "undefined")
   root[nsParts[i]] = new Object();

  root = root[nsParts[i]];
 }
}

registerNS("JSPPTrustee");

/*
 * Utility function derived from the mechanism described by Kevin Lindsey at
 * http://www.kevlindev.com/tutorials/javascript/inheritance/index.htm
 */
/**
 * A function used to extend one class with another
 * 
 * @param {Object} child
 * 		The inheriting class, or subclass
 * @param {Object} parent
 * 		The class from which to inherit
 */
JSPPTrustee.extend = function(child, parent) {
   function inheritance() {}
   inheritance.prototype = parent.prototype;

   child.prototype = new inheritance();
   child.prototype.constructor = child;
   child.baseConstructor = parent;
   child.superClass = parent.prototype;
}
