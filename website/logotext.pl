use Imager;
use Imager::Color;
use Imager::Font;

use strict;
use warnings;

my ($inpimg, $outpimg, $face, $size, $x, $y, $string) = @ARGV;

my $img = Imager->new();

$img->read(file => $inpimg) or die $img->errstr;

#my $black = Imager::Color->new(0,0,0);
my $font = Imager::Font->new(face => $face);
$img->string(font => $font, x => $x, y => $y, size => $size, align => 0,
	color => '#40404080', string => $string) or die $img->errstr;
$img->write(file => $outpimg) or die $img->errstr;